<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf_token" content="<?php echo csrf_token(); ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height" />
        
        <meta name="keywords" content="<?php echo e(Setting::get('meta_keywords')); ?>">
        <meta name="description" content="<?php echo e(Setting::get('meta_description')); ?>">
        <link rel="icon" type="image/x-icon" href="<?php echo url('setting/'.Setting::get('favicon')); ?>">

        <meta content="<?php echo e(url('/')); ?>" property="og:url" />
        <meta content="<?php echo url('setting/'.Setting::get('logo')); ?>" property="og:image" />
        <meta content="<?php echo e(Setting::get('meta_description')); ?>" property="og:description" />
        <meta content="<?php echo e(Setting::get('site_name')); ?>" property="og:title" />
        <meta content="website" property="og:type" />
        <meta content="<?php echo e(Setting::get('site_name')); ?>" property="og:site_name" />


        <title><?php echo e(Theme::get('title')); ?></title>
                 <!-- Bootstrap CSS File -->
        <link rel="stylesheet" id="flaticon-css" href="<?php echo e(Theme::asset()->url('lib/flaticon.css')); ?>" type="text/css" media="all">
          <link href="<?php echo e(Theme::asset()->url('lib/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet">
          <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@800&display=swap" rel="stylesheet">
          <!-- Libraries CSS Files -->
          <link href="<?php echo e(Theme::asset()->url('lib/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet">
          <link href="<?php echo e(Theme::asset()->url('lib/prettyphoto/css/prettyphoto.css')); ?>" rel="stylesheet">
          <link href="<?php echo e(Theme::asset()->url('lib/hover/hoverex-all.css')); ?>" rel="stylesheet">
          <link href="<?php echo e(Theme::asset()->url('lib/jetmenu/jetmenu.css')); ?>" rel="stylesheet">
          <link href="<?php echo e(Theme::asset()->url('lib/owl-carousel/owl-carousel.css')); ?>" rel="stylesheet">

          <link rel="icon" type="image/x-icon" href="<?php echo url('setting/'.Setting::get('favicon')); ?>">

          <!-- Main Stylesheet File -->
          <link href="<?php echo e(Theme::asset()->url('lib/style.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(Theme::asset()->url('css/flag-icon.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(Theme::asset()->url('css/home.css')); ?>" rel="stylesheet">
  <script src="<?php echo Theme::asset()->url('lib/jquery/jquery.min.js'); ?>"></script>
  <script src="<?php echo Theme::asset()->url('lib/bootstrap/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo Theme::asset()->url('lib/js/modernizr.custom.js'); ?>"></script>

        <?php echo Theme::asset()->styles(); ?>


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
        function SP_source() {
          return "<?php echo e(url('/')); ?>/";
        }
        var base_url = "<?php echo e(url('/')); ?>/";
        var theme_url = "<?php echo Theme::asset()->url(''); ?>";
        var current_username = "";
        </script>
        <?php echo Theme::asset()->scripts(); ?>

        <?php if(Setting::get('google_analytics') != NULL): ?>
            <?php echo Setting::get('google_analytics'); ?>

        <?php endif; ?>
        <script src="<?php echo Theme::asset()->url('js/lightgallery.js'); ?>"></script>
    </head>
    <body <?php if(Setting::get('enable_rtl') == 'on'): ?> class="direction-rtl" <?php endif; ?>>
       <?php echo Theme::partial('home-header'); ?>


        <div class="main-content ">
            <?php echo Theme::content(); ?> 
        </div>

        
        <?php echo Theme::partial('home-footer'); ?> 

        <script>
          <?php if(Config::get('app.debug')): ?>
            // Pusher.logToConsole = true;
          <?php endif; ?>
            var pusherConfig = {
                token: "<?php echo e(csrf_token()); ?>",
                PUSHER_KEY: "<?php echo e(config('broadcasting.connections.pusher.key')); ?>"
            };
       </script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.5.0/socket.io.min.js"></script>
        <!-- JavaScript Libraries -->
  <script src="<?php echo Theme::asset()->url('lib/php-mail-form/validate.js'); ?>"></script>
  <script src="<?php echo Theme::asset()->url('lib/prettyphoto/js/prettyphoto.js'); ?>"></script>
  <script src="<?php echo Theme::asset()->url('lib/isotope/isotope.min.js'); ?>"></script>
  <script src="<?php echo Theme::asset()->url('lib/hover/hoverdir.js'); ?>"></script>
  <script src="<?php echo Theme::asset()->url('lib/hover/hoverex.min.js'); ?>"></script>
  <script src="<?php echo Theme::asset()->url('lib/unveil-effects/unveil-effects.js'); ?>"></script>
  <script src="<?php echo Theme::asset()->url('lib/owl-carousel/owl-carousel.js'); ?>"></script>
  <script src="<?php echo Theme::asset()->url('lib/jetmenu/jetmenu.js'); ?>"></script>
  <script src="<?php echo Theme::asset()->url('lib/animate-enhanced/animate-enhanced.min.js'); ?>"></script>
  <script src="<?php echo Theme::asset()->url('lib/jigowatt/jigowatt.js'); ?>"></script>
  <script src="<?php echo Theme::asset()->url('lib/easypiechart/easypiechart.min.js'); ?>"></script>
  <script src="<?php echo Theme::asset()->url('lib/js/bookblock.js'); ?>"></script>
  <script src="<?php echo Theme::asset()->url('lib/js/bookblock.min.js'); ?>"></script>
  <script src="<?php echo Theme::asset()->url('lib/js/jquery.bookblock.js'); ?>"></script>
  <script src="<?php echo Theme::asset()->url('lib/js/jquery.bookblock.min.js'); ?>"></script>
  <script src="<?php echo Theme::asset()->url('lib/js/jquerypp.custom.js'); ?>"></script>

  <!-- Template Main Javascript File -->
  <script src="<?php echo Theme::asset()->url('lib/main.js'); ?>"></script>
        <?php echo Theme::asset()->container('footer')->scripts(); ?>

    </body>
</html>
