
<?php if($timeline->timeline_user != null): ?>
<?php //dd($timeline->timeline_user); ?>
<?php if(Auth::user()->hasRole('customer') || $timeline->timeline_user->id == Auth::user()->id): ?> 
<?php if(!empty($timeline->user_services)): ?>
<div class="widget-pictures widget-best-pictures all-groups" style="margin-top: 0px;">
	<div class="picture side-left">
		Calander
	</div>
	<div class="clearfix"></div>
	<div class="best-pictures mydatepick scrollable">
		<div class="row">
			<div id="mdatepicker"></div>		
		</div><!-- /row -->
	</div>
	<div class="show-more-options text-center">
	</div>
</div>
<script>
	$(function(){ 
		var currentDate = new Date();
		$( "#mdatepicker" ).datepicker({ startDate: new Date(),todayHighlight: true, format: "yyyy-mm-dd"});

		$("#bookApt").on('click',function(){
			if($('#mdatepicker').data('datepicker').getFormattedDate('yyyy-mm-dd') == ""){
				alert("Please select Appointment Date First");
				return;
			}
			$("#appDate").html("Appoinment Date on "+$('#mdatepicker').data('datepicker').getFormattedDate('dd M,yyyy'));
		//	alert($('#mdatepicker').data('datepicker').getFormattedDate('yyyy-mm-dd'));
			$("#apointmentPopup").modal('show');
		});
	});
</script>
<?php endif; ?>
<?php if($timeline->timeline_user->id == Auth::user()->id): ?>
  	<a href="<?php echo e(url('/'.$timeline->username.'/settings/my-bookings')); ?>" class="btn defBtn">My Bookings</a>
<?php else: ?>         
<?php if(!empty($timeline->user_services) && (!empty($timeline->timeline_user->lat_pos)&&!empty($timeline->timeline_user->long_pos))): ?>
	<input type="button" id="bookApt" value="Book Appointment" class="btn btn-book">
	<input type="button" value="Consult Instantly" class="btn btn-bo-ins">

	<!-- Modal -->
<div id="apointmentPopup" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="appDate"></h4>
      </div>
      <div class="modal-body">
        <p>Select Available Services</p>
		<input type="button" id="bookApt" value="Request Appointment" class="btn btn-book">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<?php else: ?>
	<p>Currently Not Accepting Bookings</p>
<?php endif; ?>
<?php endif; ?>
<?php endif; ?>
<div class="widget-pictures widget-best-pictures all-groups">
	<div class="picture side-left">
		Location
		<?php if($timeline->timeline_user->id == Auth::user()->id): ?>
          <a href="<?php echo e(url('/'.$timeline->username.'/settings/general')); ?>">Update Location</a>
        <?php endif; ?>
	</div>
	<div class="clearfix"></div>
	<br/>
	<div class="best-pictures scrollable">
		<div class="row">
			<?php if(!empty($timeline->timeline_user->lat_pos)&&!empty($timeline->timeline_user->long_pos)): ?>
			<iframe width="100%" height="200" src="https://maps.google.com/maps?q=<?php echo e($timeline->timeline_user->lat_pos); ?>,<?php echo e($timeline->timeline_user->long_pos); ?>&hl=es;z=14&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>	
			<?php else: ?>
			<p class="p10">Address not available</p>
			<?php endif; ?>
		</div><!-- /row -->
	</div>
	<div class="show-more-options text-center">
	</div>
</div>


<?php endif; ?>
<?php if(Setting::get('timeline_right_ad') != NULL): ?>
	<?php echo htmlspecialchars_decode(Setting::get('timeline_right_ad')); ?>

<?php endif; ?>
<?php if(Config::get('app.env') == 'demo' || Config::get('app.env') == 'local'): ?>
	<!-- <img src="http://placehold.it/165x200?text=Ad+Block"><br><br>
	<img src="http://placehold.it/165x200?text=Ad+Block"> -->
<?php endif; ?>
