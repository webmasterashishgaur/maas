<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
     protected $table = 'galleries';
	 public $timestamps = FALSE;
     protected $fillable = ['name','images', 'status', 'normal_price', 'member_price', 'type', 'descriptions'];
}
