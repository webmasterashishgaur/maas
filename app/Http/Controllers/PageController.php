<?php

namespace App\Http\Controllers;

use App\Post;
use App\Blog;
use App\Slider;
use App\Cities;
use App\Setting;
use App\Category;
use App\StaticPage;
use Flash;
use App\User;
use DB;
use Auth;
use Redirect;
use Session;
use URL;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Teepluss\Theme\Facades\Theme;
use Validator;

class PageController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->checkCensored();
    }

    protected function checkCensored()
    {
        $messages['not_contains'] = 'The :attribute must not contain banned words';
        if($this->request->method() == 'POST') {
            // Adjust the rules as needed
            $this->validate($this->request, 
                [
                  'name'          => 'not_contains',
                  'about'         => 'not_contains',
                  'title'         => 'not_contains',
                  'description'   => 'not_contains',
                  'tag'           => 'not_contains',
                  'email'         => 'not_contains',
                  'body'          => 'not_contains',
                  'link'          => 'not_contains',
                  'address'       => 'not_contains',
                  'website'       => 'not_contains',
                  'display_name'  => 'not_contains',
                  'key'           => 'not_contains',
                  'value'         => 'not_contains',
                  'subject'       => 'not_contains',
                  'username'      => 'not_contains',
                  'email'         => 'email',
                ],$messages);
        }
    }
    
    public function home(){
		$sliders = Slider::all();
		$blog = Blog::all();
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('home');
        $theme->setTitle(trans('common.contact').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
        $categories = Category::select(['id','name'])->get();
        $cities = Cities::select(['city_key','city_name'])->get();
        //dd($categories);
        return $theme->scope('index', compact('sliders','categories','cities', 'blog'))->render();
    }
    public function page($pagename)
    {
        $page = StaticPage::where('slug', $pagename)->first();
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('guest');
        $theme->setTitle($pagename.' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
        return $theme->scope('pages/page', compact('page'))->render();
    }
    public function contact(){
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('home');
        $theme->setTitle(trans('common.contact').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
        return $theme->scope('pages/contact')->render();
    }
     public function aboutus()
    {	
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('home');
        $theme->setTitle(trans('common.about us').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
        return $theme->scope('pages/about-us')->render();
    }
    public function services()
    {
		
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('home');
        $theme->setTitle(trans('common.about us').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('pages/services')->render();
    }
	
	   public function getaccredited()
    {
		
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('home');
        $theme->setTitle(trans('common.about us').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('pages/get-accredited')->render();
    }
	public function covid()
    {
		
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('home');
        $theme->setTitle(trans('common.about us').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('pages/covid-19-certification')->render();
    }
	   public function joinmass()
    {
		$pay = 0;
        $pay = \Session::get('pay_success');

        if($pay == 1){
          
            $jmat = \Session::get('joinMassAmt');
            $jmcr = \Session::get('joinMassCur');
            $uid = \Session::get('joinMassUID');
            $utm = \Session::get('joinMassTM');
            \Session::put('success', 'Payment success');

            DB::table('users')
            ->where('id', $uid)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('payment_status' => 1));  // update the record in the DB.

            $user = User::find($uid);
            Auth::login($user);
            return Redirect::to('/'.$utm);
        }
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('home');
        $theme->setTitle(trans('common.about us').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
        $cities = Cities::all()->pluck('city_name', 'city_key');
        //dd($cities);
        return $theme->scope('pages/join-mass', compact('cities'))->render();
    }

	public function trainningcourses()
    {
		
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('home');
        $theme->setTitle(trans('common.about us').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('pages/trainning-courses')->render();
    }
	public function testimonial()
    {
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('home');
        $theme->setTitle(trans('common.about us').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('pages/testimonials')->render();
    }

	public function result(Request $request)
    {
		
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('home');
        $theme->setTitle(trans('common.about us').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
     
        if(empty($request->category) && empty($request->location)){
            $userLists = User::limit(30)->where("role_id","=",2)->join('cities','cities.city_key','=','users.city_key')->leftJoin('timelines','users.timeline_id','=','timelines.id')->join('media','media.id','=','timelines.avatar_id')->get(['timelines.id','timelines.name as profile_name','cities.city_name','users.pd_price','users.review_count','users.review_star','media.source']);
            //dd($userLists);
        }else{
           $userLists = User::limit(30)->where("role_id","=",2)->join('cities','cities.city_key','=','users.city_key')->leftJoin('timelines','users.timeline_id','=','timelines.id')->join('media','media.id','=','timelines.avatar_id')->get(['timelines.id','timelines.name','cities.city_name','users.pd_price','users.review_count','users.review_star','media.source']);
        }
        //dd($userLists);
        return $theme->scope('pages/results',compact('userLists'))->render();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateContactPage(array $data)
    {
        return Validator::make($data, [
            'name'    => 'required',
            'email'   => 'required',
            'subject' => 'required',
            'message' => 'required',
        ]);
    }

    public function saveContact(Request $request)
    {
        $validator = $this->validateContactPage($request->all());

        if ($validator->fails()) {
            return redirect()->back()
            ->withInput($request->all())
            ->withErrors($validator->errors());
        }

        $mail = $request->all();

        $emailStatus = Mail::send('emails.usermail', ['mail' => $mail], function ($m) use ($mail) {
            $m->from($mail['email'], Setting::get('site_name').' contact form');
            $m->to(Setting::get('support_email'))->subject(Setting::get('site_name').' support: '.$mail['subject']);
        });

        if ($emailStatus) {
            Flash::success('Thanks for contacting us! We will get back to you soon.');
        }

        return redirect()->back();
    }

    public function sharePost($post_id)
    {
        $post = Post::where('id', '=', $post_id)->first();

        //Redirect to home page if post doesn't exist
        if ($post == null) {
            return redirect('/');
        }
        $theme = Theme::uses('default')->layout('share');

        return $theme->scope('share-post', compact('post'))->render();
    }
	
    public function gallery()
    {
		
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('home');
        $theme->setTitle(trans('common.gallery').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('pages/galleries')->render();
    }

    public function post()
    {
		
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('home');
        $theme->setTitle(trans('common.news').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('pages/blog')->render();
    }
}
