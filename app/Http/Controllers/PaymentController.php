<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Auth;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;

/** All Paypal Details class **/
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use App\Transactions;
use App\User;
use PayPal\Api\PaymentExecution;

use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use DB;
use Redirect;
use Session;
use URL;

class PaymentController extends Controller
{
    private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);

    }
    public function index()
    {
        return view('paywithpaypal');
    }
    public function paywithpaypal()
    {
       // dd($request); Request $request
    //    \Session::put('joinMassAmt', $request->get('amount'));
     //   \Session::put('joinMassCur', 'SGD');

        $amt = \Session::get('joinMassAmt');
        $cur = \Session::get('joinMassCur');
        $uid = \Session::get('joinMassUID');

        if( !isset($amt)|| !isset($cur) || !isset($uid)){
        dd($uid);
            \Session::put('joinMassUID', null);
            \Session::put('joinMassAmt', null);
            \Session::put('joinMassCur', null);
             \Session::put('error', 'System Error');
             return Redirect::to('/');
        }

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();

        $item_1->setName('Joining Fee MAAS Membership') /** item name **/
            ->setCurrency($cur)
            ->setQuantity(1)
            ->setPrice($amt); /** unit price **/

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency($cur)
            ->setTotal($amt);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Membership Fee');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::to('/payment/status')) /** Specify return URL **/
            ->setCancelUrl(URL::to('/payment/status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/
        try {

            $payment->create($this->_api_context);

        } catch (\PayPal\Exception\PPConnectionException $ex) {

            if (\Config::get('app.debug')) {

                \Session::put('error', 'Connection timeout');
                return Redirect::to('/');

            } else {

                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::to('/');

            }

        }

        foreach ($payment->getLinks() as $link) {

            if ($link->getRel() == 'approval_url') {

                $redirect_url = $link->getHref();
                break;

            }

        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());

        if (isset($redirect_url)) {

            /** redirect to paypal **/
            return Redirect::away($redirect_url);

        }

        \Session::put('error', 'Unknown error occurred');
        return Redirect::to('/');

    }

    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {

            \Session::put('error', 'Payment failed');
           // return Redirect::to('/');

        }
       // dd($payment_id);

        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));

        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        $payId = Transactions::where(['pay_id'=>$result->id])->first();
        
        $jmat = \Session::get('joinMassAmt');
        $jmcr = \Session::get('joinMassCur');
        $uid = \Session::get('joinMassUID');
        $utm = \Session::get('joinMassTM');

        if(empty($payId)){
            $data = array(
                        'pay_id'=>$result->id,
                        'pay_intent'=>$result->intent,
                        'status'=>$result->state,
                        'cart_id'=>$result->cart,
                        'merchant_id'=>$result->transactions[0]->payee->merchant_id,
                        'email'=>$result->payer->payer_info->email,
                        'first_name'=>$result->payer->payer_info->first_name,
                        'last_name'=>$result->payer->payer_info->last_name,
                        'payer_id'=>$result->payer->payer_info->payer_id,
                        'amount'=>$result->transactions[0]->amount->total,
                        'currency'=>$result->transactions[0]->amount->currency,
                        'create_time'=>$result->create_time,
                        'update_time'=>$result->update_time,
                        'user_id'=>$uid,
                    );

            DB::table('transactions')->insert($data);
        }


      

        if ($result->getState() == 'approved' && $result->transactions[0]->amount->total == $jmat && $result->transactions[0]->amount->currency == $jmcr) {

            \Session::put('success', 'Payment success');

                DB::table('users')
            ->where('id', $uid)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('payment_status' => 1));  // update the record in the DB.

            $user = User::find($uid);
            Auth::login($user);
            return Redirect::to('/'.$utm);

        }

        \Session::put('error', 'Payment failed');
      //  return Redirect::to('/');

    }


}