<section id="slider">
   <div id="hero-wrapper">
      <div class="carousel-wrapper">
         <div id="hero-carousel" class="carousel slide carousel-fade ">
            <div class="carousel-inner">
		@foreach($sliders as $key=>$slider)
               <div class="item {{ $key ==  0 ? 'active' : ''  }}">
                  <img src="{{ url('images/slider/'.$slider->images) }}">
               </div>
			   
@endforeach 
            
            </div>
         </div>
      </div>
      <div class="container posrel">
         <div class="homeSrc">
            <h3>Find your Nearest Makeup Artist</h3>
            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</h4>

            <form method="POST" action="{{ url('/results/') }}">
                        {{ csrf_field() }}
            <div class="container srcbox">
               <div class="col-lg-5 col-md-5 col-sm-10 srcboxl">
                  <!--<i class="fa fa-calendar-check-o"></i>-->
                  <select id="srctxt" name="category">
                     <option value="" disabled selected>Find makeup artist</option>
                     @foreach($categories as $category)
                     <option value="{{$category->id}}">{{$category->name}}</option>
                     @endforeach
                  </select>
               </div>
			    <div class="col-lg-5 col-md-5 col-sm-10 srcboxl">
                  <!--<i class="fa fa-calendar-check-o"></i>-->
                   <select id="srctxt" name="location">
                     <option value="" disabled selected>Select Location</option>
                     @foreach($cities as $city)
                     <option value="{{$city->city_key}}">{{$city->city_name}}</option>
                     @endforeach
                  </select>
               </div>
               <div class="col-lg-2 col-md-2 col-sm-2 srcboxr">
                  <input type="submit" class="btn" value="Search Now">
               </div>
            </div>
            </form>

            <div class="container popsrc">
               <span>Popular Searches:</span>
               <ul>
                  <li><a href="#">Lorem ipsum</a></li>
                  <li><a href="#">dolor sit</a></li>
                  <li><a href="#">sit amet</a></li>
                  <li><a href="#">consectetur adipiscing</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>
<section id="intro">
   <div class="container">
      <div class="ror">
         <div class="col-md-8 col-md-offset-2">
            <h2 class="hk1">WHY MASS</h2>
            <h3  class="hk2">Professionally Personalized</h3>
            <h3  class="hk3">YOUR BEAUTY IS PERSONAL</h3>
            <p>Join the fastest growing market place for makeup artists and beauty professionals in Singapore. Receive job leads, manage your portfolio and your appointment calendar right within your profile. Get found easily by clients and let your work speak for you through client testimonials. Enjoy discounted prices on workshops, cosmetic products and learn business tips from experts. Enjoy exclusive industry invites and make your place among the elite.</p>
            <a href="#">VIEW SIGNATURE SERVICES</a>
         </div>
      </div>
   </div>
</section>
<div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1586899744000">
   <div class="wpb_column vc_column_container vc_col-sm-3">
      <div class="vc_column-inner">
         <div class="wpb_wrapper"></div>
      </div>
   </div>
   <div class="wpb_column vc_column_container vc_col-sm-6">
      <div class="vc_column-inner">
         <div class="wpb_wrapper">
            <div class="wpb_text_column wpb_content_element ">
               <div class="wpb_wrapper">
                  <!--<h2 style="text-align: center; color: #ff9b8f;"><strong>Make Up Artist search Here</strong></h2>-->
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="wpb_column vc_column_container vc_col-sm-3">
      <div class="vc_column-inner">
         <div class="wpb_wrapper"></div>
      </div>
   </div>
</div>
<div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-parallax="1.4" data-vc-parallax-image="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/bg-parallax3.jpg" class="vc_row wpb_row vc_row-fluid vc_custom_1586899825178 vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving bg-image-ps-center" style="position: relative;box-sizing: border-box;width: 1349px;padding-left: 89.5px;padding-right: 89.5px;">
   <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-5 vc_col-md-5">
      <div class="vc_column-inner vc_custom_1538899828956">
         <div class="wpb_wrapper">
            <div id="ct-space-5ed16a81f2fbf">
               <style type="text/css">
                  @media screen and (min-width: 1200px) {
                  #ct-space-5ed16a81f2fbf .ct-space {
                  height: 82px;
                  }
                  }
                  @media (min-width: 992px) and (max-width: 1991px) {
                  #ct-space-5ed16a81f2fbf .ct-space {
                  height: 82px;
                  }
                  }
                  @media (min-width: 768px) and (max-width: 991px) {
                  #ct-space-5ed16a81f2fbf .ct-space {
                  height: 72px;
                  }
                  }
                  @media screen and (max-width: 767px) {
                  #ct-space-5ed16a81f2fbf .ct-space {
                  height: 72px;
                  }
                  }
               </style>
               <div class="ct-space"></div>
            </div>
            <div id="ct-space-5ed16a82182fe">
               <style type="text/css">
                  @media screen and (min-width: 1200px) {
                  #ct-space-5ed16a82182fe .ct-space {
                  height: 99px;
                  }
                  }
                  @media (min-width: 992px) and (max-width: 1991px) {
                  #ct-space-5ed16a82182fe .ct-space {
                  height: 99px;
                  }
                  }
               </style>
               <div class="ct-space"></div>
            </div>
            <div id="ct-heading" class="ct-heading align-left align-left-md align-left-sm align-left-xs ">
               <h3 class="ct-heading-tag " style="color:#232323; ">
                  Our Vision                        
               </h3>
            </div>
            <div id="ct-heading-2" class="ct-heading align-left align-left-md align-left-sm align-left-xs ">
               <style type="text/css">
                  @media (min-width: 991px) and (max-width: 1200px) {
                  #ct-heading-2 .ct-heading-tag {
                  font-size: 18px !important;
                  line-height: 28px !important;
                  }
                  }
               </style>
               <style type="text/css">
                  @media (min-width: 768px) and (max-width: 991px) {
                  #ct-heading-2 .ct-heading-tag {
                  font-size: 18px !important;
                  line-height: 28px !important;
                  }
                  }
               </style>
               <style type="text/css">
                  @media screen and (max-width: 767px) {
                  #ct-heading-2 .ct-heading-tag {
                  font-size: 16px !important;
                  line-height: 24px !important;
                  }
                  }
               </style>
               <p class="ct-heading-tag " style="color:#232323;font-size:18px;line-height:28px; ">
                  Makeup artist association of Singapore’s vision is for the makeup artistry industry in Singapore to be seen as an attractive choice of employment and the makeup artistry community to be seen as a valued and contributing members of the society.                        
               </p>
            </div>
            <div id="ct-heading-3" class="ct-heading align-left align-left-md align-left-sm align-left-xs ">
               <h3 class="ct-heading-tag " style="color:#232323; ">
                  Our mission                        
               </h3>
            </div>
            <div id="ct-heading-4" class="ct-heading align-left align-left-md align-left-sm align-left-xs ">
               <style type="text/css">
                  @media (min-width: 991px) and (max-width: 1200px) {
                  #ct-heading-4 .ct-heading-tag {
                  font-size: 18px !important;
                  line-height: 28px !important;
                  }
                  }
               </style>
               <style type="text/css">
                  @media (min-width: 768px) and (max-width: 991px) {
                  #ct-heading-4 .ct-heading-tag {
                  font-size: 18px !important;
                  line-height: 28px !important;
                  }
                  }
               </style>
               <style type="text/css">
                  @media screen and (max-width: 767px) {
                  #ct-heading-4 .ct-heading-tag {
                  font-size: 16px !important;
                  line-height: 24px !important;
                  }
                  }
               </style>
               <p class="ct-heading-tag " style="color:#232323;font-size:18px;line-height:28px; ">
                  Bringing the makeup artistry community in Singapore together and providing them with opportunities to earn, learn and share.                        
               </p>
            </div>
            <div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>
            <div class="ct-button-wrapper align-left align-left-md align-left-sm align-left-xs ct-btn-group ">
               <a href="http://barnco99.com/makeupartistassociation/about-us-2/" target="_self" class="btn btn-default size-default">
               <span>About US</span>
               <i class="ti-arrow-right"></i>
               </a>
            </div>
            <div class="vc_empty_space" style="height: 60px"><span class="vc_empty_space_inner"></span></div>
         </div>
      </div>
   </div>
   <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-7 vc_col-md-7 rm-padding-lg">
      <div class="vc_column-inner vc_custom_1538899837219">
         <div class="wpb_wrapper">
            <div id="ct-space-5ed16a825f8da">
               <style type="text/css">
                  @media screen and (min-width: 1200px) {
                  #ct-space-5ed16a825f8da .ct-space {
                  height: 82px;
                  }
                  }
                  @media (min-width: 992px) and (max-width: 1991px) {
                  #ct-space-5ed16a825f8da .ct-space {
                  height: 82px;
                  }
                  }
               </style>
               <div class="ct-space"></div>
            </div>
            <div class="vc_row wpb_row vc_inner vc_row-fluid">
               <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-6 rm-padding-lg">
                  <div class="vc_column-inner vc_custom_1539168350559" style="margin-left:60px;">
                     <div class="wpb_wrapper">
                        <div class="wpb_single_image wpb_content_element vc_align_left  wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp vc_custom_1538899471234 inherit inherit wpb_start_animation animated">
                           <figure class="wpb_wrapper vc_figure">
                              <div class="vc_single_image-wrapper border-shadow hover-scale  vc_box_border_grey"><img src="{{ asset('images/about2.jpg') }}" class="vc_single_image-img attachment-full wp-post-image" style="width:233px; height:233px;"></div>
                           </figure>
                        </div>
                        <div class="vc_empty_space"><span class="vc_empty_space_inner"></span></div>
                        <div class="wpb_single_image wpb_content_element vc_align_left  wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp vc_custom_1538899480996 inherit inherit wpb_start_animation animated">
                           <figure class="wpb_wrapper vc_figure">
                              <div class="vc_single_image-wrapper border-shadow hover-scale  vc_box_border_grey"><img src="{{ asset('images/about3.jpg') }}" class="vc_single_image-img attachment-full wp-post-image" alt="" style="width:233px; height:275px;"></div>
                           </figure>
                        </div>
                        <div id="ct-space-5ed16a8266a3a">
                           <style type="text/css">
                           </style>
                           <div class="ct-space"></div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-6 rm-padding-lg">
                  <div class="vc_column-inner vc_custom_1539168356826">
                     <div class="wpb_wrapper">
                        <div class="wpb_single_image wpb_content_element vc_align_left  wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp vc_custom_1538899491282 inherit inherit wpb_start_animation animated">
                           <figure class="wpb_wrapper vc_figure">
                              <div class="vc_single_image-wrapper border-shadow hover-scale  vc_box_border_grey"><img src="{{ asset('images/about4.jpg') }}" class="vc_single_image-img attachment-full wp-post-image" alt="" style="width:233px; height:275px;"></div>
                           </figure>
                        </div>
                        <div id="ct-space-5ed16a82674b2">
                           <style type="text/css">
                              @media (min-width: 768px) and (max-width: 991px) {
                              #ct-space-5ed16a82674b2 .ct-space {
                              height: 30px;
                              }
                              *, ::after, ::before {
                              box-sizing: border-box;
                              }
                              }
                              @media screen and (max-width: 767px) {
                              #ct-space-5ed16a82674b2 .ct-space {
                              height: 30px;
                              }
                              }
                           </style>
                           <div class="ct-space"></div>
                        </div>
                        <div class="wpb_single_image wpb_content_element vc_align_left  wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp vc_custom_1538899499599 inherit inherit wpb_start_animation animated">
                           <figure class="wpb_wrapper vc_figure">
                              <div class="vc_single_image-wrapper border-shadow hover-scale  vc_box_border_grey"><img src="{{ asset('images/about5.jpg') }}" class="vc_single_image-img attachment-full wp-post-image" alt="" style="width:233px; height:219px;"></div>
                           </figure>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="vc_parallax-inner skrollable skrollable-between" data-bottom-top="top: -40%;" data-top-bottom="top: 0%;" style="height: 140%; background-image: url(&quot;http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/bg-parallax3.jpg&quot;); top: -37.0596%;"></div>
</div>
<section class="section section-lg bg-default text-center">
   <div class="container">
      <h2 class="h2Head">Our Services</h2>
      <div class="divider-lg"></div>
   </div>
   <div class="container">
   <div id="carousel-example-generic" class="carousel slide">
      <ol class="carousel-indicators">
         <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
         <li data-target="#carousel-example-generic" data-slide-to="1"></li>
         <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
         <div class="item active">
            <div class="row">
               <div class="col-sm-4 col-xs-8 item-featured">
                  <img src="{{ asset('images/blog4-1-500x500.jpg')}}" width="500" height="500" class="services-circle" alt=""/>
                  <div class="item-holder">
                     <div class="item-meta">
                        <h3 class="item-title">
                           <a href="#">Marketplace for makeup artists</a>
                        </h3>
                        <div class="item-content">
                           Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...                                
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-4 col-xs-8 item-featured">
                  <img src="{{ asset('images/blog3-1-500x500.jpg')}}" width="500" height="500" class="services-circle" alt=""/>
                  <div class="item-holder">
                     <div class="item-meta">
                        <h3 class="item-title">
                           <a href="#">Skills upgrade Workshops</a>
                        </h3>
                        <div class="item-content">
                           Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...                                
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-4 col-xs-8 item-featured">
                  <img src="{{ asset('images/blog1-1-500x500.jpg')}}" width="500" height="500" class="services-circle" alt=""/>
                  <div class="item-holder">
                     <div class="item-meta">
                        <h3 class="item-title">
                           <a href="#">Networking events</a>
                        </h3>
                        <div class="item-content">
                           Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...                                
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="item">
            <div class="row">
               <div class="col-sm-4 col-xs-8 item-featured">
                  <img src="{{ asset('images/blog4-1-500x500.jpg')}}" width="500" height="500" class="services-circle" alt=""/>
                  <div class="item-holder">
                     <div class="item-meta">
                        <h3 class="item-title">
                           <a href="#">makeup artists</a>
                        </h3>
                        <div class="item-content">
                           Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...                                
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-4 col-xs-8 item-featured">
                  <img src="{{ asset('images/blog3-1-500x500.jpg')}}" width="500" height="500" class="services-circle" alt=""/>
                  <div class="item-holder">
                     <div class="item-meta">
                        <h3 class="item-title">
                           <a href="#">Makeup exam and accreditation</a>
                        </h3>
                        <div class="item-content">
                           Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...                                
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-4 col-xs-8 item-featured">
                  <img src="{{ asset('images/blog1-1-500x500.jpg')}}" width="500" height="500" class="services-circle" alt=""/>
                  <div class="item-holder">
                     <div class="item-meta">
                        <h3 class="item-title">
                           <a href="#">Makeup courses</a>
                        </h3>
                        <div class="item-content">
                           Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...                                
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="item">
            <div class="row">
               <div class="col-sm-4 col-xs-8 item-featured">
                  <img src="{{ asset('images/blog4-1-500x500.jpg')}}" class="services-circle" width="500" height="500" alt=""/>
                  <div class="item-holder">
                     <div class="item-meta">
                        <h3 class="item-title">
                           <a href="#">Marketplace for makeup artists</a>
                        </h3>
                        <div class="item-content">
                           Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...                                
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-4 col-xs-8 item-featured">
                  <img src="{{ asset('images/blog3-1-500x500.jpg')}}" class="services-circle" width="500" height="500" alt=""/>
                  <div class="item-holder">
                     <div class="item-meta">
                        <h3 class="item-title">
                           <a href="#">makeup artists</a>
                        </h3>
                        <div class="item-content">
                           Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...                                
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-4 col-xs-8 item-featured">
                  <img src="{{ asset('images/blog1-1-500x500.jpg')}}" class="services-circle" width="500" height="500" alt=""/>
                  <div class="item-holder">
                     <div class="item-meta">
                        <h3 class="item-title">
                           <a href="#">Skills upgrade Workshops</a>
                        </h3>
                        <div class="item-content">
                           Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...                                
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"> 
         <span class="icon-prev"></span> 
         </a>
         <a class="right carousel-control" href="#carousel-example-generic" data-slide="next"> 
         <span class="icon-next"></span> 
         </a>
      </div>
   </div>
</section>
<section id="featbox" class="section section-lg bg-default text-center">
   <div class="container">
      <h2 class="h2Head">Our Featured Makeup Artists</h2>
      <div class="divider-lg"></div>
   </div>
   <div id="myCarousel" class="carousel slide">
   <div class="carousel-inner">
   <div class="item active">
      <div class="row">
         <div class="col-lg-4">
            <div class="team-minimal team-minimal-type-2">
               <a href="#x"><img src="{{ asset('images/team1.jpg')}}" alt="Image" class="img-responsive" width="370" height="370"></a>
                  <h4 class="team-title"><a href="">CAROL OLIVER</a></h4>
                  <p>Senior Masseur</p>
            </div>
         </div>
         <div class="col-lg-4">
            <div class="team-minimal team-minimal-type-2">
               <a href="#x"><img src="{{ asset('images/team2.jpg')}}" alt="Image" class="img-responsive" width="370" height="370"></a>
                  <h4 class="team-title"><a href="team-member-profile.html">MICHELLE BAILEY</a></h4>
                  <p>Senior Masseur</p>
            </div>
         </div>
         <div class="col-lg-4">
            <div class="team-minimal team-minimal-type-2">
               <a href="#x"><img src="{{ asset('images/team3.jpg')}}" alt="Image" class="img-responsive" width="370" height="370"></a>
                  <h4 class="team-title"><a href="team-member-profile.html">PAUL BRENNAN</a></h4>
                  <p>Senior Masseur</p>
               </div>
            </div>
         </div>
      </div>
      <div class="item">
         <div class="row">
            <div class="col-lg-4">
               <div class="team-minimal team-minimal-type-2">
                  <a href="#x"><img src="{{ asset('images/team1.jpg')}}" alt="Image" class="img-responsive" width="370" height="370"></a>
                     <h4 class="team-title"><a href="team-member-profile.html">CAROL OLIVER</a></h4>
                     <p>Senior Masseur</p>
               </div>
            </div>
            <div class="col-lg-4">
               <div class="team-minimal team-minimal-type-2">
                  <a href="#x"><img src="{{ asset('images/team2.jpg')}}" alt="Image" class="img-responsive" width="370" height="370"></a>
                     <h4 class="team-title"><a href="team-member-profile.html">MICHELLE BAILEY</a></h4>
                     <p>Senior Masseur</p>
               </div>
            </div>
            <div class="col-lg-4">
               <div class="team-minimal team-minimal-type-2">
                  <a href="#x"><img src="{{ asset('images/team3.jpg')}}" alt="Image" class="img-responsive" width="370" height="370"></a>
                     <h4 class="team-title"><a href="team-member-profile.html">PAUL BRENNAN</a></h4>
                     <p>Senior Masseur</p>
               </div>
            </div>
         </div>
      </div>
      <!--<a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="fa fa-chevron-left fa-4"></i></a>
         <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="fa fa-chevron-right fa-4"></i></a>-->
   </div>
</section>
<!--<section id="featbox" class="section section-lg bg-default text-center">
   <div class="container">
      <h2 class="h2Head">Our Featured Makeup Artists</h2>
      <div class="divider-lg"></div>
   </div>
   <div class="container">
      <div class="row no-gutters">
         <div class="col-lg-4">
            <div class="team-minimal team-minimal-type-2" data-effect="slide-bottom" >
               <figure><img src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team1.jpg" alt="" width="370" height="370"></figure>
               <div class="team-minimal-caption">
                  <h4 class="team-title"><a href="team-member-profile.html">CAROL OLIVER</a></h4>
                  <p>Senior Masseur</p>
               </div>
            </div>
         </div>
         <div class="col-lg-4">
            <div class="team-minimal team-minimal-type-2" data-effect="slide-bottom" >
               <figure><img src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team2.jpg" alt="" width="370" height="370"></figure>
               <div class="team-minimal-caption">
                  <h4 class="team-title"><a href="team-member-profile.html">MICHELLE BAILEY</a></h4>
                  <p>Senior Masseur</p>
               </div>
            </div>
         </div>
         <div class="col-lg-4">
            <div class="team-minimal team-minimal-type-2" data-effect="slide-bottom" >
               <figure><img src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team3.jpg" alt="" width="370" height="370"></figure>
               <div class="team-minimal-caption">
                  <h4 class="team-title"><a href="team-member-profile.html">PAUL BRENNAN</a></h4>
                  <p>Senior Masseur</p>
               </div>
            </div>
         </div>
      </div>
   </div>
   </section>-->
<section id="blogbox" class="section section-lg bg-default text-center">
   <div class="container">
      <h2 class="h2Head">Our Latest Blog</h2>
      <div class="divider-lg"></div>
   </div>
   <div class="container">
      <div class="row">
	  @foreach($blog as $row)
		  
	  
         <div class="col-lg-4">
            <div class="latest_post_two_image">
               <a href="#">
               <img src="{{ url('images/blogs/'.$row->images) }}" alt="a" width="420" height="300">
               </a>
            </div>
            <div class="latest_post_two_inner">
               <div class="latest_post_two_text">
                  <h5 itemprop="name" class="latest_post_two_title entry_title">
                     <a itemprop="url" href="">{{ $row->title }}</a>
                  </h5>
                  <div class="separator small left "></div>
                  <p itemprop="description" class="latest_post_two_excerpt">{{ str_limit($row->description,50)}}</p>
               </div>
               <div class="latest_post_two_info">
                  Read More <i class="fa fa-arrow-right"></i>
               </div>
            </div>
         </div>
	  @endforeach
      </div>
   </div>
</section>
<section id="bottomScroller">
   <div class="top-gallery">
   <div class="ct-carousel owl-carousel images-light-box-carousel owl-loaded owl-drag" data-item-xs="4" data-item-sm="6" data-item-md="8" data-item-lg="10" data-margin="0" data-loop="true" data-autoplay="true" data-autoplaytimeout="5000" data-smartspeed="250" data-center="false" data-arrows="false" data-bullets="false" data-stagepadding="0" data-stagepaddingsm="0" data-rtl="false">
      <div class="owl-stage-outer">
         <div class="owl-stage" style="transform: translate3d(-1483px, 0px, 0px); transition: all 0.25s ease 0s; width: 4048px;">
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic1.jpg') }}"><img src="{{asset('images/pic1-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic2.jpg') }}"><img src="{{ asset('images/pic2-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic3.jpg') }}"><img src="{{ asset('images/pic3-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic4.jpg') }}"><img src="{{ asset('images/pic4-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic5.jpg') }}"><img src="{{ asset('images/pic5-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic6.jpg') }}"><img src="{{ asset('images/pic6-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic7.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic8.jpg') }}"><img src="{{ asset('images/pic8-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic9.jpg') }}"><img src="{{ asset('images/pic9-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic10.jpg') }}"><img src="{{ asset('images/pic10-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic1.jpg') }}"><img src="{{ asset('images/pic1-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic2.jpg') }}"><img src="{{ asset('images/pic2-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic3.jpg') }}"><img src="{{ asset('images/pic3-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic4.jpg') }}"><img src="{{ asset('images/pic4-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic5.jpg') }}')"><img src="{{ asset('images/pic5-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic6.jpg') }}"><img src="{{ asset('images/pic6-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic7.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic8.jpg') }}"><img src="{{ asset('images/pic8-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic9.jpg') }}"><img src="{{ asset('images/pic9-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic10.jpg') }}"><img src="{{ asset('images/pic10-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic1.jpg') }}"><img src="{{ asset('images/pic1-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic2.jpg') }}"><img src="{{ asset('images/pic2-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic3.jpg') }}"><img src="{{ asset('images/pic3-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic4.jpg') }}"><img src="{{ asset('images/pic4-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic5.jpg') }}"><img src="{{ asset('images/pic5-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic6.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic7.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic8.jpg') }}"><img src="{{ asset('images/pic8-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic9.jpg') }}"><img src="{{ asset('images/pic9-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic10.jpg') }}"><img src="{{ asset('images/pic10-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
         </div>
      </div>
      <div class="owl-nav disabled">
         <div class="owl-prev"><i class="ti-angle-left"></i></div>
         <div class="owl-next"><i class="ti-angle-right"></i></div>
      </div>
      <div class="owl-dots disabled"></div>
   </div>
</div>
</section>
<!-- end section1 -->
<script type="text/javascript">
   $(function(){
     $('.carousel').carousel({
       interval: 8000,
       pause: false
     })
   });
</script>
</script>