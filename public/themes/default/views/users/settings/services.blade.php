<!-- main-section -->
<!-- <div class="main-content"> -->
<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="post-filters">
					{!! Theme::partial('usermenu-settings') !!}
				</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
				
					<div class="panel-heading no-bg panel-settings">
					@include('flash::message')
						<h3 class="panel-title">
							{{ trans('common.my_services') }}
						</h3>
					</div>
					<div class="panel-body nopadding">
						<div class="socialite-form">							
							<form method="POST" action="{{ url('/'.$username.'/settings/services/') }}">
								{{ csrf_field() }}
								<div class="row">
									<div class="col-md-6">

										<fieldset class="form-group required {{ $errors->has('username') ? ' has-error' : '' }}">
											{{ Form::label('username', 'Service') }}
											
										</fieldset>
										
									</div>
									<div class="col-md-6">
										<fieldset class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
											{{ Form::label('name', 'Price') }}
										</fieldset>
									</div>
								</div>
								@foreach($categories as $item)
								<div class="row">
									<div class="col-md-6">

										<fieldset class="form-group">
											{{ Form::label('name', $item->name) }} :
										</fieldset>
										
									</div>
									<div class="col-md-6">
										<fieldset class="form-group">
											@if(array_key_exists($item->id,$services))
											{{ Form::text('cat_'.$item->id, $services[$item->id], ['class' => 'form-control','name'=>'cat_'.$item->id, 'placeholder' => 'price']) }}
											@else
											{{ Form::text('cat_'.$item->id, '0', ['class' => 'form-control','name'=>'cat_'.$item->id, 'placeholder' => 'price']) }}
											@endif
										</fieldset>
									</div>
								</div>
								@endforeach
								
									<div class="pull-right">
										{{ Form::submit(trans('common.save_changes'), ['class' => 'btn btn-success']) }}
									</div>
									<div class="clearfix"></div>
								</form>
							</div><!-- /Socialite-form -->
						</div>
					</div>
					<!-- End of first panel -->


				</div>
			</div><!-- /row -->
		</div>
	<!-- </div> --><!-- /main-content -->
