<div id="pagetitle" class="page-title bg-overlay">
   <div class="container">
      <div class="page-title-inner">
         <h1 class="page-title">Contact Us</h1>
         <ul class="ct-breadcrumb">
            <li><a class="breadcrumb-entry" href="#">Home</a></li>
            <li><span class="breadcrumb-entry">Contact Us</span></li>
         </ul>
      </div>
   </div>
</div>
<section id="down-section" class="wow fadeIn bg-light-gray" style="visibility: visible; animation-name: fadeIn;">
            <div class="container">
        <div class="row justify-content-center">
                    <div class="col-12 col-lg-7 text-center margin-100px-bottom sm-margin-40px-bottom">
                        <div class="position-relative overflow-hidden w-100">
                        <span class="text-small text-outside-line-full alt-font font-weight-600 text-uppercase"><h6><span id="typewriter1">Get In </span> Touch</h6></span>
                        </div>
                    </div>
                </div>
                <form id="contact-form-3" action="" method="post">
                    <div class="row "> 
                        <div class="col-12 col-lg-6 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                            <div class="border-radius-6 lg-padding-seven-all">
                                <div class="text-extra-dark-gray alt-font text-large font-weight-600 margin-30px-bottom">Our team is ready to answer your queries

</div> 
                                <div>
                                    <div id="success-contact-form-3" class="mx-0" style="display: none;"></div>
                                    <input type="text" name="name" id="name" placeholder="Name*" class="input-bg" required="">
                                    <input type="text" name="email" id="email" placeholder="E-mail*" class="input-bg" required="">
									<input type="text" name="phone" id="phone" placeholder="Phone*" class="input-bg" required="">
                                  
                                    <textarea name="message" id="comment" placeholder="Your Message" class="input-bg" required=""></textarea>
                                    <input id="contact-us-button-3" type="submit" value="Submit" name="save" class="btn btn-medium btn-dark-gray btn-rounded lg-margin-15px-bottom d-table d-lg-inline-block md-margin-lr-auto" required="">
                                </div>
                            </div>
                        </div>
<div class="col-12 col-lg-6 wow fadeIn" style="visibility: visible; animation-name: fadeIn; top:17px;">
 <div class=" border-radius-6 lg-padding-seven-all information-padding">
             
                    <div class="col-12 col-xl-12 col-md-12 margin-six-bottom lg-margin-six-bottom sm-margin-ten-bottom wow fadeInUp last-paragraph-no-margin" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="feature-box-5 position-relative">
                           <i class="fa fa-envelope  icon-color icon-extra-medium" aria-hidden="true"></i>
                            <div class="feature-content">
                                <div class="text-extra-dark-gray margin-10px-bottom alt-font font-weight-600 icon-color-text">E-MAIL:</div>
                                <p>info@yoursite.com</p>
                            </div>
                        </div>
                    </div>


     <div class="col-12 col-xl-12 col-md-12 margin-six-bottom lg-margin-six-bottom sm-margin-ten-bottom wow fadeInUp last-paragraph-no-margin" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="feature-box-5 position-relative">
                           <i class="fa fa-phone icon-color icon-extra-medium" aria-hidden="true"></i>
                            <div class="feature-content">
                                <div class="text-extra-dark-gray margin-10px-bottom alt-font font-weight-600 icon-color-text">PHONE:</div>
                                <p>+123 456 789</p>
                            </div>
                        </div>
                    </div>

                      <div class="col-12 col-xl-12 col-md-12 margin-six-bottom lg-margin-six-bottom sm-margin-ten-bottom wow fadeInUp last-paragraph-no-margin" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="feature-box-5 position-relative">
                           <i class="fa fa-globe icon-color icon-extra-medium" aria-hidden="true"></i>
                            <div class="feature-content">
                                <div class="text-extra-dark-gray margin-10px-bottom alt-font font-weight-600 icon-color-text">ADDRESS:</div>
                                <p>Some Fine Address, 887, Madrid, Spain.</p>
                            </div>
                        </div>
                    </div>


</div>

</div>

                    </div>
                </form>
            </div>     
        </section>
		<br/><br/>
<section class="no-padding">
<div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d510562.45330268116!2d103.56405349164459!3d1.313984343024645!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da11238a8b9375%3A0x887869cf52abf5c4!2sSingapore!5e0!3m2!1sen!2sin!4v1596449733335!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe></div>
</section>