
   

<div class="VendorList">
<div class="container">
<ul class="PrimaryVendorCard">
   @foreach($userLists as $item)
   <?php //dd($item->profile_name); ?>
      <li class="vendor-picture shadow col-md-3">
         <a href="#">
            <div class="vendor-card extra-radius">
                                    <div class="vendor-picture margin-r-10"><img class="info-icon pointer" data-tip="true" data-for="17246" src="{{ url('/user/avatar') }}/{{$item->source}}"></div>
                                    <div class="vendor-info">
                                       <div class="padding-10">
                                          <div class="line f-space-between">
                                             <div class="frow">{{$item->profile_name}}</div>
                                             <div class="nowrap"><span class="StarRating center rating-5 regular"><i class="fa fa-star margin-r-5"></i>{{$item->review_count}}</span></div>
                                          </div>
                                          <div class="line">
                                             <p class="vendor-detail"><i class="fa h6 fa-map-marker"></i>  Delhi NCR</p>
                                             <span class="review-cnt regular nowrap">{{$item->review_star}} reviews</span>
                                          </div>
                                       </div>
                                       <div class="line">
                                          <hr>
                                       </div>
                                       <div class="vendor-price frow margin-10 f-space-between">
                                          <span class="frow v-center">
                                             <div>
                                                <div class="frow">
                                                   <p class="vendor-detail text-bold text-primary v-center margin-r-5"><i class="fa h6 fa-inr margin-r-5"></i><span class="">{{$item->pd_price}}</span></p>
                                                   <p class="text-primary">per day</p>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                 </div>
         </a>
      </li>
   @endforeach
</ul>
</div>
</div>
<div class="clear"></div>