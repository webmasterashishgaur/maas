<div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-parallax="1.3" data-vc-parallax-image="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/bg-parallax2.jpg" class="vc_row wpb_row vc_row-fluid vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving bg-image-ps-inherit" style="position: relative; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;">
                           <div class="wpb_column vc_column_container vc_col-sm-12">
                              <div class="vc_column-inner vc_custom_1538839183877">
                                 <div class="wpb_wrapper">
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-8 rm-padding-md">
                                          <div class="vc_column-inner vc_custom_1538063128965">
                                             <div class="wpb_wrapper">
                                                <div id="ct-heading-6" class="ct-heading align-center align-center-md align-center-sm align-center-xs ">
                                                   <h3 class="ct-heading-tag ">
                                                      Testimonials Of Our Clients                        
                                                   </h3>
                                                   <div class="ct-heading-separator"><i class="flaticon-spa"></i></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="vc_empty_space" style="height: 45px"><span class="vc_empty_space_inner"></span></div>
                                    <div id="ct-testimonial-carousel" class="ct-testimonial-carousel default owl-carousel nav-middle skrollable skrollable-after owl-loaded owl-drag" data-item-xs="1" data-item-sm="2" data-item-md="3" data-item-lg="3" data-margin="30" data-loop="true" data-autoplay="true" data-autoplaytimeout="5000" data-smartspeed="250" data-center="false" data-arrows="true" data-bullets="true" data-stagepadding="0" data-rtl="false" data-item-xs-custom="2" style="">
                                       <div class="owl-stage-outer">
                                          <div class="owl-stage" style="transform: translate3d(-1560px, 0px, 0px); transition: all 0.25s ease 0s; width: 4680px;">
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author1-2.jpg" width="100" height="100" alt="author1" title="author1">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Joanna Wang                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author2-2.jpg" width="100" height="100" alt="author2" title="author2">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Espen Brunberg                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author3-2.jpg" width="100" height="100" alt="author3" title="author3">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         John Doe                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author1-2.jpg" width="100" height="100" alt="author1" title="author1">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Joanna Wang                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author2-2.jpg" width="100" height="100" alt="author2" title="author2">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Espen Brunberg                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author3-2.jpg" width="100" height="100" alt="author3" title="author3">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         John Doe                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author1-2.jpg" width="100" height="100" alt="author1" title="author1">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Joanna Wang                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author2-2.jpg" width="100" height="100" alt="author2" title="author2">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Espen Brunberg                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author3-2.jpg" width="100" height="100" alt="author3" title="author3">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         John Doe                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author1-2.jpg" width="100" height="100" alt="author1" title="author1">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Joanna Wang                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author2-2.jpg" width="100" height="100" alt="author2" title="author2">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Espen Brunberg                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author3-2.jpg" width="100" height="100" alt="author3" title="author3">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         John Doe                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div><div class="owl-stage-outer">
                                          <div class="owl-stage" style="transform: translate3d(-1560px, 0px, 0px); transition: all 0.25s ease 0s; width: 4680px;">
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author1-2.jpg" width="100" height="100" alt="author1" title="author1">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Joanna Wang                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author2-2.jpg" width="100" height="100" alt="author2" title="author2">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Espen Brunberg                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author3-2.jpg" width="100" height="100" alt="author3" title="author3">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         John Doe                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author1-2.jpg" width="100" height="100" alt="author1" title="author1">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Joanna Wang                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author2-2.jpg" width="100" height="100" alt="author2" title="author2">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Espen Brunberg                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author3-2.jpg" width="100" height="100" alt="author3" title="author3">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         John Doe                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author1-2.jpg" width="100" height="100" alt="author1" title="author1">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Joanna Wang                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author2-2.jpg" width="100" height="100" alt="author2" title="author2">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Espen Brunberg                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author3-2.jpg" width="100" height="100" alt="author3" title="author3">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         John Doe                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author1-2.jpg" width="100" height="100" alt="author1" title="author1">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Joanna Wang                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author2-2.jpg" width="100" height="100" alt="author2" title="author2">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Espen Brunberg                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author3-2.jpg" width="100" height="100" alt="author3" title="author3">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         John Doe                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div><div class="owl-stage-outer">
                                          <div class="owl-stage" style="transform: translate3d(-1560px, 0px, 0px); transition: all 0.25s ease 0s; width: 4680px;">
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author1-2.jpg" width="100" height="100" alt="author1" title="author1">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Joanna Wang                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author2-2.jpg" width="100" height="100" alt="author2" title="author2">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Espen Brunberg                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author3-2.jpg" width="100" height="100" alt="author3" title="author3">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         John Doe                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author1-2.jpg" width="100" height="100" alt="author1" title="author1">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Joanna Wang                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author2-2.jpg" width="100" height="100" alt="author2" title="author2">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Espen Brunberg                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author3-2.jpg" width="100" height="100" alt="author3" title="author3">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         John Doe                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author1-2.jpg" width="100" height="100" alt="author1" title="author1">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Joanna Wang                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author2-2.jpg" width="100" height="100" alt="author2" title="author2">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Espen Brunberg                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author3-2.jpg" width="100" height="100" alt="author3" title="author3">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         John Doe                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author1-2.jpg" width="100" height="100" alt="author1" title="author1">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Joanna Wang                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author2-2.jpg" width="100" height="100" alt="author2" title="author2">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Espen Brunberg                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author3-2.jpg" width="100" height="100" alt="author3" title="author3">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         John Doe                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="owl-nav">
                                          <div class="owl-prev"><i class="ti-angle-left"></i></div>
                                          <div class="owl-next"><i class="ti-angle-right"></i></div>
                                       </div>
                                       <div class="owl-dots">
                                          <div class="owl-dot active"><span></span></div>
                                          <div class="owl-dot"><span></span></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="vc_parallax-inner skrollable skrollable-before" data-bottom-top="top: -30%;" data-top-bottom="top: 0%;" style="height: 130%; background-image: url(&quot;http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/bg-parallax2.jpg&quot;); top: -30%;"></div>
                        </div>