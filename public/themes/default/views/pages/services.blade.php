<div id="pagetitle" class="page-title bg-overlay">
   <div class="container">
      <div class="page-title-inner">
         <h1 class="page-title">Our Services</h1>
         <ul class="ct-breadcrumb">
            <li><a class="breadcrumb-entry" href="#">Home</a></li>
            <li><span class="breadcrumb-entry">Our Services</span></li>
         </ul>
      </div>
   </div>
</div>
<div id="content" class="site-content">
   <div class="content-inner">
      <div class="container content-container">
         <div class="row content-row" style="display:block;">
            <div id="primary" class="content-area content-full-width col-12">
               <main id="main" class="site-main">
                  <article id="post-26" class="post-26 page type-page status-publish hentry">
                     <div class="entry-content clearfix">
                        <div class="vc_row wpb_row vc_row-fluid bg-image-ps-inherit">
                           <div class="wpb_column vc_column_container vc_col-sm-12">
                              <div class="vc_column-inner vc_custom_1538067306154">
                                 <div class="wpb_wrapper">
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-8 rm-padding-md">
                                          <div class="vc_column-inner vc_custom_1538067333010">
                                             <div class="wpb_wrapper">
                                                <div id="ct-heading" class="ct-heading align-center align-center-md align-center-sm align-center-xs ">
                                                   <h3 class="ct-heading-tag ">
                                                      Our Services                        
                                                   </h3>
                                                   <div class="ct-heading-separator"><i class="flaticon-spa"></i></div>
                                                   <div class="ct-heading-desc" style="">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the.</div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div id="ct-service-grid" class="ct-grid ct-grid-service-layout1 style1 ">
                                       <div class="ct-grid-inner ct-grid-masonry row animation-time" data-gutter="0" style="height: 644px;">
                                          <div class="grid-sizer col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12"></div>
                                          <div class="grid-item col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 " style="position: absolute; left: 0%; top: 0px;">
                                             <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated" style="animation-delay: 20ms;">
                                                <div class="item-holder" style="height: 322px;">
                                                   <div class="item-icon">
                                                      <a href="#"><i class="flaticon-barbershop"></i></a>
                                                   </div>
                                                   <h3 class="item-title">
                                                      <a href="#">makeup artists</a>
                                                   </h3>
                                                   <div class="item-content">
                                                      Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...                            
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="grid-item col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 " style="position: absolute; left: 33.2636%; top: 0px;">
                                             <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated" style="animation-delay: 50ms;">
                                                <div class="item-holder" style="height: 322px;">
                                                   <div class="item-icon">
                                                      <a href=""><i class="flaticon-makeup"></i></a>
                                                   </div>
                                                   <h3 class="item-title">
                                                      <a href="">Makeup exam and accreditation</a>
                                                   </h3>
                                                   <div class="item-content">
                                                      Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...                            
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="grid-item col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 " style="position: absolute; left: 66.6318%; top: 0px;">
                                             <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated" style="animation-delay: 80ms;">
                                                <div class="item-holder" style="height: 322px;">
                                                   <div class="item-icon">
                                                      <a href=""><i class="flaticon-makeup-1"></i></a>
                                                   </div>
                                                   <h3 class="item-title">
                                                      <a href="#">Makeup courses</a>
                                                   </h3>
                                                   <div class="item-content">
                                                      Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...                            
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="grid-item col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 " style="position: absolute; left: 0%; top: 322px;">
                                             <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated" style="animation-delay: 110ms;">
                                                <div class="item-holder" style="">
                                                   <div class="item-icon">
                                                      <a href="#"><i class="flaticon-woman-1"></i></a>
                                                   </div>
                                                   <h3 class="item-title">
                                                      <a href="#">Marketplace for makeup artists</a>
                                                   </h3>
                                                   <div class="item-content">
                                                      Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...                            
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="grid-item col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 " style="position: absolute; left: 33.2636%; top: 322px;">
                                             <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated" style="animation-delay: 140ms;">
                                                <div class="item-holder" style="height: 322px;">
                                                   <div class="item-icon">
                                                      <a href="#"><i class="flaticon-woman"></i></a>
                                                   </div>
                                                   <h3 class="item-title">
                                                      <a href="#">Skills upgrade Workshops</a>
                                                   </h3>
                                                   <div class="item-content">
                                                      Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...                            
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="grid-item col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 " style="position: absolute; left: 66.6318%; top: 322px;">
                                             <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated" style="animation-delay: 170ms;">
                                                <div class="item-holder" style="height: 322px;">
                                                   <div class="item-icon">
                                                      <a href="#"><i class="flaticon-candle-1"></i></a>
                                                   </div>
                                                   <h3 class="item-title">
                                                      <a href="#">Networking events</a>
                                                   </h3>
                                                   <div class="item-content">
                                                      Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...                            
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1538791880181 vc_row-has-fill bg-image-ps-bottom" style="position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;">
                           <div class="wpb_column vc_column_container vc_col-sm-12">
                              <div class="vc_column-inner vc_custom_1538793413178">
                                 <div class="wpb_wrapper">
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp wpb_start_animation animated" style="animation-delay: 100ms;">
                                          <div class="vc_column-inner vc_custom_1538844325130">
                                             <div class="wpb_wrapper">
                                                <div class="ct-fancybox-default-wrap">
                                                   <div class="ct-fancybox-default  ">
                                                      <div class="ct-fancybox-inner clearfix">
                                                         <div class="ct-fancybox-icon">
                                                            <i class="flaticon-woman" style=""></i>
                                                         </div>
                                                         <div class="ct-fancybox-content">
                                                            <h3 class="ct-fancybox-title" style="">
                                                               We are Professional                    
                                                            </h3>
                                                            <div class="ct-fancybox-desc" style="">
                                                               Lorem Ipsum is simply dummy text of the printing and typesetting industry.                
                                                            </div>
                                                            <div class="ct-fancybox-more">
                                                               <a class="btn btn-secondary" href="#" target="_self">Site Button</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp wpb_start_animation animated" style="animation-delay: 200ms;">
                                          <div class="vc_column-inner vc_custom_1538844331469">
                                             <div class="wpb_wrapper">
                                                <div class="ct-fancybox-default-wrap">
                                                   <div class="ct-fancybox-default  ">
                                                      <div class="ct-fancybox-inner clearfix">
                                                         <div class="ct-fancybox-icon">
                                                            <i class="flaticon-mortar" style=""></i>
                                                         </div>
                                                         <div class="ct-fancybox-content">
                                                            <h3 class="ct-fancybox-title" style="">
                                                               Lux Cosmetic                    
                                                            </h3>
                                                            <div class="ct-fancybox-desc" style="">
                                                               Lorem Ipsum is simply dummy text of the printing and typesetting industry.                
                                                            </div>
                                                            <div class="ct-fancybox-more">
                                                               <a class="btn btn-secondary" href="#" target="_self">Site Button</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp wpb_start_animation animated" style="animation-delay: 300ms;">
                                          <div class="vc_column-inner vc_custom_1538844341959">
                                             <div class="wpb_wrapper">
                                                <div class="ct-fancybox-default-wrap">
                                                   <div class="ct-fancybox-default  ">
                                                      <div class="ct-fancybox-inner clearfix">
                                                         <div class="ct-fancybox-icon">
                                                            <i class="flaticon-candle" style=""></i>
                                                         </div>
                                                         <div class="ct-fancybox-content">
                                                            <h3 class="ct-fancybox-title" style="">
                                                               Medical Education                    
                                                            </h3>
                                                            <div class="ct-fancybox-desc" style="">
                                                               Lorem Ipsum is simply dummy text of the printing and typesetting industry.                
                                                            </div>
                                                            <div class="ct-fancybox-more">
                                                               <a class="btn btn-secondary" href="#" target="_self">Site Button</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp wpb_start_animation animated" style="animation-delay: 400ms;">
                                          <div class="vc_column-inner vc_custom_1538844351372">
                                             <div class="wpb_wrapper">
                                                <div class="ct-fancybox-default-wrap">
                                                   <div class="ct-fancybox-default  ">
                                                      <div class="ct-fancybox-inner clearfix">
                                                         <div class="ct-fancybox-icon">
                                                            <i class="flaticon-sauna-1" style=""></i>
                                                         </div>
                                                         <div class="ct-fancybox-content">
                                                            <h3 class="ct-fancybox-title" style="">
                                                               The Newest Equipment                    
                                                            </h3>
                                                            <div class="ct-fancybox-desc" style="">
                                                               Lorem Ipsum is simply dummy text of the printing and typesetting industry.                
                                                            </div>
                                                            <div class="ct-fancybox-more">
                                                               <a class="btn btn-secondary" href="#" target="_self">Site Button</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1538834879315 vc_row-has-fill bg-image-ps-center" style="position: relative;left: -89.5px;box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;">
                           <div class="wpb_column vc_column_container vc_col-sm-12">
                              <div class="vc_column-inner vc_custom_1538794072383">
                                 <div class="wpb_wrapper">
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-8 rm-padding-md">
                                          <div class="vc_column-inner vc_custom_1538067333010">
                                             <div class="wpb_wrapper">
                                                <div id="ct-heading-2" class="ct-heading align-center align-center-md align-center-sm align-center-xs ">
                                                   <h3 class="ct-heading-tag ">
                                                      Our Professional Team                        
                                                   </h3>
                                                   <div class="ct-heading-separator"><i class="flaticon-spa"></i></div>
                                                   <div class="ct-heading-desc" style="">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the.</div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="ct-team-carousel-wrap">
                                       <div id="ct-team-carousel" class="ct-team-carousel default owl-carousel nav-middle   wpb_animate_when_almost_visible wpb_fadeIn fadeIn skrollable skrollable-after owl-loaded owl-drag wpb_start_animation animated" data-item-xs="1" data-item-sm="3" data-item-md="3" data-item-lg="5" data-margin="30" data-loop="true" data-autoplay="true" data-autoplaytimeout="5000" data-smartspeed="250" data-center="true" data-arrows="true" data-bullets="true" data-stagepadding="0" data-rtl="false" style="">
                                          <div class="owl-stage-outer">
                                             <div class="owl-stage" style="transition: all 0s ease 0s; width: 3608px; transform: translate3d(-656px, 0px, 0px);">
                                                <div class="owl-item cloned" style="width: 298px; margin-right: 30px;">
                                                   <div class="ct-team-item">
                                                      <div class="ct-team-item-inner">
                                                         <div class="team-featured">
                                                            <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team3-470x500.jpg" width="470" height="500" alt="team3" title="team3">                            
                                                            <div class="team-social-wrap">
                                                               <div class="team-social">
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="team-holder">
                                                            <h3 class="team-title">
                                                               Paul Brennan                        
                                                            </h3>
                                                            <span class="team-position">
                                                            Cosmetologist                        </span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="owl-item cloned" style="width: 298px; margin-right: 30px;">
                                                   <div class="ct-team-item">
                                                      <div class="ct-team-item-inner">
                                                         <div class="team-featured">
                                                            <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team4-470x500.jpg" width="470" height="500" alt="team4" title="team4">                            
                                                            <div class="team-social-wrap">
                                                               <div class="team-social">
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="team-holder">
                                                            <h3 class="team-title">
                                                               Bill Thompson                        
                                                            </h3>
                                                            <span class="team-position">
                                                            Cosmetologist                        </span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="owl-item cloned active" style="width: 298px; margin-right: 30px;">
                                                   <div class="ct-team-item">
                                                      <div class="ct-team-item-inner">
                                                         <div class="team-featured">
                                                            <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team5-470x500.jpg" width="470" height="500" alt="team5" title="team5">                            
                                                            <div class="team-social-wrap">
                                                               <div class="team-social">
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="team-holder">
                                                            <h3 class="team-title">
                                                               Michelle Bailey                        
                                                            </h3>
                                                            <span class="team-position">
                                                            Cosmetologist                        </span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="owl-item active center" style="width: 298px; margin-right: 30px;">
                                                   <div class="ct-team-item">
                                                      <div class="ct-team-item-inner">
                                                         <div class="team-featured">
                                                            <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team1-470x500.jpg" width="470" height="500" alt="team1" title="team1">                            
                                                            <div class="team-social-wrap">
                                                               <div class="team-social">
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="team-holder">
                                                            <h3 class="team-title">
                                                               Carol Oliver                        
                                                            </h3>
                                                            <span class="team-position">
                                                            Cosmetologist                        </span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="owl-item active" style="width: 298px; margin-right: 30px;">
                                                   <div class="ct-team-item">
                                                      <div class="ct-team-item-inner">
                                                         <div class="team-featured">
                                                            <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team2-470x500.jpg" width="470" height="500" alt="team2" title="team2">                            
                                                            <div class="team-social-wrap">
                                                               <div class="team-social">
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="team-holder">
                                                            <h3 class="team-title">
                                                               Sandra Kinder                        
                                                            </h3>
                                                            <span class="team-position">
                                                            Cosmetologist                        </span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="owl-item" style="width: 298px; margin-right: 30px;">
                                                   <div class="ct-team-item">
                                                      <div class="ct-team-item-inner">
                                                         <div class="team-featured">
                                                            <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team3-470x500.jpg" width="470" height="500" alt="team3" title="team3">                            
                                                            <div class="team-social-wrap">
                                                               <div class="team-social">
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="team-holder">
                                                            <h3 class="team-title">
                                                               Paul Brennan                        
                                                            </h3>
                                                            <span class="team-position">
                                                            Cosmetologist                        </span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="owl-item" style="width: 298px; margin-right: 30px;">
                                                   <div class="ct-team-item">
                                                      <div class="ct-team-item-inner">
                                                         <div class="team-featured">
                                                            <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team4-470x500.jpg" width="470" height="500" alt="team4" title="team4">                            
                                                            <div class="team-social-wrap">
                                                               <div class="team-social">
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="team-holder">
                                                            <h3 class="team-title">
                                                               Bill Thompson                        
                                                            </h3>
                                                            <span class="team-position">
                                                            Cosmetologist                        </span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="owl-item" style="width: 298px; margin-right: 30px;">
                                                   <div class="ct-team-item">
                                                      <div class="ct-team-item-inner">
                                                         <div class="team-featured">
                                                            <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team5-470x500.jpg" width="470" height="500" alt="team5" title="team5">                            
                                                            <div class="team-social-wrap">
                                                               <div class="team-social">
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="team-holder">
                                                            <h3 class="team-title">
                                                               Michelle Bailey                        
                                                            </h3>
                                                            <span class="team-position">
                                                            Cosmetologist                        </span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="owl-item cloned" style="width: 298px; margin-right: 30px;">
                                                   <div class="ct-team-item">
                                                      <div class="ct-team-item-inner">
                                                         <div class="team-featured">
                                                            <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team1-470x500.jpg" width="470" height="500" alt="team1" title="team1">                            
                                                            <div class="team-social-wrap">
                                                               <div class="team-social">
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="team-holder">
                                                            <h3 class="team-title">
                                                               Carol Oliver                        
                                                            </h3>
                                                            <span class="team-position">
                                                            Cosmetologist                        </span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="owl-item cloned" style="width: 298px; margin-right: 30px;">
                                                   <div class="ct-team-item">
                                                      <div class="ct-team-item-inner">
                                                         <div class="team-featured">
                                                            <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team2-470x500.jpg" width="470" height="500" alt="team2" title="team2">                            
                                                            <div class="team-social-wrap">
                                                               <div class="team-social">
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="team-holder">
                                                            <h3 class="team-title">
                                                               Sandra Kinder                        
                                                            </h3>
                                                            <span class="team-position">
                                                            Cosmetologist                        </span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="owl-item cloned" style="width: 298px; margin-right: 30px;">
                                                   <div class="ct-team-item">
                                                      <div class="ct-team-item-inner">
                                                         <div class="team-featured">
                                                            <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team3-470x500.jpg" width="470" height="500" alt="team3" title="team3">                            
                                                            <div class="team-social-wrap">
                                                               <div class="team-social">
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a class="hover-effect" href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="team-holder">
                                                            <h3 class="team-title">
                                                               Paul Brennan                        
                                                            </h3>
                                                            <span class="team-position">
                                                            Cosmetologist                        </span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="owl-nav">
                                             <div class="owl-prev"><i class="ti-angle-left"></i></div>
                                             <div class="owl-next"><i class="ti-angle-right"></i></div>
                                          </div>
                                          <div class="owl-dots">
                                             <div class="owl-dot active"><span></span></div>
                                             <div class="owl-dot"><span></span></div>
                                             <div class="owl-dot"><span></span></div>
                                             <div class="owl-dot"><span></span></div>
                                             <div class="owl-dot"><span></span></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                       <!-- <div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-parallax="1.5" data-vc-parallax-image="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/bg-parallax1-1.jpg" class="vc_row wpb_row vc_row-fluid vc_custom_1538800497675 vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving row-overlay bg-image-ps-inherit" style="position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;">
                           <div data-vc-parallax="1.5" class="wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill vc_general vc_parallax vc_parallax-content-moving">
                              <div class="vc_column-inner vc_custom_1538801703166">
                                 <div class="wpb_wrapper">
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-8 rm-padding-md">
                                          <div class="vc_column-inner vc_custom_1538800572756">
                                             <div class="wpb_wrapper">
                                                <div id="ct-heading-3" class="ct-heading align-center align-center-md align-center-sm align-center-xs ">
                                                   <h3 class="ct-heading-tag " style="margin-bottom:31px;color:#ffffff;font-weight:800; ">
                                                      Video Presentation                        
                                                   </h3>
                                                   <div class="ct-heading-desc" style="color:#ffffff;">In this video, our staff members tell about their work at Solari, how they achieve the best results for their clients every day and more. Click the Play button below to watch this presentation.</div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div id="ct-video" class="ct-video-wrapper  ">
                                       <div class="ct-video-inner">
                                          <a class="ct-video-button" href="https://www.youtube.com/watch?v=SF4aHwxHtZ0">
                                          <i class="ti-control-play"></i>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="vc_parallax-inner skrollable skrollable-after" data-bottom-top="top: -50%;" data-top-bottom="top: 0%;" style="height: 150%; top: 0%;"></div>
                           </div>
                           <div class="vc_parallax-inner skrollable skrollable-after" data-bottom-top="top: -50%;" data-top-bottom="top: 0%;" style="height: 150%; background-image: url(&quot;http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/bg-parallax1-1.jpg&quot;); top: 0%;"></div>
                        </div>-->
                        <div class="vc_row-full-width vc_clearfix"></div>
							<div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1538834813103 vc_row-has-fill bg-image-ps-center" style="position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;">
                           <div class="wpb_column vc_column_container vc_col-sm-12">
                              <div class="vc_column-inner vc_custom_1538839762078">
                                 <div class="wpb_wrapper">
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-8 rm-padding-md">
                                          <div class="vc_column-inner vc_custom_1538067333010">
                                             <div class="wpb_wrapper">
                                                <div id="ct-heading-7" class="ct-heading align-center align-center-md align-center-sm align-center-xs ">
                                                   <h3 class="ct-heading-tag ">
                                                      Our Latest Blog                        
                                                   </h3>
                                                   <div class="ct-heading-separator"><i class="flaticon-spa"></i></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div id="ct-blog-carousel" class="ct-blog-carousel layout2 ct-grid-blog-layout1 owl-carousel nav-middle skrollable skrollable-after owl-loaded owl-drag" data-item-xs="1" data-item-sm="2" data-item-md="3" data-item-lg="3" data-margin="30" data-loop="true" data-autoplay="false" data-autoplaytimeout="5000" data-smartspeed="250" data-center="false" data-arrows="true" data-bullets="false" data-stagepadding="0" data-rtl="false" style="">
                                       <div class="owl-stage-outer">
                                          <div class="owl-stage" style="transform: translate3d(-1170px, 0px, 0px); transition: all 0s ease 0s; width: 4680px;">
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog2-1-600x429.jpg" width="600" height="429" alt="An Evening with Kayla &amp; Ieva on all things Pregnancy" title="blog2">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 24, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="">An Evening with Kayla &amp; Ieva on all things Pregnancy</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog1-1-600x429.jpg" width="600" height="429" alt="Spring is in the Air and and So Our These Amazing Spa Offers" title="blog1">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 24, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="">Spring is in the Air and and So Our These Amazing</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog5-1-600x429.jpg" width="600" height="429" alt="The Correct Order to Apply Your Skincare Products" title="blog5">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 22, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">The Correct Order to Apply Your Skincare Products</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog2-1-600x429.jpg" width="600" height="429" alt="An Evening with Kayla &amp; Ieva on all things Pregnancy" title="blog2">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>November 15, 2019</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">Gutenberg Style</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog4-1-600x429.jpg" width="600" height="429" alt="10 Spas in Ireland to Get Pregnancy Spa Treatments" title="blog4">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 24, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">10 Spas in Ireland to Get Pregnancy Spa Treatments</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog3-1-600x429.jpg" width="600" height="429" alt="Christmas Customer Event Chill Spa at the Ice House" title="blog3">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 24, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">Christmas Customer Event Chill Spa at the Ice House</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog2-1-600x429.jpg" width="600" height="429" alt="An Evening with Kayla &amp; Ieva on all things Pregnancy" title="blog2">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 24, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">An Evening with Kayla &amp; Ieva on all things Pregnancy</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog1-1-600x429.jpg" width="600" height="429" alt="Spring is in the Air and and So Our These Amazing Spa Offers" title="blog1">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 24, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">Spring is in the Air and and So Our These Amazing</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog5-1-600x429.jpg" width="600" height="429" alt="The Correct Order to Apply Your Skincare Products" title="blog5">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 22, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">The Correct Order to Apply Your Skincare Products</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog2-1-600x429.jpg" width="600" height="429" alt="An Evening with Kayla &amp; Ieva on all things Pregnancy" title="blog2">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>November 15, 2019</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">Gutenberg Style</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog4-1-600x429.jpg" width="600" height="429" alt="10 Spas in Ireland to Get Pregnancy Spa Treatments" title="blog4">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 24, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">10 Spas in Ireland to Get Pregnancy Spa Treatments</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog3-1-600x429.jpg" width="600" height="429" alt="Christmas Customer Event Chill Spa at the Ice House" title="blog3">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 24, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">Christmas Customer Event Chill Spa at the Ice House</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="owl-nav">
                                          <div class="owl-prev"><i class="ti-angle-left"></i></div>
                                          <div class="owl-next"><i class="ti-angle-right"></i></div>
                                       </div>
                                       <div class="owl-dots disabled"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                     </div>
                  </article>
               </main>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
$(".owl-carousel").owlCarousel({
      autoPlay: 3000,
      items : 1, // THIS IS IMPORTANT
      responsive : {
            480 : { items : 1  }, // from zero to 480 screen width 4 items
            768 : { items : 2  }, // from 480 screen widthto 768 6 items
            1024 : { items : 3   // from 768 screen width to 1024 8 items
            }
        },
  });
  </script>
<div class="top-gallery">
   <div class="ct-carousel owl-carousel images-light-box-carousel owl-loaded owl-drag" data-item-xs="4" data-item-sm="6" data-item-md="8" data-item-lg="10" data-margin="0" data-loop="true" data-autoplay="true" data-autoplaytimeout="5000" data-smartspeed="250" data-center="false" data-arrows="false" data-bullets="false" data-stagepadding="0" data-stagepaddingsm="0" data-rtl="false">
      <div class="owl-stage-outer">
         <div class="owl-stage" style="transform: translate3d(-1483px, 0px, 0px); transition: all 0.25s ease 0s; width: 4048px;">
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic1.jpg') }}"><img src="{{asset('images/pic1-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic2.jpg') }}"><img src="{{ asset('images/pic2-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic3.jpg') }}"><img src="{{ asset('images/pic3-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic4.jpg') }}"><img src="{{ asset('images/pic4-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic5.jpg') }}"><img src="{{ asset('images/pic5-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic6.jpg') }}"><img src="{{ asset('images/pic6-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic7.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic8.jpg') }}"><img src="{{ asset('images/pic8-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic9.jpg') }}"><img src="{{ asset('images/pic9-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic10.jpg') }}"><img src="{{ asset('images/pic10-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic1.jpg') }}"><img src="{{ asset('images/pic1-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic2.jpg') }}"><img src="{{ asset('images/pic2-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic3.jpg') }}"><img src="{{ asset('images/pic3-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic4.jpg') }}"><img src="{{ asset('images/pic4-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic5.jpg') }}')"><img src="{{ asset('images/pic5-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic6.jpg') }}"><img src="{{ asset('images/pic6-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic7.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic8.jpg') }}"><img src="{{ asset('images/pic8-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic9.jpg') }}"><img src="{{ asset('images/pic9-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic10.jpg') }}"><img src="{{ asset('images/pic10-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic1.jpg') }}"><img src="{{ asset('images/pic1-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic2.jpg') }}"><img src="{{ asset('images/pic2-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic3.jpg') }}"><img src="{{ asset('images/pic3-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic4.jpg') }}"><img src="{{ asset('images/pic4-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic5.jpg') }}"><img src="{{ asset('images/pic5-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic6.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic7.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic8.jpg') }}"><img src="{{ asset('images/pic8-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic9.jpg') }}"><img src="{{ asset('images/pic9-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic10.jpg') }}"><img src="{{ asset('images/pic10-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
         </div>
      </div>
      <div class="owl-nav disabled">
         <div class="owl-prev"><i class="ti-angle-left"></i></div>
         <div class="owl-next"><i class="ti-angle-right"></i></div>
      </div>
      <div class="owl-dots disabled"></div>
   </div>
</div>