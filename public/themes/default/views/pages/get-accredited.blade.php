<div id="pagetitle" class="page-title bg-overlay">
   <div class="container">
      <div class="page-title-inner">
         <h1 class="page-title">GET ACCREDITATED BY MAAS</h1>
         <ul class="ct-breadcrumb">
            <li><a class="breadcrumb-entry" href="#">Home</a></li>
            <li><span class="breadcrumb-entry">GET ACCREDITATED BY MAAS</span></li>
         </ul>
      </div>
   </div>
</div>
<div id="content" class="site-content">
   <div class="content-inner">
      <div class="container content-container">
         <div class="row content-row">
            <div id="primary" class="content-area content-full-width col-12">
               <main id="main" class="site-main">
                  <article id="post-980" class="post-980 page type-page status-publish hentry">
                     <div class="entry-content clearfix">
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1585685346843 bg-image-ps-inherit">
                           <div class="wpb_column vc_column_container vc_col-sm-12">
                              <div class="vc_column-inner">
                                 <div class="wpb_wrapper">
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-8">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper">
                                                <!--<div id="ct-heading" class="ct-heading align-center align-center-md align-center-sm align-center-xs">
                                                   <h3 class="ct-heading-tag" style="margin-bottom:13px;">GET ACCREDITATED BY MAAS</h3>
                                                   <div class="ct-heading-separator">
                                                      <i class="flaticon-spa"></i>
                                                   </div>
                                                </div>-->
                                                <div class="ct-banner">
                                                   <div class="ct-banner-inner row">
                                                      <div class="ct-banner-content col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                         <div class="ct-banner-desc">
                                                            Never learnt makeup officially but good at it? Now you can apply for a certificate from MAAS to endorse your skills. Simply book an examination slot, appear for an exam and receive a certificate of proficiency.               
                                                         </div>
                                                      </div>
                                                      <div class="ct-banner-image col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                         <!--<span class="bg-image" style="background-image: {{asset('images/makeup-accreditation.jpg') }};">
                                                            </span>-->
                                                         <span class="bg-image" style="background-image: url('{{ asset('images/makeup-accreditation.jpg') }}');">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-8">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper">
                                                <div class="vc_empty_space" style="height: 35px"><span class="vc_empty_space_inner"></span></div>
                                                <div class="vc_empty_space" style="height: 35px"><span class="vc_empty_space_inner"></span></div>
                                                <div id="ct-heading-2" class="ct-heading align-center align-center-md align-center-sm align-center-xs ">
                                                   <h3 class="ct-heading-tag " style="margin-bottom:13px; ">
                                                      How it works?                        
                                                   </h3>
                                                   <div class="ct-heading-separator"><i class="flaticon-spa"></i></div>
                                                </div>
                                                <div class="wpb_text_column wpb_content_element ">
                                                   <div class="wpb_wrapper">
                                                      <p style="text-align: justify;">Makeup artistry is a craft which is developed with years of practice and technique. While you can learn the techniques of makeup from a school, practice is essential.With the rise of internet and makeup tutorials available online, many makeup artists have developed skills through self-learning.</p>
                                                      <p style="text-align: justify;">Now you can get these self-learnt skills endorsed by MAAS and apply for a certificate of proficiency.</p>
                                                      <p style="text-align: justify;">STEP 1: Choose the skill you want to be endorsed for. Eg. Bridal makeup, special effects, fashion etc</p>
                                                      <p style="text-align: justify;">STEP 2: Choose an available examination slot from the calendar.</p>
                                                      <p style="text-align: justify;">STEP 3: Make payment (SGD 200 for 1st attempt and SGD 100 for all subsequent attempts).</p>
                                                      <p style="text-align: justify;">STEP 4: Receive confirmation of slot.</p>
                                                      <p style="text-align: justify;">STEP 5: Arrive for the examination on the confirmed slot. You will need to bring your own model and makeup kit.</p>
                                                      <p style="text-align: justify;">STEP 6: Present your model to the examiner.</p>
                                                      <p style="text-align: justify;">STEP 7: Receive certificate of proficiency if you pass.</p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- .entry-content -->
                  </article>
                  <!-- #post-980 -->
               </main>
               <!-- #main -->
            </div>
            <!-- #primary -->
         </div>
      </div>
   </div>
   <!-- #content inner -->
</div>

<div class="row">
	  <div class="col-md-2"></div>
    <div class="modal-content">
    <div class="log-box">
          <div class="row no-gutters" style="width:850px;">
            <div class="col-xl-5 col-sm-5 col-12 pad-right-0">
              <div class="logo-back">
                <img src="{{ Theme::asset()->url('images/login_bg.jpg') }}" style="height:270px;width:347px;">
              </div>
            </div>
            <div class="col-xl-7 col-sm-7 col-12 pad-left-0">
          <div class="contact-form">
				<div class="form-group">
				  <div class="col-sm-10">          
					<input type="text" class="form-control" id="fname" placeholder="Enter First Name" name="fname">
				  </div>
				</div>
				<div class="form-group">
				  <div class="col-sm-10">          
					<input type="text" class="form-control" id="email" placeholder="Enter Email" name="lname">
				  </div>
				</div>
				<div class="form-group">
				  <div class="col-sm-10">
					<input type="text" class="form-control" id="Phone No" placeholder="Enter Phone" name="Phone">
				  </div>
				</div>
				<div class="form-group">        
				  <div class="col-sm-10">
					<input type="submit" class="btn btn-default" Value="Pay 200">
				  </div>
				</div>
			</div>
            </div>
          </div>
        </div>
    </div>
<div class="col-md-2"></div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
$(".owl-carousel").owlCarousel({
      autoPlay: 3000,
      items : 1, // THIS IS IMPORTANT
      responsive : {
            480 : { items : 1  }, // from zero to 480 screen width 4 items
            768 : { items : 2  }, // from 480 screen widthto 768 6 items
            1024 : { items : 3   // from 768 screen width to 1024 8 items
            }
        },
  });
  </script>
<div class="top-gallery">
   <div class="ct-carousel owl-carousel images-light-box-carousel owl-loaded owl-drag" data-item-xs="4" data-item-sm="6" data-item-md="8" data-item-lg="10" data-margin="0" data-loop="true" data-autoplay="true" data-autoplaytimeout="5000" data-smartspeed="250" data-center="false" data-arrows="false" data-bullets="false" data-stagepadding="0" data-stagepaddingsm="0" data-rtl="false">
      <div class="owl-stage-outer">
         <div class="owl-stage" style="transform: translate3d(-1483px, 0px, 0px); transition: all 0.25s ease 0s; width: 4048px;">
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic1.jpg') }}"><img src="{{asset('images/pic1-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic2.jpg') }}"><img src="{{ asset('images/pic2-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic3.jpg') }}"><img src="{{ asset('images/pic3-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic4.jpg') }}"><img src="{{ asset('images/pic4-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic5.jpg') }}"><img src="{{ asset('images/pic5-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic6.jpg') }}"><img src="{{ asset('images/pic6-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic7.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic8.jpg') }}"><img src="{{ asset('images/pic8-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic9.jpg') }}"><img src="{{ asset('images/pic9-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic10.jpg') }}"><img src="{{ asset('images/pic10-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic1.jpg') }}"><img src="{{ asset('images/pic1-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic2.jpg') }}"><img src="{{ asset('images/pic2-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic3.jpg') }}"><img src="{{ asset('images/pic3-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic4.jpg') }}"><img src="{{ asset('images/pic4-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic5.jpg') }}')"><img src="{{ asset('images/pic5-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic6.jpg') }}"><img src="{{ asset('images/pic6-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic7.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic8.jpg') }}"><img src="{{ asset('images/pic8-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic9.jpg') }}"><img src="{{ asset('images/pic9-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic10.jpg') }}"><img src="{{ asset('images/pic10-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic1.jpg') }}"><img src="{{ asset('images/pic1-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic2.jpg') }}"><img src="{{ asset('images/pic2-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic3.jpg') }}"><img src="{{ asset('images/pic3-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic4.jpg') }}"><img src="{{ asset('images/pic4-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic5.jpg') }}"><img src="{{ asset('images/pic5-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic6.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic7.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic8.jpg') }}"><img src="{{ asset('images/pic8-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic9.jpg') }}"><img src="{{ asset('images/pic9-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic10.jpg') }}"><img src="{{ asset('images/pic10-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
         </div>
      </div>
	  
      <div class="owl-nav disabled">
         <div class="owl-prev"><i class="ti-angle-left"></i></div>
         <div class="owl-next"><i class="ti-angle-right"></i></div>
      </div>
      <div class="owl-dots disabled"></div>
   </div>
</div>
