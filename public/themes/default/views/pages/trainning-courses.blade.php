<section class="testimonial-bg" style="background-image:url( {{ asset( 'images/testimonial-bg.jpg' ) }} background-size: cover; background-repeat: no-repeat; ">
    
<div id="particles-js1" style="background:transparent!important;" ></div>
    <div class="container" >
            
                      <div class="row justify-content-center">
                    <div class="col-12 col-lg-7 text-center margin-100px-bottom sm-margin-40px-bottom">
                        <div class="position-relative overflow-hidden w-100">
                        <span class="text-small text-outside-line-full alt-font font-weight-600 text-uppercase  client-text"><h6 style="color: #000!important;"><span id="typewriter1">Our Trainning Courses </h6></span>
                        </div>
                    </div>
                </div> 

            <div class="main clearfix">
                <div class="bb-custom-wrapper">
                    <div id="bb-bookblock" class="bb-bookblock">
                        
                          <div class="bb-item" style="background-color: rgb(11, 164, 65) !important;">
                             <div class="fc-calendar-wrap">
                               
                                <div class="fc-calendar-container">
                                     <h2>John Castilo</h2>
                                    <p>One of the best places to learn Blockchain. The best thing is that the trainer takes all his efforts to resolve the course-related query and issues and also will provide additional knowledge. Courses here are highly recommended.</p>
                                </div>
                            </div>
                        </div>
                        
                          <div class="bb-item" style="background-color: rgb(184, 56, 0)  !important;">
                             <div class="fc-calendar-wrap">
                               
                                <div class="fc-calendar-container">
                                     <h2>Seema Narang</h2>
                                    <p>Feeling great !!! Good content for Blockchain training, very well explained. It can be implemented after understanding in building up various use-cases.</p>
                                </div>
                            </div>
                        </div>
                        
                        
                          <div class="bb-item" style="background-color: rgb(11, 164, 65) !important;">
                             <div class="fc-calendar-wrap">
                               
                                <div class="fc-calendar-container">
                                     <h2>Anand Sharma</h2>
                                    <p>Great course I really enjoyed it and the course was way easy to learn with very good explanations of the code, I could easily understand and develop applications with the.</p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="bb-item" style="background-color:rgb(184, 56, 0) !important;">
                             <div class="fc-calendar-wrap">
                              
                                <div class="fc-calendar-container">
                                      <h2>Anil Mathew</h2>
                                    <p itemprop="bestRating">I really enjoyed this course, the instructor is so experienced and this is totally cool, at the same time and the examples made the learning quick. Thank you so much for the guidance and for explaining things in such a friendly manner!.</p>
                                </div>
                            </div>
                        </div>
                        <div class="bb-item" style="background-color: rgb(11, 164, 65) !important;">
                             <div class="fc-calendar-wrap">
                               
                                <div class="fc-calendar-container">
                                     <h2>Shyam Sharma</h2>
                                    <p>The instructor has a lot of experience in this field. The example he shows touches many different areas with many details.</p>
                                </div>
                            </div>
                        </div>
                        <!--<div class="bb-item" style="background-color: rgb(253, 77, 138) !important;">
                            <img class="bb-custom-img" src="<?php //echo base_url();?>assets-ui/images/testimonial3.jpg" alt="image3"/>
                           <div class="fc-calendar-wrap">
                               
                                <div class="fc-calendar-container">
                                     <h2>MRS. LAURA</h2>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                </div>
                            </div>
                        </div>-->
                   
                       
                      
                    </div>
                    <nav>
                        <a id="bb-nav-prev" href="#" class=""><i class="fas fa-angle-left"></i> Previous</a>
                        <a id="bb-nav-next" href="#" class=""><i class="fas fa-angle-right"></i> Next</a>
                    </nav>
                   
                </div>
            </div>
        </div><!-- /container -->
       
          <script src="{!! Theme::asset()->url('lib/js/jquery.bookblock.js') !!}"></script>
        <script>
            var Page = (function() {
                
                var config = {
                        $bookBlock : $( '#bb-bookblock' ),
                        $navNext : $( '#bb-nav-next' ),
                        $navPrev : $( '#bb-nav-prev' )
                    },
                    init = function() {
                        config.$bookBlock.bookblock( {
                            orientation : 'horizontal',
                            speed : 700
                        } );
                        initEvents();
                    },
                    initEvents = function() {

                        var $slides = config.$bookBlock.children();
                        
                        // add navigation events
                        config.$navNext.on( 'click touchstart', function() {
                            config.$bookBlock.bookblock( 'next' );
                            return false;
                        } );

                        config.$navPrev.on( 'click touchstart', function() {
                            config.$bookBlock.bookblock( 'prev' );
                            return false;
                        } );

                        // add keyboard events
                        $( document ).keydown( function(e) {
                            var keyCode = e.keyCode || e.which,
                                arrow = {
                                    left : 37,
                                    up : 38,
                                    right : 39,
                                    down : 40
                                };

                            switch (keyCode) {
                                case arrow.up:
                                    config.$bookBlock.bookblock( 'prev' );
                                    e.preventDefault();
                                    break;
                                case arrow.down:
                                    config.$bookBlock.bookblock( 'next' );
                                    e.preventDefault();
                                    break;
                            }

                        } );
                    };

                    return { init : init };

            })();


               Page.init();
        </script>
     
</section>





<div id="content" class="site-content">
   <div class="content-inner">
      <div class="container content-container">
         <div class="row content-row">
            <div id="primary" class="content-area content-full-width col-12">
               <main id="main" class="site-main">
                  <article id="post-980" class="post-980 page type-page status-publish hentry">
                     <div class="entry-content clearfix">
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1585685346843 bg-image-ps-inherit">
                           <div class="wpb_column vc_column_container vc_col-sm-12">
                              <div class="vc_column-inner">
                                 <div class="wpb_wrapper">
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-8">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper">
                                                <div id="ct-heading" class="ct-heading align-center align-center-md align-center-sm align-center-xs">
                                                   <h3 class="ct-heading-tag" style="margin-bottom:13px;">Trainning Courses</h3>
                                                   <div class="ct-heading-separator">
                                                      <i class="flaticon-spa"></i>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       
                                    </div>
                                   	<div id="quiz">
		<h3 id="question"></h3>
		<button id="choice0"></button>
		<button id="choice1"></button>
		<button id="choice2"></button>
		<footer>
			<p><span id="progress"></span><span id="score"></span></p>
		</footer>
	</div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- .entry-content -->
                  </article>
                  <!-- #post-980 -->
               </main>
               <!-- #main -->
            </div>
            <!-- #primary -->
         </div>
      </div>
   </div>
   <!-- #content inner -->
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
$(".owl-carousel").owlCarousel({
      autoPlay: 3000,
      items : 1, // THIS IS IMPORTANT
      responsive : {
            480 : { items : 1  }, // from zero to 480 screen width 4 items
            768 : { items : 2  }, // from 480 screen widthto 768 6 items
            1024 : { items : 3   // from 768 screen width to 1024 8 items
            }
        },
  });
  </script>
<div class="top-gallery">
   <div class="ct-carousel owl-carousel images-light-box-carousel owl-loaded owl-drag" data-item-xs="4" data-item-sm="6" data-item-md="8" data-item-lg="10" data-margin="0" data-loop="true" data-autoplay="true" data-autoplaytimeout="5000" data-smartspeed="250" data-center="false" data-arrows="false" data-bullets="false" data-stagepadding="0" data-stagepaddingsm="0" data-rtl="false">
      <div class="owl-stage-outer">
         <div class="owl-stage" style="transform: translate3d(-1483px, 0px, 0px); transition: all 0.25s ease 0s; width: 4048px;">
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic1.jpg') }}"><img src="{{asset('images/pic1-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic2.jpg') }}"><img src="{{ asset('images/pic2-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic3.jpg') }}"><img src="{{ asset('images/pic3-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic4.jpg') }}"><img src="{{ asset('images/pic4-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic5.jpg') }}"><img src="{{ asset('images/pic5-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic6.jpg') }}"><img src="{{ asset('images/pic6-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic7.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic8.jpg') }}"><img src="{{ asset('images/pic8-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic9.jpg') }}"><img src="{{ asset('images/pic9-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic10.jpg') }}"><img src="{{ asset('images/pic10-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic1.jpg') }}"><img src="{{ asset('images/pic1-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic2.jpg') }}"><img src="{{ asset('images/pic2-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic3.jpg') }}"><img src="{{ asset('images/pic3-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic4.jpg') }}"><img src="{{ asset('images/pic4-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic5.jpg') }}')"><img src="{{ asset('images/pic5-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic6.jpg') }}"><img src="{{ asset('images/pic6-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic7.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic8.jpg') }}"><img src="{{ asset('images/pic8-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic9.jpg') }}"><img src="{{ asset('images/pic9-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic10.jpg') }}"><img src="{{ asset('images/pic10-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic1.jpg') }}"><img src="{{ asset('images/pic1-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic2.jpg') }}"><img src="{{ asset('images/pic2-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic3.jpg') }}"><img src="{{ asset('images/pic3-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic4.jpg') }}"><img src="{{ asset('images/pic4-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic5.jpg') }}"><img src="{{ asset('images/pic5-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic6.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic7.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic8.jpg') }}"><img src="{{ asset('images/pic8-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic9.jpg') }}"><img src="{{ asset('images/pic9-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic10.jpg') }}"><img src="{{ asset('images/pic10-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
         </div>
      </div>
      <div class="owl-nav disabled">
         <div class="owl-prev"><i class="ti-angle-left"></i></div>
         <div class="owl-next"><i class="ti-angle-right"></i></div>
      </div>
      <div class="owl-dots disabled"></div>
   </div>
</div>


<script>
// QUESTION CONSTRUCTOR
function Question(text, choices, answer) {
	this.text = text; // string
	this.choices = choices; // array
	this.answer = answer; // string
}
Question.prototype.isCorrect = function(choice) {
	// Return TRUE if choice matches correct answer
	return this.answer === choice; 
};

// QUIZ CONSTRUCTOR
function Quiz(questions) {
	// Array of questions
	this.questions = questions;
	// Track which question you're on, starting with the first question
	this.currentQuestionIndex = 0;
	this.score = 0; // Score keeper
}
Quiz.prototype.getCurrentQuestion = function() {
	return this.questions[this.currentQuestionIndex];
};
Quiz.prototype.checkAnswer = function(answer) {
	if(this.getCurrentQuestion().isCorrect(answer)) {
		this.score++; // Add 1 point if correct
	}
	this.currentQuestionIndex++; // Get ready for next question
};
// Check if quiz end is reached
Quiz.prototype.hasEnded = function() {
	// Return TRUE only after last question
	return this.currentQuestionIndex >= this.questions.length;
};

// QUIZ UI
var QuizUI = {
	displayNext: function() {
		if(quiz.hasEnded()) {
			this.showResults();
		} else {
			this.displayQuestion();
			this.displayChoices();
			this.displayProgress();
			this.displayScore();
		}
	},
	displayQuestion: function() {
		this.populateIdWithHTML('question', quiz.getCurrentQuestion().text);
	},
	displayChoices: function() {
		var choices = quiz.getCurrentQuestion().choices;
		// Loop through each choice and display on page
		for(var i = 0; i < choices.length; i++) {
			var choiceId = 'choice' + i;
			var choiceText = choices[i];
			this.populateIdWithHTML(choiceId, choiceText);
			this.checkAnswerHandler(choiceId, choiceText);
		}
	},
	checkAnswerHandler: function(id, guess) {
		var button = document.getElementById(id);
		button.onclick = function() {
			quiz.checkAnswer(guess);
			QuizUI.displayNext();
		}
	},
	displayScore: function() {
		var scoreText = 'Score: ' + quiz.score;
		this.populateIdWithHTML('score', scoreText);
	},
	displayProgress: function() {
		var questionNumber = quiz.currentQuestionIndex + 1;
		var totalQuestions = quiz.questions.length;
		var progressText = 'Question ' + questionNumber + ' of ' + totalQuestions;
		this.populateIdWithHTML('progress', progressText);
	},
	showResults: function() {
		var grade = quiz.score/quiz.questions.length;
		var results = '<h2>';
		if(grade >= 0.8) {
			results += 'Excellent!';
		} else if(grade < 0.8 && grade > 0.5) {
			results += 'Not Bad...';
		} else {
			results += 'Terrible!';
		}
		results += '</h2><h3>Your final score is: ' + quiz.score + '</h3>';
		results += '<button id="reset">Try Again?</button>';
		this.populateIdWithHTML('quiz', results);
		this.resetQuizHandler();
	},
	resetQuizHandler: function() {
		var resetBtn = document.getElementById('reset');
		// Reload quiz to start from beginning
		resetBtn.onclick = function() {
			window.location.reload(false);
		}
	},
	populateIdWithHTML: function(id, content) {
		var element = document.getElementById(id);
		element.innerHTML = content;
	}
};

var questions = [
	new Question('Which state is Chicago in?', ['Iowa', 'Illinois', 'Indiana'], 'Illinois'),
	new Question('How many states are in the United States?', ['48', '49', '50'], '50'),
	new Question('Who was the first president of the United States?', ['George Washington', 'Abraham Lincoln', 'Andrew Jackson'], 'George Washington')
];
// CREATE QUIZ & DISPLAY FIRST QUESTION
var quiz = new Quiz(questions);
QuizUI.displayNext();
</script>