<div id="pagetitle" class="page-title bg-overlay">
   <div class="container">
      <div class="page-title-inner">
         <h1 class="page-title">Blog</h1>
         <ul class="ct-breadcrumb">
            <li><a class="breadcrumb-entry" href="#">Home</a></li>
            <li><span class="breadcrumb-entry">Blog</span></li>
         </ul>
      </div>
   </div>
</div>

<div class="VendorList">
<div class="container">
<ul class="PrimaryVendorCard">
      <li class="vendor-picture shadow col-md-3">
         <a href="#">
            <div class="vendor-card extra-radius">
                                    <div class="vendor-picture margin-r-10"><img class="info-icon pointer" data-tip="true" data-for="17246" src="{{ url('images/blogs/1592177244.jpg') }}"></div>
                                    <div class="vendor-info">
                                       <div class="padding-10">
                                          <div class="line f-space-between">
                                             <div class="frow"></div>
                                             <div class="nowrap"></div>
                                          </div>
                                          <div class="line">
                                             <p class="vendor-detail">Priceless, and also free</p>
                                             <span class="review-cnt regular nowrap"></span>
                                          </div>
                                       </div>
                                       <div class="line">
                                          <hr>
                                       </div>
                                       <div class="vendor-price frow margin-10 f-space-between">
                                          <span class="frow v-center">
                                             <div>
                                                <div class="frow">
                                                   <p class="vendor-detail text-bold text-primary v-center margin-r-5"><span class=""></span></p>
                                                   <p class="text-primary">This is the blog post ...</p>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                 </div>
         </a>
      </li>
	        <li class="vendor-picture shadow col-md-3">
         <a href="#">
            <div class="vendor-card extra-radius">
                                    <div class="vendor-picture margin-r-10"><img class="info-icon pointer" data-tip="true" data-for="17246" src="{{ url('images/blogs/1592177244.jpg') }}"></div>
                                    <div class="vendor-info">
                                       <div class="padding-10">
                                          <div class="line f-space-between">
                                             <div class="frow"></div>
                                             <div class="nowrap"></div>
                                          </div>
                                          <div class="line">
                                             <p class="vendor-detail">Priceless, and also free</p>
                                             <span class="review-cnt regular nowrap"></span>
                                          </div>
                                       </div>
                                       <div class="line">
                                          <hr>
                                       </div>
                                       <div class="vendor-price frow margin-10 f-space-between">
                                          <span class="frow v-center">
                                             <div>
                                                <div class="frow">
                                                   <p class="vendor-detail text-bold text-primary v-center margin-r-5"><span class=""></span></p>
                                                   <p class="text-primary">This is the blog post ...</p>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                 </div>
         </a>
      </li>
	        <li class="vendor-picture shadow col-md-3">
         <a href="#">
            <div class="vendor-card extra-radius">
                                    <div class="vendor-picture margin-r-10"><img class="info-icon pointer" data-tip="true" data-for="17246" src="{{ url('images/blogs/1592177244.jpg') }}"></div>
                                    <div class="vendor-info">
                                       <div class="padding-10">
                                          <div class="line f-space-between">
                                             <div class="frow"></div>
                                             <div class="nowrap"></div>
                                          </div>
                                          <div class="line">
                                             <p class="vendor-detail">Priceless, and also free</p>
                                             <span class="review-cnt regular nowrap"></span>
                                          </div>
                                       </div>
                                       <div class="line">
                                          <hr>
                                       </div>
                                       <div class="vendor-price frow margin-10 f-space-between">
                                          <span class="frow v-center">
                                             <div>
                                                <div class="frow">
                                                   <p class="vendor-detail text-bold text-primary v-center margin-r-5"><span class=""></span></p>
                                                   <p class="text-primary">This is the blog post ...</p>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                 </div>
         </a>
      </li>
	        <li class="vendor-picture shadow col-md-3">
         <a href="#">
            <div class="vendor-card extra-radius">
                                    <div class="vendor-picture margin-r-10"><img class="info-icon pointer" data-tip="true" data-for="17246" src="{{ url('images/blogs/1592177244.jpg') }}"></div>
                                    <div class="vendor-info">
                                       <div class="padding-10">
                                          <div class="line f-space-between">
                                             <div class="frow"></div>
                                             <div class="nowrap"></div>
                                          </div>
                                          <div class="line">
                                             <p class="vendor-detail">Priceless, and also free</p>
                                             <span class="review-cnt regular nowrap"></span>
                                          </div>
                                       </div>
                                       <div class="line">
                                          <hr>
                                       </div>
                                       <div class="vendor-price frow margin-10 f-space-between">
                                          <span class="frow v-center">
                                             <div>
                                                <div class="frow">
                                                   <p class="vendor-detail text-bold text-primary v-center margin-r-5"><span class=""></span></p>
                                                   <p class="text-primary">This is the blog post ...</p>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                 </div>
         </a>
      </li>
</ul>
</div>
</div>
<div class="clear"></div>