<div id="pagetitle" class="page-title bg-overlay">
   <div class="container">
      <div class="page-title-inner">
         <h1 class="page-title">About Us</h1>
         <ul class="ct-breadcrumb">
            <li><a class="breadcrumb-entry" href="#">Home</a></li>
            <li><span class="breadcrumb-entry">About Us</span></li>
         </ul>
      </div>
   </div>
</div>
<div id="content" class="site-content">
   <div class="content-inner">
      <div class="container content-container">
         <div class="row content-row">
            <div id="primary" class="content-area content-full-width col-12">
               <main id="main" class="site-main">
                  <article id="post-12" class="post-12 page type-page status-publish hentry">
                     <div class="entry-content clearfix">
                        <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1538834919705 vc_row-has-fill bg-image-ps-center" style="position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;">
                           <div class="wpb_column vc_column_container vc_col-sm-12">
                              <div class="vc_column-inner vc_custom_1538820139055" style="padding-top: 35px;">
                                 <div class="wpb_wrapper">
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-8 rm-padding-md">
                                          <div class="vc_column-inner vc_custom_1538820125683">
                                             <div class="wpb_wrapper">
                                                <div id="ct-heading" class="ct-heading align-center align-center-md align-center-sm align-center-xs ">
                                                   <h3 class="ct-heading-tag " style="margin-bottom:13px; ">
                                                      Mission                        
                                                   </h3>
                                                   <div class="ct-heading-separator"><i class="flaticon-spa"></i></div>
                                                </div>
                                                <div class="wpb_text_column wpb_content_element ">
                                                   <div class="wpb_wrapper">
                                                      <p style="text-align: justify;">The Makeup Artist Association of Singapore, or MAAS, unites makeup artists across the Lion City, providing members with opportunities to earn, learn and share.</p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-8 rm-padding-md">
                                          <div class="vc_column-inner vc_custom_1538820125683">
                                             <div class="wpb_wrapper">
                                                <div id="ct-heading-2" class="ct-heading align-center align-center-md align-center-sm align-center-xs ">
                                                   <h3 class="ct-heading-tag " style="margin-bottom:13px; ">
                                                      Vision                        
                                                   </h3>
                                                   <div class="ct-heading-separator"><i class="flaticon-spa"></i></div>
                                                </div>
                                                <div class="wpb_text_column wpb_content_element ">
                                                   <div class="wpb_wrapper">
                                                      <p style="text-align: justify;">The MAAS vision is to promote widespread respect for the Singapore makeup artistry community as valued and contributing members of society while positioning makeup artistry as an attractive career path.</p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-8 rm-padding-md">
                                          <div class="vc_column-inner vc_custom_1585471759837">
                                             <div class="wpb_wrapper">
                                                <div id="ct-heading-3" class="ct-heading align-center align-center-md align-center-sm align-center-xs ">
                                                   <h3 class="ct-heading-tag " style="margin-bottom:13px; ">
                                                      History                        
                                                   </h3>
                                                   <div class="ct-heading-separator"><i class="flaticon-spa"></i></div>
                                                </div>
                                                <div class="wpb_text_column wpb_content_element ">
                                                   <div class="wpb_wrapper">
                                                      <p style="text-align: justify;">The life of a makeup artist may seem glamorous, but it also presents challenges. Every independent makeup artist needs not only to excel at the craft but also master the many facets of entrepreneurship and business management.</p>
                                                      <p style="text-align: justify;">The Makeup Artist Association of Singapore, or MAAS, was founded in 2017 to address the challenges faced by Singapore’s makeup artistry community by building a powerful professional network designed to unite practitioners across the nation. The association was founded by professional makeup artist and brand manager Minakshi Singh Sengar, who started her career as a makeup artist in 2012 after graduating from a local makeup academy. Upon entering the profession, she discovered that the life of a makeup artist is not always easy. At the time, tough competition was driving down service rates as makeup product prices seemed to grow daily. In addition, she found few resources and no coordinated efforts to support the several thousands of makeup artists across Singapore. As a result, many talented artists struggled to grow their businesses and had no advocates in their underrepresented industry.</p>
                                                      <p style="text-align: justify;">This observation formed the seed of what was to become MAAS. Minakshi’s vision was to provide resources to support skilled makeup artists who are not able to make a decent living because they lack the business acumen and resources to grow their enterprises. In 2015, Minakshi started convening groups of makeup artists across Singapore to understand their concerns and needs. This led to the birth of the Makeup Artist Association of Singapore in 2017, with the sole aim of providing makeup artists in Singapore with the resources to further their businesses.</p>
                                                      <p style="text-align: justify;">As MAAS quickly grew by the hundreds and then thousands, the association launched a marketplace for makeup artists to advertise their services and maintain their portfolios and bookings. At LINK, the general public can search for makeup artists depending on their needs and budget. This common marketplace has helped members align service pricing and has made rates more competitive for clients.</p>
                                                      <p style="text-align: justify;">Today, MAAS is the single largest marketplace for Singapore makeup artists and consumers. The association continues to be involved in activities that uplift the makeup artistry profession through continuing education and business leadership training opportunities, accreditation, industry representation and advocacy, collaboration, product discounts, professional awards and recognition, and much more while creating a place for makeup and beauty professionals to forge lasting relationships with their peers.</p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1538820707801 vc_row-has-fill bg-image-ps-bottom" style="position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;">
                           <div class="wpb_column vc_column_container vc_col-sm-12">
                              <div class="vc_column-inner vc_custom_1585471711066">
                                 <div class="wpb_wrapper">
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-8 rm-padding-md">
                                          <div class="vc_column-inner vc_custom_1538063128965">
                                             <div class="wpb_wrapper">
                                                <div id="ct-heading-4" class="ct-heading align-center align-center-md align-center-sm align-center-xs ">
                                                   <h3 class="ct-heading-tag ">
                                                      Our Pricing                        
                                                   </h3>
                                                   <div class="ct-heading-separator"><i class="flaticon-spa"></i></div>
                                                   <div class="ct-heading-desc" style="">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the.</div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper">
                                                <div class="ct-pricing-default ">
                                                   <div class="ct-pricing-item  wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="ct-pricing-meta">
                                                         <h3>Deep Tissue Massage</h3>
                                                         <span>60 – 100 Minute Session </span>
                                                      </div>
                                                      <div class="ct-pricing-price">$40.00</div>
                                                   </div>
                                                   <div class="ct-pricing-item  wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="ct-pricing-meta">
                                                         <h3>Swedish Massage</h3>
                                                         <span>30 – 40 Minute Session </span>
                                                      </div>
                                                      <div class="ct-pricing-price">$35.00</div>
                                                   </div>
                                                   <div class="ct-pricing-item  wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="ct-pricing-meta">
                                                         <h3>Deep Tissue Massage</h3>
                                                         <span>60 – 100 Minute Session </span>
                                                      </div>
                                                      <div class="ct-pricing-price">$40.00</div>
                                                   </div>
                                                   <div class="ct-pricing-item  wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="ct-pricing-meta">
                                                         <h3>Therapeutic Massage</h3>
                                                         <span>40 – 60 Minute Session</span>
                                                      </div>
                                                      <div class="ct-pricing-price">$30.00</div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper">
                                                <div class="ct-pricing-default ">
                                                   <div class="ct-pricing-item  wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="ct-pricing-meta">
                                                         <h3>Hot Stone Massage</h3>
                                                         <span>50 – 60 Minute Session </span>
                                                      </div>
                                                      <div class="ct-pricing-price">$34.00</div>
                                                   </div>
                                                   <div class="ct-pricing-item  wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="ct-pricing-meta">
                                                         <h3>Couples Massage</h3>
                                                         <span>20 – 30 Minute Session </span>
                                                      </div>
                                                      <div class="ct-pricing-price">$42.00</div>
                                                   </div>
                                                   <div class="ct-pricing-item  wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="ct-pricing-meta">
                                                         <h3>Facial</h3>
                                                         <span>15 – 30 Minute Session </span>
                                                      </div>
                                                      <div class="ct-pricing-price">$38.00</div>
                                                   </div>
                                                   <div class="ct-pricing-item  wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="ct-pricing-meta">
                                                         <h3>Body Waxing</h3>
                                                         <span>80 – 100 Minute Session </span>
                                                      </div>
                                                      <div class="ct-pricing-price">$65.00</div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div class="vc_row wpb_row vc_row-fluid bg-image-ps-inherit">
                           <div class="wpb_column vc_column_container vc_col-sm-12">
                              <div class="vc_column-inner vc_custom_1538834974217">
                                 <div class="wpb_wrapper">
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-8 rm-padding-md">
                                          <div class="vc_column-inner vc_custom_1538063128965">
                                             <div class="wpb_wrapper">
                                                <div id="ct-heading-5" class="ct-heading align-center align-center-md align-center-sm align-center-xs ">
                                                   <h3 class="ct-heading-tag ">
                                                      Our Professional Team                        
                                                   </h3>
                                                   <div class="ct-heading-separator"><i class="flaticon-spa"></i></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="vc_empty_space" style="height: 10px"><span class="vc_empty_space_inner"></span></div>
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 wpb_animate_when_almost_visible wpb_fadeInRight fadeInRight wpb_start_animation animated" style="animation-delay: 100ms;">
                                          <div class="vc_column-inner vc_custom_1538844072775">
                                             <div class="wpb_wrapper">
                                                <div class="ct-team-member-default  ">
                                                   <div class="ct-team-member-inner clearfix">
                                                      <div class="ct-team-member-image">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team1.jpg" width="500" height="500" alt="team1" title="team1">            
                                                      </div>
                                                      <div class="ct-team-member-content">
                                                         <h3 class="ct-team-member-title">
                                                            Carol Oliver                
                                                         </h3>
                                                         <div class="ct-team-member-desc">
                                                            It is a long established fact that a reader will be distracted by the readable content of a page.                
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 wpb_animate_when_almost_visible wpb_fadeInRight fadeInRight wpb_start_animation animated" style="animation-delay: 200ms;">
                                          <div class="vc_column-inner vc_custom_1538844081489">
                                             <div class="wpb_wrapper">
                                                <div class="ct-team-member-default">
                                                   <div class="ct-team-member-inner clearfix">
                                                      <div class="ct-team-member-image">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team2.jpg" width="500" height="500" alt="team2" title="team2">            
                                                      </div>
                                                      <div class="ct-team-member-content">
                                                         <h3 class="ct-team-member-title">
                                                            Michelle Bailey                
                                                         </h3>
                                                         <div class="ct-team-member-desc">
                                                            It is a long established fact that a reader will be distracted by the readable content of a page.                
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 wpb_animate_when_almost_visible wpb_fadeInRight fadeInRight wpb_start_animation animated" style="animation-delay: 300ms;">
                                          <div class="vc_column-inner vc_custom_1538844087995">
                                             <div class="wpb_wrapper">
                                                <div class="ct-team-member-default  ">
                                                   <div class="ct-team-member-inner clearfix">
                                                      <div class="ct-team-member-image">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team3.jpg" width="500" height="500" alt="team3" title="team3">            
                                                      </div>
                                                      <div class="ct-team-member-content">
                                                         <h3 class="ct-team-member-title">
                                                            Paul Brennan                
                                                         </h3>
                                                         <div class="ct-team-member-desc">
                                                            It is a long established fact that a reader will be distracted by the readable content of a page.                
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 wpb_animate_when_almost_visible wpb_fadeInRight fadeInRight wpb_start_animation animated" style="animation-delay: 400ms;">
                                          <div class="vc_column-inner vc_custom_1538844093822">
                                             <div class="wpb_wrapper">
                                                <div class="ct-team-member-default  ">
                                                   <div class="ct-team-member-inner clearfix">
                                                      <div class="ct-team-member-image">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/team4.jpg" width="500" height="500" alt="team4" title="team4">            
                                                      </div>
                                                      <div class="ct-team-member-content">
                                                         <h3 class="ct-team-member-title">
                                                            Bill Thompson                
                                                         </h3>
                                                         <div class="ct-team-member-desc">
                                                            It is a long established fact that a reader will be distracted by the readable content of a page.                
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-parallax="1.3" data-vc-parallax-image="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/bg-parallax2.jpg" class="vc_row wpb_row vc_row-fluid vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving bg-image-ps-inherit" style="position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;">
                           <div class="wpb_column vc_column_container vc_col-sm-12">
                              <div class="vc_column-inner vc_custom_1538839183877">
                                 <div class="wpb_wrapper">
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-8 rm-padding-md">
                                          <div class="vc_column-inner vc_custom_1538063128965">
                                             <div class="wpb_wrapper">
                                                <div id="ct-heading-6" class="ct-heading align-center align-center-md align-center-sm align-center-xs ">
                                                   <h3 class="ct-heading-tag ">
                                                      Testimonials Of Our Clients                        
                                                   </h3>
                                                   <div class="ct-heading-separator"><i class="flaticon-spa"></i></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="vc_empty_space" style="height: 45px"><span class="vc_empty_space_inner"></span></div>
                                    <div id="ct-testimonial-carousel" class="ct-testimonial-carousel default owl-carousel nav-middle skrollable skrollable-after owl-loaded owl-drag" data-item-xs="1" data-item-sm="2" data-item-md="3" data-item-lg="3" data-margin="30" data-loop="true" data-autoplay="true" data-autoplaytimeout="5000" data-smartspeed="250" data-center="false" data-arrows="true" data-bullets="true" data-stagepadding="0" data-rtl="false" data-item-xs-custom="2" style="">
                                       <div class="owl-stage-outer">
                                          <div class="owl-stage" style="transform: translate3d(-1560px, 0px, 0px); transition: all 0.25s ease 0s; width: 4680px;">
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author1-2.jpg" width="100" height="100" alt="author1" title="author1">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Joanna Wang                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author2-2.jpg" width="100" height="100" alt="author2" title="author2">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Espen Brunberg                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author3-2.jpg" width="100" height="100" alt="author3" title="author3">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         John Doe                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author1-2.jpg" width="100" height="100" alt="author1" title="author1">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Joanna Wang                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author2-2.jpg" width="100" height="100" alt="author2" title="author2">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Espen Brunberg                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author3-2.jpg" width="100" height="100" alt="author3" title="author3">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         John Doe                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author1-2.jpg" width="100" height="100" alt="author1" title="author1">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Joanna Wang                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author2-2.jpg" width="100" height="100" alt="author2" title="author2">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Espen Brunberg                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author3-2.jpg" width="100" height="100" alt="author3" title="author3">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         John Doe                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author1-2.jpg" width="100" height="100" alt="author1" title="author1">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Joanna Wang                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author2-2.jpg" width="100" height="100" alt="author2" title="author2">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         Espen Brunberg                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-testimonial-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="testimonial-featured">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/author3-2.jpg" width="100" height="100" alt="author3" title="author3">                        
                                                      </div>
                                                      <div class="testimonial-description">BeautyZone was extremely creative and forward thinking. They are also very quick and efficient when executing changes for us.</div>
                                                      <h3 class="testimonial-title">
                                                         John Doe                    
                                                      </h3>
                                                      <div class="testimonial-position">
                                                         Student                    
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="owl-nav">
                                          <div class="owl-prev"><i class="ti-angle-left"></i></div>
                                          <div class="owl-next"><i class="ti-angle-right"></i></div>
                                       </div>
                                       <div class="owl-dots">
                                          <div class="owl-dot active"><span></span></div>
                                          <div class="owl-dot"><span></span></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="vc_parallax-inner skrollable skrollable-before" data-bottom-top="top: -30%;" data-top-bottom="top: 0%;" style="height: 130%; background-image: url(&quot;http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/10/bg-parallax2.jpg&quot;); top: -30%;"></div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1538834813103 vc_row-has-fill bg-image-ps-center" style="position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;">
                           <div class="wpb_column vc_column_container vc_col-sm-12">
                              <div class="vc_column-inner vc_custom_1538839762078">
                                 <div class="wpb_wrapper">
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-8 rm-padding-md">
                                          <div class="vc_column-inner vc_custom_1538067333010">
                                             <div class="wpb_wrapper">
                                                <div id="ct-heading-7" class="ct-heading align-center align-center-md align-center-sm align-center-xs ">
                                                   <h3 class="ct-heading-tag ">
                                                      Our Latest Blog                        
                                                   </h3>
                                                   <div class="ct-heading-separator"><i class="flaticon-spa"></i></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="wpb_column vc_column_container vc_col-sm-2">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div id="ct-blog-carousel" class="ct-blog-carousel layout2 ct-grid-blog-layout1 owl-carousel nav-middle skrollable skrollable-after owl-loaded owl-drag" data-item-xs="1" data-item-sm="2" data-item-md="3" data-item-lg="3" data-margin="30" data-loop="true" data-autoplay="false" data-autoplaytimeout="5000" data-smartspeed="250" data-center="false" data-arrows="true" data-bullets="false" data-stagepadding="0" data-rtl="false" style="">
                                       <div class="owl-stage-outer">
                                          <div class="owl-stage" style="transform: translate3d(-1170px, 0px, 0px); transition: all 0s ease 0s; width: 4680px;">
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog2-1-600x429.jpg" width="600" height="429" alt="An Evening with Kayla &amp; Ieva on all things Pregnancy" title="blog2">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 24, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">An Evening with Kayla &amp; Ieva on all things Pregnancy</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog1-1-600x429.jpg" width="600" height="429" alt="Spring is in the Air and and So Our These Amazing Spa Offers" title="blog1">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 24, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">Spring is in the Air and and So Our These Amazing</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog5-1-600x429.jpg" width="600" height="429" alt="The Correct Order to Apply Your Skincare Products" title="blog5">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 22, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">The Correct Order to Apply Your Skincare Products</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog2-1-600x429.jpg" width="600" height="429" alt="An Evening with Kayla &amp; Ieva on all things Pregnancy" title="blog2">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>November 15, 2019</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">Gutenberg Style</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog4-1-600x429.jpg" width="600" height="429" alt="10 Spas in Ireland to Get Pregnancy Spa Treatments" title="blog4">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 24, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">10 Spas in Ireland to Get Pregnancy Spa Treatments</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item active" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog3-1-600x429.jpg" width="600" height="429" alt="Christmas Customer Event Chill Spa at the Ice House" title="blog3">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 24, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">Christmas Customer Event Chill Spa at the Ice House</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog2-1-600x429.jpg" width="600" height="429" alt="An Evening with Kayla &amp; Ieva on all things Pregnancy" title="blog2">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 24, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">An Evening with Kayla &amp; Ieva on all things Pregnancy</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog1-1-600x429.jpg" width="600" height="429" alt="Spring is in the Air and and So Our These Amazing Spa Offers" title="blog1">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 24, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">Spring is in the Air and and So Our These Amazing</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog5-1-600x429.jpg" width="600" height="429" alt="The Correct Order to Apply Your Skincare Products" title="blog5">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 22, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">The Correct Order to Apply Your Skincare Products</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog2-1-600x429.jpg" width="600" height="429" alt="An Evening with Kayla &amp; Ieva on all things Pregnancy" title="blog2">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>November 15, 2019</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">Gutenberg Style</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog4-1-600x429.jpg" width="600" height="429" alt="10 Spas in Ireland to Get Pregnancy Spa Treatments" title="blog4">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 24, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">10 Spas in Ireland to Get Pregnancy Spa Treatments</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="owl-item cloned" style="width: 360px; margin-right: 30px;">
                                                <div class="ct-carousel-item">
                                                   <div class="grid-item-inner wpb_animate_when_almost_visible wpb_fadeIn fadeIn">
                                                      <div class="item-featured">
                                                         <a href="#">
                                                         <img class="" src="http://barnco99.com/makeupartistassociation/wp-content/uploads/2018/09/blog3-1-600x429.jpg" width="600" height="429" alt="Christmas Customer Event Chill Spa at the Ice House" title="blog3">                                 </a>
                                                      </div>
                                                      <div class="item-body">
                                                         <ul class="item-meta">
                                                            <li>September 24, 2018</li>
                                                            <li>
                                                               <a class="item-comment" href="#">No Comments</a>
                                                            </li>
                                                         </ul>
                                                         <h3 class="item-title" style="">
                                                            <a href="#">Christmas Customer Event Chill Spa at the Ice House</a>
                                                         </h3>
                                                         <div class="item-readmore">
                                                            <a href="#">Read More</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="owl-nav">
                                          <div class="owl-prev"><i class="ti-angle-left"></i></div>
                                          <div class="owl-next"><i class="ti-angle-right"></i></div>
                                       </div>
                                       <div class="owl-dots disabled"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                     </div>
                  </article>
               </main>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
$(".owl-carousel").owlCarousel({
      autoPlay: 3000,
      items : 1, // THIS IS IMPORTANT
      responsive : {
            480 : { items : 1  }, // from zero to 480 screen width 4 items
            768 : { items : 2  }, // from 480 screen widthto 768 6 items
            1024 : { items : 3   // from 768 screen width to 1024 8 items
            }
        },
  });
  </script>
<div class="top-gallery">
   <div class="ct-carousel owl-carousel images-light-box-carousel owl-loaded owl-drag" data-item-xs="4" data-item-sm="6" data-item-md="8" data-item-lg="10" data-margin="0" data-loop="true" data-autoplay="true" data-autoplaytimeout="5000" data-smartspeed="250" data-center="false" data-arrows="false" data-bullets="false" data-stagepadding="0" data-stagepaddingsm="0" data-rtl="false">
      <div class="owl-stage-outer">
         <div class="owl-stage" style="transform: translate3d(-1483px, 0px, 0px); transition: all 0.25s ease 0s; width: 4048px;">
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic1.jpg') }}"><img src="{{asset('images/pic1-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic2.jpg') }}"><img src="{{ asset('images/pic2-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic3.jpg') }}"><img src="{{ asset('images/pic3-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic4.jpg') }}"><img src="{{ asset('images/pic4-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic5.jpg') }}"><img src="{{ asset('images/pic5-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic6.jpg') }}"><img src="{{ asset('images/pic6-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic7.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic8.jpg') }}"><img src="{{ asset('images/pic8-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic9.jpg') }}"><img src="{{ asset('images/pic9-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic10.jpg') }}"><img src="{{ asset('images/pic10-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic1.jpg') }}"><img src="{{ asset('images/pic1-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic2.jpg') }}"><img src="{{ asset('images/pic2-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic3.jpg') }}"><img src="{{ asset('images/pic3-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic4.jpg') }}"><img src="{{ asset('images/pic4-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic5.jpg') }}')"><img src="{{ asset('images/pic5-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic6.jpg') }}"><img src="{{ asset('images/pic6-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic7.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic8.jpg') }}"><img src="{{ asset('images/pic8-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic9.jpg') }}"><img src="{{ asset('images/pic9-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic10.jpg') }}"><img src="{{ asset('images/pic10-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned active" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic1.jpg') }}"><img src="{{ asset('images/pic1-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic2.jpg') }}"><img src="{{ asset('images/pic2-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic3.jpg') }}"><img src="{{ asset('images/pic3-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic4.jpg') }}"><img src="{{ asset('images/pic4-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic5.jpg') }}"><img src="{{ asset('images/pic5-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic6.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic7.jpg') }}"><img src="{{ asset('images/pic7-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic8.jpg') }}"><img src="{{ asset('images/pic8-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic9.jpg') }}"><img src="{{ asset('images/pic9-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 134.9px;">
               <div class="ct-carousel-item">
                  <a class="light-box" href="{{ asset('images/pic10.jpg') }}"><img src="{{ asset('images/pic10-480x430.jpg') }}" alt=""></a>
               </div>
            </div>
         </div>
      </div>
      <div class="owl-nav disabled">
         <div class="owl-prev"><i class="ti-angle-left"></i></div>
         <div class="owl-next"><i class="ti-angle-right"></i></div>
      </div>
      <div class="owl-dots disabled"></div>
   </div>
</div>