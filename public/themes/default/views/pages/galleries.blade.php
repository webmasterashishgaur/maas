<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>

<div id="pagetitle" class="page-title bg-overlay">
   <div class="container">
      <div class="page-title-inner">
         <h1 class="page-title">Training Courses & Workshop</h1>
         <ul class="ct-breadcrumb">
            <li><a class="breadcrumb-entry" href="#">Home</a></li>
            <li><span class="breadcrumb-entry">Training Courses & Workshop</span></li>
         </ul>
      </div>
   </div>
</div>


<div class="container">
         <!--<div class="row">
            <div class="col-lg-12 text-center my-2">
               <h4>Isotope filter magical layouts with Bootstrap 4</h4>
                <p class="border-bottom border-dark p-2">Any questions you can ask on twitter @nitesh75malviya</p>
            </div>
         </div>-->
         <div class="portfolio-menu mt-2 mb-4">
            <ul>
               <li class="btn btn-outline-dark active" data-filter="*">All</li>
               <li class="btn btn-outline-dark" data-filter=".gts">Training Courses</li>
               <li class="btn btn-outline-dark" data-filter=".lap">Workshop</li>
            </ul>
         </div>
         <div class="portfolio-item row">
            <div class="item selfie col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/about2.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light"> 
               <img class="img-fluid" src="{{ url('images/about2.jpg') }}" alt="">
               </a>
			   
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
			   
			   
			   
            </div>
            <div class="item gts col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light"> 
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
			   
            </div>
            <div class="item selfie col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog2-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog2-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item gts col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog3-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog3-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item gts col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog4-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog4-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item gts col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item lap col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog8-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog8-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item lap col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/pic1-480x430.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/pic1-480x430.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item gts col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item lap col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item gts col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item gts col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item gts col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item gts col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item lap col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item gts col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item gts col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item selfie col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item selfie col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item selfie col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item lap col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
            </div>
            <div class="item lap col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item lap col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item gts col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item gts col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item gts col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item lap col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item lap col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
            <div class="item selfie col-lg-3 col-md-4 col-6 col-sm">
               <a href="{{ url('images/blog1-1-500x500.jpg') }}" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="{{ url('images/blog1-1-500x500.jpg') }}" alt="">
               </a>
			     <div class="item-box-blog-body">
                    <div class="item-box-blog-heading">
                        <a href="#" tabindex="0">
                            <h5>Gallery Name</h5>
                               </a>
                    </div>
					<div class="item-box-blog-data">
						<p>Member price 150, | Normal Price 200</p>
					</div>
                    <div class="item-box-blog-text">
                              <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet.</p>
                            </div>
                          </div>
            </div>
         </div>
      </div>
<style>
.portfolio-menu{
	text-align:center;
}
.portfolio-menu ul li{
	display:inline-block;
	margin:0;
	list-style:none;
	padding:10px 15px;
	cursor:pointer;
	-webkit-transition:all 05s ease;
	-moz-transition:all 05s ease;
	-ms-transition:all 05s ease;
	-o-transition:all 05s ease;
	transition:all .5s ease;
}

.portfolio-item{
	width:100%;
}
.portfolio-item .item{
	width:303px;
	float:left;
	margin-bottom:10px;
	border: 1px solid #dadada;
	text-align: center;
	z-index: 4;
	padding: 20px;
}

</style>
<script type="text/javascript">
         $('.portfolio-menu ul li').click(function(){
         	$('.portfolio-menu ul li').removeClass('active');
         	$(this).addClass('active');
         	
         	var selector = $(this).attr('data-filter');
         	$('.portfolio-item').isotope({
         		filter:selector
         	});
         	return  false;
         });
         $(document).ready(function() {
         var popup_btn = $('.popup-btn');
         popup_btn.magnificPopup({
         type : 'image',
         gallery : {
         	enabled : true
         }
         });
         });

</script>