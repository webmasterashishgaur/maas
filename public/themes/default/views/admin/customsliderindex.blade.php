<div class="panel panel-default">
@include('flash::message')
<div class="panel-heading no-bg panel-settings">
	<h3 class="panel-title">
		{{ trans('admin.blogs') }}	
		<div class="btn-custom btn-rtl">
			<a class="btn btn-success" href="{{ url('admin/custom-sliders/create') }}">Create</a>
		</div>
	</h3>

</div>
<div class="panel-body">	
	<div class="announcement-container">	
		<table class="table table-responsive" id="timelines-table">
		    <thead>
		        <th>Images</th>
		        <th>{{ trans('common.status') }}</th>
		        <th colspan="3">{{ trans('admin.action') }}</th>
		    </thead>
		    <tbody>
		    @foreach($sliders as $slider)
		        <tr>	        	
		        	<td><img src="{{ url('images/slider/'.$slider->images) }}" style="width:100px;"></td>
		             <?php $status = $slider->status == 1 ? trans('admin.active') : trans('admin.inactive'); ?>
		            <td>{{ $status }}</td>
					<td><a href="{{ url('admin/custom-sliders/'.$slider->id.'/edit')}}">{{ trans('common.edit') }}</a></td>              		            
		        </tr>
		    @endforeach			    
		    </tbody>
		</table>			
	</div>
</div>
</div>