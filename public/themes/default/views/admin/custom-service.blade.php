@include('flash::message')
<div class="panel panel-default">

	<div class="panel-heading no-bg panel-settings">
		<h3 class="panel-title">
			{{ $mode.' '.trans('admin.custom_page') }}
		</h3>
	</div>
	<div class="panel-body">		
		@if($mode =="create")
			<form method="POST" class="socialite-form" action="{{ url('admin/storeCustomService') }}" enctype="multipart/form-data">
		@else
			<form method="POST" class="socialite-form" action="{{ url('admin/custom-service/'.$blogs->id.'/update') }}"enctype="multipart/form-data">
		@endif

			{{ csrf_field() }}
<div class="privacy-question">
				<fieldset class="form-group required {{ $errors->has('title') ? ' has-error' : '' }}">
					{{ Form::label('title', trans('admin.page_title')) }}
					@if($mode == "create")
						{{ Form::text('title', NULL , array('class' => 'form-control', 'placeholder' => trans('admin.page_title_placeholder') )) }}
					@else
						{{ Form::text('title', $blogs->title , array('class' => 'form-control', 'placeholder' => trans('admin.page_title_placeholder') )) }}
					@endif	
					@if ($errors->has('title'))
					<span class="help-block">
						{{ $errors->first('title') }}
					</span>
					@endif
				</fieldset>
					
						<fieldset class="form-group required {{ $errors->has('title') ? ' has-error' : '' }}">
					{{ Form::label('type', trans('type')) }}
					@if($mode == "create")
						{{ Form::text('type', NULL , array('class' => 'form-control', 'placeholder' => trans('type') )) }}
					@else
						{{ Form::text('type', $blogs->title , array('class' => 'form-control', 'placeholder' => trans('type') )) }}
					@endif	
					@if ($errors->has('title'))
					<span class="help-block">
						{{ $errors->first('title') }}
					</span>
					@endif
				</fieldset>
					
				<fieldset class="form-group">
					{{ Form::label('active', trans('common.status')) }}
					@if($mode == "create")
						{{ Form::select('active', array(1 => trans('admin.active'), 0 => trans('admin.inactive')), NULL, array('class' => 'form-control')) }}
					@else
						{{ Form::select('active', array(1 => trans('admin.active'), 0 => trans('admin.inactive')), $blogs->active , array('class' => 'form-control')) }}				
					@endif
				</fieldset>
				<div class="pull-right">
					{{ Form::submit(trans('common.save_changes'), ['class' => 'btn btn-success']) }}
				</div>
			</div>
		</form>
	</div>
</div><!-- /panel -->