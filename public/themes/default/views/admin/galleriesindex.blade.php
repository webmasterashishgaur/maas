<div class="panel panel-default">
@include('flash::message')
<div class="panel-heading no-bg panel-settings">
	<h3 class="panel-title">
		{{ trans('admin.custom_pages') }}	
		<div class="btn-custom btn-rtl">
			<a class="btn btn-success" href="{{ url('/admin/custom-galleries/create') }}">Create</a>
		</div>
	</h3>

</div>
<div class="panel-body">	
	<div class="announcement-container">	
		<table class="table table-responsive" id="timelines-table">
		    <thead>
		    	<th>Name</th>
		        <th>Normal Price</th>
		        <th>Member Price</th>
		        <th>Type</th>
				<th>Description</th>
				<th>Images</th>
				<th>Status</th>
		    </thead>
		    <tbody>
		    
		        <tr>	        	
		        	<td></td>
		            <td></td>
		             
		            <td></td>
					<td><a href=""></a></td>              		            
		        </tr> 
		    </tbody>
		</table>			
	</div>
</div>
</div>