@include('flash::message')
<div class="panel panel-default">

	<div class="panel-heading no-bg panel-settings">
		<h3 class="panel-title">
			Create Sliders
		</h3>
	</div>
	<div class="panel-body">		
		@if($mode =="create")
			<form method="POST" class="socialite-form" action="{{ url('admin/custom-sliders')}}" enctype="multipart/form-data">
		@else
			<form method="POST" class="socialite-form" action="{{ url('admin/custom-sliders/'.$sliders->id.'/update') }}" enctype="multipart/form-data">
		@endif

			{{ csrf_field() }}
			<fieldset class="form-group">
					{{ Form::label('active', trans('common.status')) }}
					@if($mode == "create")
						{{ Form::select('active', array(1 => trans('admin.active'), 0 => trans('admin.inactive')), NULL, array('class' => 'form-control')) }}
					@else
						{{ Form::select('active', array(1 => trans('admin.active'), 0 => trans('admin.inactive')), $sliders->active , array('class' => 'form-control')) }}				
					@endif
				</fieldset>
			<div class="privacy-question">
						    <div class="form-group row">
                                            <div class="col-md-6">
                                                <input id="blog_images" type="file" class="form-control" name="image" type="file" id="imageInput">
                                            </div>
                                        </div>
					
				<div class="pull-right">
					{{ Form::submit(trans('common.save_changes'), ['class' => 'btn btn-success']) }}
				</div>
			</div>
		</form>
	</div>
</div><!-- /panel -->