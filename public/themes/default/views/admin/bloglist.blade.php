<div class="panel panel-default">
@include('flash::message')
<div class="panel-heading no-bg panel-settings">
	<h3 class="panel-title">
		{{ trans('admin.blogs') }}	
		<div class="btn-custom btn-rtl">
			<a class="btn btn-success" href="{{ url('admin/blogs/create') }}">Create</a>
		</div>
	</h3>

</div>
<div class="panel-body">	
	<div class="announcement-container">	
		<table class="table table-responsive" id="timelines-table">
		    <thead>
		    	<th>{{ trans('admin.title') }}</th>
		        <th>{{ trans('common.description') }}</th>
		        <th>{{ trans('common.status') }}</th>
		        <th colspan="3">{{ trans('admin.action') }}</th>
		    </thead>
		    <tbody>
		        <tr>	        	
		        	<td>{{  }}</td>
		            <td>{{  }}</td>
		             <?php $status = $staticpage->active == 1 ? trans('admin.active') : trans(''); ?>
		            <td>{{  }}</td>
					<td><a href="{{ url('admin/custom-pages/'.$staticpage->id.'/edit')}}">{{ }}</a></td>              		            
		        </tr>	    
		    </tbody>
		</table>			
	</div>
</div>
</div>