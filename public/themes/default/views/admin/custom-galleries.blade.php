@include('flash::message')
<div class="panel panel-default">

	<div class="panel-heading no-bg panel-settings">
		<h3 class="panel-title">
			{{ $mode.' '.trans('admin.custom_page') }}
		</h3>
	</div>
	<div class="panel-body">		
			<form method="POST" class="socialite-form" action="{{ url('admin/storeGallery') }}" enctype="multipart/form-data">
			{{ csrf_field() }}
<div class="privacy-question">
				<fieldset class="form-group required {{ $errors->has('title') ? ' has-error' : '' }}">
					{{ Form::label('Name', trans('admin.gallery_title')) }}
					@if($mode == "create")
						{{ Form::text('title', NULL , array('class' => 'form-control', 'placeholder' => trans('admin.page_title_placeholder') )) }}
					@else
						{{ Form::text('title', $blogs->title , array('class' => 'form-control', 'placeholder' => trans('admin.page_title_placeholder') )) }}
					@endif	
					@if ($errors->has('title'))
					<span class="help-block">
						{{ $errors->first('title') }}
					</span>
					@endif
				</fieldset>
					
						<fieldset class="form-group required {{ $errors->has('title') ? ' has-error' : '' }}">
					{{ Form::label('type', trans('type')) }}
					@if($mode == "create")
						{{ Form::text('type', NULL , array('class' => 'form-control', 'placeholder' => trans('type') )) }}
					@else
						{{ Form::text('type', $blogs->title , array('class' => 'form-control', 'placeholder' => trans('type') )) }}
					@endif	
					@if ($errors->has('title'))
					<span class="help-block">
						{{ $errors->first('title') }}
					</span>
					@endif
				</fieldset>
				
					<fieldset class="form-group required {{ $errors->has('title') ? ' has-error' : '' }}">
					{{ Form::label('member_price', trans('member_price')) }}
					@if($mode == "create")
						{{ Form::text('member_price', NULL , array('class' => 'form-control', 'placeholder' => trans('member_price') )) }}
					@else
						{{ Form::text('member_price', $blogs->title , array('class' => 'form-control', 'placeholder' => trans('member_price') )) }}
					@endif	
					@if ($errors->has('title'))
					<span class="help-block">
						{{ $errors->first('title') }}
					</span>
					@endif
				</fieldset>
					<fieldset class="form-group required {{ $errors->has('title') ? ' has-error' : '' }}">
					{{ Form::label('normal_price', trans('normal_price')) }}
					@if($mode == "create")
						{{ Form::text('normal_price', NULL , array('class' => 'form-control', 'placeholder' => trans('normal_price') )) }}
					@else
						{{ Form::text('normal_price', $blogs->title , array('class' => 'form-control', 'placeholder' => trans('normal_price') )) }}
					@endif	
					@if ($errors->has('title'))
					<span class="help-block">
						{{ $errors->first('title') }}
					</span>
					@endif
				</fieldset>
				
				
				<fieldset class="form-group required {{ $errors->has('description') ? ' has-error' : '' }}">
					{{ Form::label('description', trans('admin.page_description')) }}
					@if($mode == "create")
						{{ Form::textarea('description', NULL , array('class' => 'form-control mytextarea', 'placeholder' => trans('messages.create_page_placeholder') )) }}
					@else
						{{ Form::textarea('description', $blogs->description , array('class' => 'form-control mytextarea', 'placeholder' => trans('messages.create_page_placeholder') )) }}
					@endif	
					@if ($errors->has('description'))
					<span class="help-block">
						{{ $errors->first('description') }}
					</span>
					@endif
				</fieldset>
					
				<fieldset class="form-group">
					{{ Form::label('active', trans('common.status')) }}
					@if($mode == "create")
						{{ Form::select('active', array(1 => trans('admin.active'), 0 => trans('admin.inactive')), NULL, array('class' => 'form-control')) }}
					@else
						{{ Form::select('active', array(1 => trans('admin.active'), 0 => trans('admin.inactive')), $blogs->active , array('class' => 'form-control')) }}				
					@endif
				</fieldset>
				    <div class="form-group row">
                                            <div class="col-md-6">
                                                <input id="blog_images" type="file" class="form-control" name="image" type="file" id="imageInput">
                                            </div>
                                        </div>
				
				<div class="pull-right">
					{{ Form::submit(trans('common.save_changes'), ['class' => 'btn btn-success']) }}
				</div>
			</div>
		</form>
	</div>
</div>