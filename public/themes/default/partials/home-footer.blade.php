
  <footer class="footer">
    <div class="container">
      <div class="widget col-lg-3 col-md-6 col-sm-12">
        <h4 class="title">Company</h4>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s standard dummy text ever since the 1500s..</p>
        <a class="button small" href="#">read more</a>
      </div>
      <!-- end widget -->
      <div class="widget col-lg-3 col-md-6 col-sm-12">
        <h4 class="title">Useful Links</h4>
        <ul class="recent_posts">
          <li>
            <a class="readmore" href="#">read more</a>
          </li>
          <li>
            <a class="readmore" href="#">read more</a>
          </li>
        </ul> 
        <!-- recent posts -->
      </div>
      <!-- end widget -->
      <div class="widget col-lg-3 col-md-6 col-sm-12">
        <h4 class="title">Get In Touch</h4>
        <ul class="contact_details">
          <li><i class="fa fa-envelope-o"></i> info@yoursite.com</li>
          <li><i class="fa fa-phone-square"></i> +123 456 789</li>
          <li><i class="fa fa-home"></i> Some Fine Address, 887, Madrid, Spain.</li>
          <li><a href="#"><i class="fa fa-map-marker"></i> View large map</a></li>
        </ul>
        <!-- contact_details -->
      </div>
      <!-- end widget -->
      <div class="widget col-lg-3 col-md-6 col-sm-12">
        <h4 class="title">social Media</h4>
        <ul class="flickr">
          <li><a href="https://www.facebook.com" target="_blank" class="margin-r-10 v-center" style="color: rgb(59, 89, 152);"><i class="fa fa-facebook h6"></i></a></li>
          <li></li>
          <li><a href="https://twitter.com" target="_blank" class="margin-r-10 v-center" style="color: rgb(85, 172, 238);"><i class="fa fa-twitter h6"></i></a></li>
          <li><a href="https://www.pinterest.com" target="_blank" class="margin-r-10 v-center" style="color: rgb(203, 32, 39);"><i class="fa fa-pinterest h6"></i></a></li>
          <li><a href="https://www.instagram.com" target="_blank" class="margin-r-10 v-center" style="color: rgb(233, 89, 80);"><i class="fa fa-instagram h6"></i></a></li>
          <li><a href="https://www.youtube.com/channel/UCe96CeIKcoexrE3dtAuBKxA" target="_blank" class="margin-r-10 v-center" style="color: rgb(255, 0, 0);"><i class="fa fa-youtube h6"></i></a></li>
        </ul>
      </div>
      <!-- end widget -->
    </div>
    <!-- end container -->

    <div class="copyrights">
      <div class="container">
        <div class="col-lg-6 col-md-6 col-sm-12 columns footer-left">
          <p>Copyright © 2020 - All rights reserved.</p>
          <div class="credits">
            <!--
              You are NOT allowed to delete the credit link to TemplateMag with free version.
              You can delete the credit link only if you bought the pro version.
              Buy the pro version with working PHP/AJAX contact form: https://templatemag.com/maxibiz-bootstrap-business-template/
              Licensing information: https://templatemag.com/license/
            -->
            Created MAAS
          </div>
        </div>
        <!-- end widget -->
        <div class="col-lg-6 col-md-6 col-sm-12 columns text-right">
          <div class="footer-menu right">
            <ul class="menu">
              <li><a href="#">Home</a></li>
              <li><a href="#">About</a></li>
              <li><a href="#">Sitemap</a></li>
              <li><a href="#">Site Terms</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </div>
        </div>
        <!-- end large-6 -->
      </div>
      <!-- end container -->
    </div>
    <!-- end copyrights -->
  </footer>
  <!-- end footer -->
  <div class="dmtop">Scroll to Top</div>

   @if (Auth::guest())
<!-- Modal -->
<div id="loginModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="log-box">
          <div class="row no-gutters">
            <div class="col-xl-5 col-sm-5 col-12 pad-right-0">
              <div class="logo-back">
                <img src="{{ Theme::asset()->url('images/login_bg.jpg') }}">
              </div>
            </div>
            <div class="col-xl-7 col-sm-7 col-12 pad-left-0">
              <div class="log-content">
                <h3>LOGIN </h3>
                <div class="log-body">
<form method="POST" class="login-form" action="{{ url('/login') }}">
          {{ csrf_field() }}
          <fieldset class="form-group mail-form {{ $errors->has('email') ? ' has-error' : '' }}">
            {{ Form::text('email', NULL, ['class' => 'form-control custom', 'id' => 'email', 'placeholder'=> trans('auth.enter_email_or_username')]) }}
          </fieldset>
          <fieldset class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            {{ Form::password('password', ['class' => 'form-control custom', 'id' => 'password', 'placeholder'=> trans('auth.password')]) }}
          </fieldset>
          {{ Form::button( trans('common.signin') , ['type' => 'submit','class' => 'btn btn-theme1']) }}
        </form>
                <div class="log-bottom-cotent">
                <p>Creat an account <a href="signup.html">Sign Up</a>
                <a href="{{ url('/password/reset') }}" class="pull-right">Forgot Password</a>
                </p>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>

  </div>
</div>


      @endif

      
{!! Theme::asset()->container('footer')->usePath()->add('app', 'js/app.js') !!}