<div class="topbar clearfix">
    <div class="container">
      <div class="col-lg-12 text-right">

        <div class="col-lg-6 col-md-6 col-sm-12 header-left">
          Tagline for the website is here
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 header-right">

          <div class="login-head">
            @if (Auth::guest())  
            <input type="button" class="lnbtn" data-toggle="modal" data-target="#loginModal" value="Login">
             @elseif(Auth::user())
             vendor
             @else
            <input type="button" class="lnbtn" value="Dashboard"> 
<form action="{{ url('/logout') }}" method="post" style="float: left;">
                    {{ csrf_field() }}

                    <button type="submit" class="lnbtn"><i class="fa fa-unlock" aria-hidden="true"></i>{{ trans('common.logout') }}</button>
                  </form>
            @endif
          </div>
          <!--<div class="head-links">
            <a href="#" data-toggle="tooltip" data-placement="bottom" title="testimonials">TESTIMONIALS</a>
          </div>-->
          <div class="social_buttons">
            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>
            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>
            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Youtube"><i class="fa fa-youtube"></i></a>
          </div>
          
        </div>

      </div>
    </div>
    <!-- end container -->
  </div>
  <!-- end topbar -->

  <header class="header">
    <div class="container">
      <div class="site-header clearfix">
      <div class="col-lg-5 col-md-12 col-sm-12 menuitems">

        <div class="left">
          <ul id="jetmenu" class="jetmenu blue">
            <li class="active"><a href="{{ url('/') }}">Home</a>
            </li>
            <li><a href="{{ url('/about-us') }}">About Us</a>
            </li>
             <li><a href="#">Join Mass</a>
                <ul class="dropdown">
                  <li><a href="#">Member Benefits</a></li>
                  <li><a href="{{ url('/join-mass') }}">Join MASS Form</a></li>
                  <li><a href="#">Thankyou Form</a></li>
                </ul>
              </li>
            <li><a href="{{ url('/services') }}">Our Services</a>
          </ul>
        </div>

      </div>
      <div class="col-lg-2 col-md-2 col-sm-12 logo-area">
          <img class="socialite-logo" src="{!! url('setting/'.Setting::get('logo')) !!}" alt="{{ Setting::get('site_name') }}" title="{{ Setting::get('site_name') }}">
      </div>
      <div class="col-lg-5 col-md-12 col-sm-12 menuitems">
        <div id="nav" class="right">
          <ul id="jetmenu" class="jetmenu blue mytopMenu">
          <li class="cc active"><a href="{{ url('/') }}">Home</a>
            </li>
        
          <li class="cc"><a href="{{ url('/about-us') }}">About Us</a>
            </li>
           <li class="cc"><a href="{{url('/get-accredited')}}">Join Mass</a>
              <!--<ul class="dropdown">
                <li><a href="#">Member Benefits</a></li>
                <li><a href="#">Join MASS Form</a></li>
                <li><a href="#">Thankyou Form</a></li>
              </ul>-->
            </li>
          <li class="cc"><a href="{{ url('/services') }}">Our Services</a>
            <li><a href="{{url('/get-accredited')}}">Get accredited by MAAS</a>
            </li>
             <li><a href="#">Events</a>
                <ul class="dropdown">
                  <li><a href="{{url('/galleries')}}">Training Courses & Workshop</a></li>
                  <li><a href="#">News</a></li>
                  <!--<li><a href="#">Signup For Newsletter</a></li>-->
                </ul>
              </li>
             <li><a href="{{ url('/contact') }}">Contact Us</a>
                <!--<ul class="dropdown">
                  <li><a href="#">Partner with Us</a></li>
                </ul>-->
              </li>
			  <li><a href="{{ url('/testimonials') }}">TESTIMONIALSvvv</a>
            </li>
          </ul>
        </div>
      </div>
       
      </div>
      <!-- site header -->
    </div>
    <!-- end container -->
  </header>
  <!-- end header -->

 