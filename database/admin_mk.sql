-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 10, 2020 at 05:19 AM
-- Server version: 5.7.29-0ubuntu0.16.04.1
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_mk`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` int(10) UNSIGNED NOT NULL,
  `timeline_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `about` text COLLATE utf8_unicode_ci NOT NULL,
  `preview_id` int(10) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `privacy` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `album_media`
--

CREATE TABLE `album_media` (
  `id` int(10) UNSIGNED NOT NULL,
  `album_id` int(10) UNSIGNED NOT NULL,
  `media_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `announcement_user`
--

CREATE TABLE `announcement_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `announcement_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `parent_id`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Airport', 'description about Airport', 1, 1, '2020-04-23 10:21:25', '2020-04-23 10:21:25', NULL),
(2, 'Automotive', 'description about Automotive', 2, 1, '2020-04-23 10:21:26', '2020-04-23 10:21:26', NULL),
(3, 'Bank/Financial Services', 'description about Bank/Financial Services', 3, 1, '2020-04-23 10:21:26', '2020-04-23 10:21:26', NULL),
(4, 'Bar', 'description about Bar', 4, 1, '2020-04-23 10:21:26', '2020-04-23 10:21:26', NULL),
(5, 'Book Store', 'description about Book Store', 5, 1, '2020-04-23 10:21:26', '2020-04-23 10:21:26', NULL),
(6, 'Business Services', 'description about Business Services', 6, 1, '2020-04-23 10:21:26', '2020-04-23 10:21:26', NULL),
(7, 'Church/Religious Organization', 'description about Church/Religious Organization', 7, 1, '2020-04-23 10:21:26', '2020-04-23 10:21:26', NULL),
(8, 'Club', 'description about Club', 8, 1, '2020-04-23 10:21:26', '2020-04-23 10:21:26', NULL),
(9, 'Concert Venue', 'description about Concert Venue', 9, 1, '2020-04-23 10:21:26', '2020-04-23 10:21:26', NULL),
(10, 'Doctor', 'description about Doctor', 10, 1, '2020-04-23 10:21:26', '2020-04-23 10:21:26', NULL),
(11, 'Education', 'description about Education', 11, 1, '2020-04-23 10:21:27', '2020-04-23 10:21:27', NULL),
(12, 'Event Planning/Event Services', 'description about Event Planning/Event Services', 12, 1, '2020-04-23 10:21:27', '2020-04-23 10:21:27', NULL),
(13, 'Home Improvement', 'description about Home Improvement', 13, 1, '2020-04-23 10:21:27', '2020-04-23 10:21:27', NULL),
(14, 'Hotel', 'description about Hotel', 14, 1, '2020-04-23 10:21:27', '2020-04-23 10:21:27', NULL),
(15, 'Landmark', 'description about Landmark', 15, 1, '2020-04-23 10:21:27', '2020-04-23 10:21:27', NULL),
(16, 'category1', 'description about category1', 16, 1, '2020-04-23 10:21:27', '2020-04-23 10:21:27', NULL),
(17, 'category2', 'description about category2', 17, 1, '2020-04-23 10:21:27', '2020-04-23 10:21:27', NULL),
(18, 'category3', 'description about category3', 18, 1, '2020-04-23 10:21:27', '2020-04-23 10:21:27', NULL),
(19, 'category4', 'description about category4', 19, 1, '2020-04-23 10:21:27', '2020-04-23 10:21:27', NULL),
(20, 'category5', 'description about category5', 20, 1, '2020-04-23 10:21:28', '2020-04-23 10:21:28', NULL),
(21, 'category6', 'description about category6', 21, 1, '2020-04-23 10:21:28', '2020-04-23 10:21:28', NULL),
(22, 'category7', 'description about category7', 22, 1, '2020-04-23 10:21:28', '2020-04-23 10:21:28', NULL),
(23, 'category8', 'description about category8', 23, 1, '2020-04-23 10:21:29', '2020-04-23 10:21:29', NULL),
(24, 'category9', 'description about category9', 24, 1, '2020-04-23 10:21:30', '2020-04-23 10:21:30', NULL),
(25, 'category10', 'description about category10', 25, 1, '2020-04-23 10:21:30', '2020-04-23 10:21:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `media_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `description`, `user_id`, `parent_id`, `created_at`, `updated_at`, `deleted_at`, `media_id`) VALUES
(1, 3, 'sdfsadf', 3, NULL, '2020-04-27 14:54:40', '2020-04-27 14:54:40', NULL, NULL),
(2, 3, 'sdafsadf', 3, 1, '2020-04-27 14:54:43', '2020-04-27 14:54:43', NULL, NULL),
(3, 15, 'testing', 3, NULL, '2020-04-28 08:38:05', '2020-04-28 08:38:05', NULL, NULL),
(4, 6, 'Tst', 3, NULL, '2020-04-28 08:38:29', '2020-04-28 08:38:29', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comment_likes`
--

CREATE TABLE `comment_likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `comment_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `timeline_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `invite_privacy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timeline_post_privacy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event_user`
--

CREATE TABLE `event_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE `followers` (
  `id` int(10) UNSIGNED NOT NULL,
  `follower_id` int(10) UNSIGNED NOT NULL,
  `leader_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `followers`
--

INSERT INTO `followers` (`id`, `follower_id`, `leader_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 1, 'approved', NULL, NULL, NULL),
(4, 3, 2, 'approved', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `timeline_id` int(10) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `member_privacy` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `post_privacy` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `event_privacy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `group_user`
--

CREATE TABLE `group_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hashtags`
--

CREATE TABLE `hashtags` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `last_trend_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `count` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `title`, `type`, `source`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Prof. Kyra Gislason IV', 'image', '7.jpg', '2020-04-23 10:21:18', '2020-04-23 10:21:18', NULL),
(2, 'Miss Anahi Cronin PhD', 'image', '1.png', '2020-04-23 10:21:18', '2020-04-23 10:21:18', NULL),
(3, 'Jaren Kilback', 'image', '11.jpg', '2020-04-23 10:21:18', '2020-04-23 10:21:18', NULL),
(4, 'Mrs. Jody Lakin', 'image', '8.jpg', '2020-04-23 10:21:18', '2020-04-23 10:21:18', NULL),
(5, 'Marlen Heathcote', 'image', '12.jpg', '2020-04-23 10:21:18', '2020-04-23 10:21:18', NULL),
(6, 'Destany Kovacek', 'image', '1.png', '2020-04-23 10:21:18', '2020-04-23 10:21:18', NULL),
(7, 'Prof. Carlos Kiehn PhD', 'image', '3.jpg', '2020-04-23 10:21:18', '2020-04-23 10:21:18', NULL),
(8, 'Concepcion Will', 'image', '10.jpg', '2020-04-23 10:21:18', '2020-04-23 10:21:18', NULL),
(9, 'Genoveva Stiedemann', 'image', '5.jpg', '2020-04-23 10:21:19', '2020-04-23 10:21:19', NULL),
(10, 'Annabelle Cummerata', 'image', '8.jpg', '2020-04-23 10:21:19', '2020-04-23 10:21:19', NULL),
(11, 'Christ Carroll DDS', 'image', '4.png', '2020-04-23 10:21:19', '2020-04-23 10:21:19', NULL),
(12, 'Evangeline McDermott', 'image', '9.jpg', '2020-04-23 10:21:19', '2020-04-23 10:21:19', NULL),
(13, 'Dr. Clotilde Doyle Sr.', 'image', '7.jpg', '2020-04-23 10:21:19', '2020-04-23 10:21:19', NULL),
(14, 'Ashleigh Johnston', 'image', '3.jpg', '2020-04-23 10:21:19', '2020-04-23 10:21:19', NULL),
(15, 'Zion Howe', 'image', '11.jpg', '2020-04-23 10:21:19', '2020-04-23 10:21:19', NULL),
(16, 'Raheem Weissnat', 'image', '2.png', '2020-04-23 10:21:19', '2020-04-23 10:21:19', NULL),
(17, 'Cora Torp', 'image', '3.jpg', '2020-04-23 10:21:19', '2020-04-23 10:21:19', NULL),
(18, 'Misty Reichel V', 'image', '1.png', '2020-04-23 10:21:19', '2020-04-23 10:21:19', NULL),
(19, 'Shaniya Kirlin', 'image', '12.jpg', '2020-04-23 10:21:20', '2020-04-23 10:21:20', NULL),
(20, 'Tatum Ebert', 'image', '6.jpg', '2020-04-23 10:21:20', '2020-04-23 10:21:20', NULL),
(21, 'Mitchell Morar', 'image', '1.png', '2020-04-23 10:21:20', '2020-04-23 10:21:20', NULL),
(22, 'Marcos Huel', 'image', '2.jpg', '2020-04-23 10:21:20', '2020-04-23 10:21:20', NULL),
(23, 'Dr. Rebeca Keeling IV', 'image', '6.jpg', '2020-04-23 10:21:20', '2020-04-23 10:21:20', NULL),
(24, 'Candida Herman', 'image', '1.png', '2020-04-23 10:21:20', '2020-04-23 10:21:20', NULL),
(25, 'Isaias Wilderman', 'image', '3.png', '2020-04-23 10:21:20', '2020-04-23 10:21:20', NULL),
(26, 'Mr. Bud Stamm', 'image', '3.jpg', '2020-04-23 10:21:20', '2020-04-23 10:21:20', NULL),
(27, 'Ms. Sadye Goldner', 'image', '13.jpg', '2020-04-23 10:21:20', '2020-04-23 10:21:20', NULL),
(28, 'Buck Hahn', 'image', '1.jpg', '2020-04-23 10:21:20', '2020-04-23 10:21:20', NULL),
(29, 'Dorian Rice DDS', 'image', '6.jpg', '2020-04-23 10:21:21', '2020-04-23 10:21:21', NULL),
(30, 'Ignacio Osinski', 'image', '6.jpg', '2020-04-23 10:21:21', '2020-04-23 10:21:21', NULL),
(31, 'Mr. Wallace Ziemann III', 'image', '7.jpg', '2020-04-23 10:21:21', '2020-04-23 10:21:21', NULL),
(32, 'Easton Marvin', 'image', '3.jpg', '2020-04-23 10:21:21', '2020-04-23 10:21:21', NULL),
(33, 'Prof. Matt Beahan', 'image', '8.jpg', '2020-04-23 10:21:21', '2020-04-23 10:21:21', NULL),
(34, 'Charles Abernathy', 'image', '2.jpg', '2020-04-23 10:21:21', '2020-04-23 10:21:21', NULL),
(35, 'Nichole Wiza', 'image', '11.jpg', '2020-04-23 10:21:21', '2020-04-23 10:21:21', NULL),
(36, 'Arthur Pagac', 'image', '4.jpg', '2020-04-23 10:21:21', '2020-04-23 10:21:21', NULL),
(37, 'Pinkie Paucek', 'image', '10.jpg', '2020-04-23 10:21:22', '2020-04-23 10:21:22', NULL),
(38, 'Arielle Hodkiewicz', 'image', '13.jpg', '2020-04-23 10:21:22', '2020-04-23 10:21:22', NULL),
(39, 'Holly Turner I', 'image', '2.jpg', '2020-04-23 10:21:22', '2020-04-23 10:21:22', NULL),
(40, 'Carolyn Stokes', 'image', '10.jpg', '2020-04-23 10:21:22', '2020-04-23 10:21:22', NULL),
(41, 'Lawrence Cummings', 'image', '2.png', '2020-04-23 10:21:22', '2020-04-23 10:21:22', NULL),
(42, 'Dr. Jonathan Satterfield PhD', 'image', '3.jpg', '2020-04-23 10:21:22', '2020-04-23 10:21:22', NULL),
(43, 'Ms. Leanne Blick PhD', 'image', '4.png', '2020-04-23 10:21:22', '2020-04-23 10:21:22', NULL),
(44, 'Golden Hahn', 'image', '3.png', '2020-04-23 10:21:22', '2020-04-23 10:21:22', NULL),
(45, 'Athena Wilderman', 'image', '6.jpg', '2020-04-23 10:21:22', '2020-04-23 10:21:22', NULL),
(46, 'Augusta Witting', 'image', '4.jpg', '2020-04-23 10:21:22', '2020-04-23 10:21:22', NULL),
(47, 'Alessandra Bartoletti', 'image', '13.jpg', '2020-04-23 10:21:22', '2020-04-23 10:21:22', NULL),
(48, 'Kelly Mohr', 'image', '12.jpg', '2020-04-23 10:21:22', '2020-04-23 10:21:22', NULL),
(49, 'Ms. Hillary Hand', 'image', '4.png', '2020-04-23 10:21:22', '2020-04-23 10:21:22', NULL),
(50, 'Amie Schumm', 'image', '13.jpg', '2020-04-23 10:21:23', '2020-04-23 10:21:23', NULL),
(51, 'Mrs. Erika Ankunding MD', 'image', '11.jpg', '2020-04-23 10:21:23', '2020-04-23 10:21:23', NULL),
(52, 'Loren VonRueden', 'image', '5.jpg', '2020-04-23 10:21:23', '2020-04-23 10:21:23', NULL),
(53, 'Khalil Kihn', 'image', '8.jpg', '2020-04-23 10:21:23', '2020-04-23 10:21:23', NULL),
(54, 'Paul Kautzer', 'image', '13.jpg', '2020-04-23 10:21:23', '2020-04-23 10:21:23', NULL),
(55, 'Prof. Bobbie Orn II', 'image', '6.jpg', '2020-04-23 10:21:23', '2020-04-23 10:21:23', NULL),
(56, 'Mara Swaniawski', 'image', '4.png', '2020-04-23 10:21:23', '2020-04-23 10:21:23', NULL),
(57, 'Graham Lemke', 'image', '1.png', '2020-04-23 10:21:23', '2020-04-23 10:21:23', NULL),
(58, 'Prof. Nicklaus Kohler', 'image', '8.jpg', '2020-04-23 10:21:23', '2020-04-23 10:21:23', NULL),
(59, 'Timmy Feeney III', 'image', '4.png', '2020-04-23 10:21:23', '2020-04-23 10:21:23', NULL),
(60, 'Walker Cruickshank', 'image', '13.jpg', '2020-04-23 10:21:23', '2020-04-23 10:21:23', NULL),
(61, 'Conner Lehner DVM', 'image', '1.jpg', '2020-04-23 10:21:24', '2020-04-23 10:21:24', NULL),
(62, 'Prof. Leonardo Kautzer', 'image', '9.jpg', '2020-04-23 10:21:24', '2020-04-23 10:21:24', NULL),
(63, 'Prof. Immanuel Stoltenberg', 'image', '1.jpg', '2020-04-23 10:21:24', '2020-04-23 10:21:24', NULL),
(64, 'Davin Orn', 'image', '4.jpg', '2020-04-23 10:21:24', '2020-04-23 10:21:24', NULL),
(65, 'Durward Satterfield', 'image', '12.jpg', '2020-04-23 10:21:24', '2020-04-23 10:21:24', NULL),
(66, 'Mrs. Tressa Yundt', 'image', '2.jpg', '2020-04-23 10:21:24', '2020-04-23 10:21:24', NULL),
(67, 'Prof. Shirley Grant', 'image', '4.jpg', '2020-04-23 10:21:24', '2020-04-23 10:21:24', NULL),
(68, 'Elnora Block', 'image', '7.jpg', '2020-04-23 10:21:24', '2020-04-23 10:21:24', NULL),
(69, 'Monserrat Lindgren Jr.', 'image', '5.jpg', '2020-04-23 10:21:24', '2020-04-23 10:21:24', NULL),
(70, 'Chase Kirlin', 'image', '5.jpg', '2020-04-23 10:21:24', '2020-04-23 10:21:24', NULL),
(71, 'Jared Cassin', 'image', '11.jpg', '2020-04-23 10:21:24', '2020-04-23 10:21:24', NULL),
(72, 'Krista Schmidt', 'image', '4.png', '2020-04-23 10:21:24', '2020-04-23 10:21:24', NULL),
(73, 'Miss Makayla Heaney III', 'image', '1.png', '2020-04-23 10:21:24', '2020-04-23 10:21:24', NULL),
(74, 'Lyda Upton', 'image', '10.jpg', '2020-04-23 10:21:25', '2020-04-23 10:21:25', NULL),
(75, 'Twila Wolf V', 'image', '12.jpg', '2020-04-23 10:21:25', '2020-04-23 10:21:25', NULL),
(76, 'Melody Runte', 'image', '2.jpg', '2020-04-23 10:21:25', '2020-04-23 10:21:25', NULL),
(77, 'Terrence Lehner', 'image', '2.jpg', '2020-04-23 10:21:25', '2020-04-23 10:21:25', NULL),
(78, 'Maryjane Christiansen', 'image', '6.jpg', '2020-04-23 10:21:25', '2020-04-23 10:21:25', NULL),
(79, 'Aglae Zieme', 'image', '2.png', '2020-04-23 10:21:25', '2020-04-23 10:21:25', NULL),
(80, 'Rusty Barton', 'image', '13.jpg', '2020-04-23 10:21:25', '2020-04-23 10:21:25', NULL),
(81, '2020-04-23-10-04-27makeup.png', 'image', '2020-04-23-10-04-27makeup.png', '2020-04-23 14:04:29', '2020-04-23 14:04:29', NULL),
(82, '2020-04-23-10-11-15IMG_20131006_095646.jpg', 'image', '2020-04-23-10-11-15IMG_20131006_095646.jpg', '2020-04-23 14:11:15', '2020-04-23 14:11:15', NULL),
(83, '2020-04-27-11-27-59logo-light.png', 'image', '2020-04-27-11-27-59logo-light.png', '2020-04-27 15:27:59', '2020-04-27 15:27:59', NULL),
(84, '2020-04-27-11-37-06pngbarn.png', 'image', '2020-04-27-11-37-06pngbarn.png', '2020-04-27 15:37:06', '2020-04-27 15:37:06', NULL),
(85, '2020-04-27-11-37-17WhatsAppImage2020-04-12at1.15.46AM.jpeg', 'image', '2020-04-27-11-37-17WhatsAppImage2020-04-12at1.15.46AM.jpeg', '2020-04-27 15:37:17', '2020-04-27 15:37:17', NULL),
(86, '2020-04-27-11-38-09PngItem_1584975.png', 'image', '2020-04-27-11-38-09PngItem_1584975.png', '2020-04-27 15:38:09', '2020-04-27 15:38:09', NULL),
(87, '2020-04-27-15-31-57logo-light.png', 'image', '2020-04-27-15-31-57logo-light.png', '2020-04-27 19:31:57', '2020-04-27 19:31:57', NULL),
(88, '2020-04-27-15-32-02untitled-3.png', 'image', '2020-04-27-15-32-02untitled-3.png', '2020-04-27 19:32:02', '2020-04-27 19:32:02', NULL),
(89, '2020-04-27-15-32-14ECommerceDB.png', 'image', '2020-04-27-15-32-14ECommerceDB.png', '2020-04-27 19:32:14', '2020-04-27 19:32:14', NULL),
(90, '2020-04-27-15-38-43untitled-3.png', 'image', '2020-04-27-15-38-43untitled-3.png', '2020-04-27 19:38:43', '2020-04-27 19:38:43', NULL),
(91, '2020-04-27-17-04-38cambodia.jpg', 'image', '2020-04-27-17-04-38cambodia.jpg', '2020-04-27 21:04:38', '2020-04-27 21:04:38', NULL),
(92, '2020-04-27-17-06-26bg3.jpg', 'image', '2020-04-27-17-06-26bg3.jpg', '2020-04-27 21:06:26', '2020-04-27 21:06:26', NULL),
(93, '2020-04-27-17-10-02a0979696ab2f9df3cc1ce9ae8af00ddf_20-social-media-icons-695.png', 'image', '2020-04-27-17-10-02a0979696ab2f9df3cc1ce9ae8af00ddf_20-social-media-icons-695.png', '2020-04-27 21:10:02', '2020-04-27 21:10:02', NULL),
(94, '2020-04-27-17-11-34WhatsAppImage2020-04-10at14.56.13.jpeg', 'image', '2020-04-27-17-11-34WhatsAppImage2020-04-10at14.56.13.jpeg', '2020-04-27 21:11:35', '2020-04-27 21:11:35', NULL),
(95, '2020-04-28-18-50-3194.jpg', 'image', '2020-04-28-18-50-3194.jpg', '2020-04-28 22:50:31', '2020-04-28 22:50:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `thread_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2014_10_12_100000_create_settings_table', 1),
(3, '2014_10_28_175635_create_threads_table', 1),
(4, '2014_10_28_175710_create_messages_table', 1),
(5, '2014_10_28_180224_create_participants_table', 1),
(6, '2014_11_03_154831_add_soft_deletes_to_participants_table', 1),
(7, '2014_11_10_083449_add_nullable_to_last_read_in_participants_table', 1),
(8, '2014_11_20_131739_alter_last_read_in_participants_table', 1),
(9, '2014_12_04_124531_add_softdeletes_to_threads_table', 1),
(10, '2016_05_11_102459_create_categories_table', 1),
(11, '2016_05_11_102459_create_followers_table', 1),
(12, '2016_05_11_102459_create_group_user_table', 1),
(13, '2016_05_11_102459_create_groups_table', 1),
(14, '2016_05_11_102459_create_languages_table', 1),
(15, '2016_05_11_102459_create_media_table', 1),
(16, '2016_05_11_102459_create_page_likes_table', 1),
(17, '2016_05_11_102459_create_page_user_table', 1),
(18, '2016_05_11_102459_create_pages_table', 1),
(19, '2016_05_11_102459_create_post_follows_table', 1),
(20, '2016_05_11_102459_create_post_likes_table', 1),
(21, '2016_05_11_102459_create_post_media_table', 1),
(22, '2016_05_11_102459_create_post_shares_table', 1),
(23, '2016_05_11_102459_create_posts_table', 1),
(24, '2016_05_11_102459_create_timelines_table', 1),
(25, '2016_05_11_102459_create_user_settings_table', 1),
(26, '2016_05_11_102459_create_users_table', 1),
(27, '2016_05_11_102460_create_timeline_reports_table', 1),
(28, '2016_05_11_102500_create_announcement_user_table', 1),
(29, '2016_05_11_102500_create_announcements_table', 1),
(30, '2016_05_11_102500_create_comment_likes_table', 1),
(31, '2016_05_11_102500_create_comments_table', 1),
(32, '2016_05_11_102500_create_hashtags_table', 1),
(33, '2016_05_11_102500_create_notifications_table', 1),
(34, '2016_05_11_102500_create_post_reports_table', 1),
(35, '2016_07_08_170940_create_post_tags_table', 1),
(36, '2016_08_01_123157_entrust_setup_tables', 1),
(37, '2016_08_02_123157_create_foreign_keys', 1),
(38, '2016_08_03_103604_create_static_pages_table', 1),
(39, '2016_08_28_194209_alter_users', 1),
(40, '2016_08_31_174439_insert_settings', 1),
(41, '2016_09_04_022020_database_update_one_dot_two', 1),
(42, '2016_09_05_224813_database_update_one_dot_three', 1),
(43, '2016_10_24_070240_database_update_one_dot_four', 1),
(44, '2016_11_12_064152_database_update_two_dot_one', 1),
(45, '2017_02_26_074925_create_sessions_table', 1),
(46, '2017_03_01_135034_database_update_two_dot_two', 1),
(47, '2017_05_18_035912_create_wallpapers', 1),
(48, '2017_05_18_051905_database_update_three', 1),
(49, '2017_05_25_085951_alter_messages_table', 1),
(50, '2017_07_10_114459_create_saved_timelines_table', 1),
(51, '2017_07_10_124410_create_saved_posts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED DEFAULT NULL,
  `timeline_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `notified_by` int(10) UNSIGNED NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `post_id`, `timeline_id`, `user_id`, `notified_by`, `seen`, `description`, `type`, `link`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 1, 1, 3, 0, 'ashish is following you', 'follow', NULL, '2020-04-27 15:28:04', '2020-04-27 15:28:04', NULL),
(2, 5, 1, 1, 3, 0, 'ashish posted on your timeline', 'user', 'bootstrapguru', '2020-04-27 15:29:59', '2020-04-27 15:29:59', NULL),
(3, 6, NULL, 1, 3, 0, 'ashish mentioned you in their post', 'mention', 'post/6', '2020-04-27 15:30:24', '2020-04-27 15:30:24', NULL),
(4, NULL, 2, 2, 3, 0, 'ashish is following you', 'follow', NULL, '2020-04-27 15:38:19', '2020-04-27 15:38:19', NULL),
(5, NULL, 2, 2, 3, 0, 'ashish is unfollowing you', 'unfollow', NULL, '2020-04-27 15:38:25', '2020-04-27 15:38:25', NULL),
(6, NULL, 2, 2, 3, 0, 'ashish is following you', 'follow', NULL, '2020-04-27 15:38:30', '2020-04-27 15:38:30', NULL),
(7, 11, NULL, 2, 3, 0, 'ashish mentioned you in their post', 'mention', 'post/11', '2020-04-27 19:39:08', '2020-04-27 19:39:08', NULL),
(8, 1, NULL, 1, 3, 0, 'ashish unshared your post', 'unshare_post', '/ashish', '2020-04-27 19:41:39', '2020-04-27 19:41:39', NULL),
(9, NULL, 2, 2, 3, 0, 'ashish is unfollowing you', 'unfollow', NULL, '2020-04-29 21:17:35', '2020-04-29 21:17:35', NULL),
(10, NULL, 2, 2, 3, 0, 'ashish is following you', 'follow', NULL, '2020-04-29 21:17:39', '2020-04-29 21:17:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `timeline_id` int(10) UNSIGNED NOT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `category_id` int(10) UNSIGNED NOT NULL,
  `message_privacy` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `member_privacy` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timeline_post_privacy` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `page_likes`
--

CREATE TABLE `page_likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `page_user`
--

CREATE TABLE `page_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE `participants` (
  `id` int(10) UNSIGNED NOT NULL,
  `thread_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `last_read` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `timeline_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `soundcloud_title` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `soundcloud_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube_video_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `shared_post_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `description`, `timeline_id`, `user_id`, `active`, `soundcloud_title`, `soundcloud_id`, `youtube_title`, `youtube_video_id`, `location`, `type`, `created_at`, `updated_at`, `deleted_at`, `shared_post_id`) VALUES
(1, 'dfgdfgd', 1, 1, 1, '', '', '', '', '', NULL, '2020-04-23 14:04:00', '2020-04-23 14:04:00', NULL, NULL),
(2, 'fdsafsadf', 3, 3, 1, '', '', '', '', '', NULL, '2020-04-27 14:24:28', '2020-04-27 14:24:28', NULL, NULL),
(3, 'asvsdavsadv', 3, 3, 1, '', '', '', '', '', NULL, '2020-04-27 14:54:36', '2020-04-27 14:54:36', NULL, NULL),
(4, 'safdsadfsa', 3, 3, 1, '', '', '', '', '', NULL, '2020-04-27 15:27:59', '2020-04-27 15:27:59', NULL, NULL),
(5, '#ashish', 1, 3, 1, '', '', '', '', '', NULL, '2020-04-27 15:29:59', '2020-04-27 15:29:59', NULL, NULL),
(6, '@bootstrapguru', 1, 3, 1, '', '', '', '', '', NULL, '2020-04-27 15:30:24', '2020-04-27 15:30:24', NULL, NULL),
(7, 'my name is ashihs', 3, 3, 1, '', '', '', '', '', NULL, '2020-04-27 15:37:50', '2020-04-27 15:37:50', NULL, NULL),
(8, 'hi this is something', 3, 3, 1, '', '', '', '', '', NULL, '2020-04-27 15:38:08', '2020-04-27 15:38:08', NULL, NULL),
(9, 'jsdaklfjaslkdfj;lasd', 3, 3, 1, '', '', '', '', '', NULL, '2020-04-27 19:34:48', '2020-04-27 19:34:48', NULL, NULL),
(10, 'asfdsdafsdaf', 3, 3, 1, '', '', '', '', '', NULL, '2020-04-27 19:38:42', '2020-04-27 19:38:42', NULL, NULL),
(11, '@vijay', 3, 3, 1, '', '', '', '', '', NULL, '2020-04-27 19:39:08', '2020-04-27 19:39:08', NULL, NULL),
(13, 'test', 3, 3, 1, '', '', '', '', '', NULL, '2020-04-27 19:59:44', '2020-04-27 19:59:44', NULL, NULL),
(14, 'test', 3, 3, 1, '', '', '', '', '', NULL, '2020-04-27 21:04:37', '2020-04-27 21:04:37', NULL, NULL),
(15, 'new testing :-o :-o :-o :-o', 3, 3, 1, '', '', '', '', '', NULL, '2020-04-27 21:06:26', '2020-04-27 21:06:26', NULL, NULL),
(16, 'nbbbjhbjbj', 3, 3, 1, '', '', '', '', '', NULL, '2020-05-05 21:14:52', '2020-05-05 21:14:52', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post_follows`
--

CREATE TABLE `post_follows` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_follows`
--

INSERT INTO `post_follows` (`id`, `post_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, NULL, NULL, NULL),
(3, 3, 3, NULL, NULL, NULL),
(4, 4, 3, NULL, NULL, NULL),
(5, 5, 3, NULL, NULL, NULL),
(6, 6, 3, NULL, NULL, NULL),
(7, 7, 3, NULL, NULL, NULL),
(8, 8, 3, NULL, NULL, NULL),
(9, 9, 3, NULL, NULL, NULL),
(10, 10, 3, NULL, NULL, NULL),
(11, 11, 3, NULL, NULL, NULL),
(13, 2, 3, NULL, NULL, NULL),
(14, 11, 3, NULL, NULL, NULL),
(15, 13, 3, NULL, NULL, NULL),
(16, 14, 3, NULL, NULL, NULL),
(20, 16, 3, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post_likes`
--

CREATE TABLE `post_likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_likes`
--

INSERT INTO `post_likes` (`id`, `post_id`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 2, 3, '2020-04-27 19:41:52', '2020-04-27 19:41:52'),
(3, 11, 3, '2020-04-27 19:41:57', '2020-04-27 19:41:57');

-- --------------------------------------------------------

--
-- Table structure for table `post_media`
--

CREATE TABLE `post_media` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `media_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_media`
--

INSERT INTO `post_media` (`id`, `post_id`, `media_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 83, NULL, NULL, NULL),
(2, 8, 86, NULL, NULL, NULL),
(3, 10, 90, NULL, NULL, NULL),
(4, 14, 91, NULL, NULL, NULL),
(5, 15, 92, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post_reports`
--

CREATE TABLE `post_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `reporter_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post_shares`
--

CREATE TABLE `post_shares` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post_tags`
--

CREATE TABLE `post_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'Admin', 'Access to everything', '2020-04-23 10:21:30', '2020-04-23 10:21:30', NULL),
(2, 'user', 'User', 'Access limited to user', '2020-04-23 10:21:30', '2020-04-23 10:21:30', NULL),
(3, 'moderator', 'Moderator', 'Access limited to moderator', '2020-04-23 10:21:30', '2020-04-23 10:21:30', NULL),
(4, 'editor', 'Editor', 'Access limited to editor', '2020-04-23 10:21:31', '2020-04-23 10:21:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `saved_posts`
--

CREATE TABLE `saved_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `saved_timelines`
--

CREATE TABLE `saved_timelines` (
  `id` int(10) UNSIGNED NOT NULL,
  `timeline_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'noreply_email', 'noreply@nofikkr.com', '2020-04-23 10:19:52', '2020-04-23 10:19:52', NULL),
(2, 'language', 'en', '2020-04-23 10:19:52', '2020-04-23 10:19:52', NULL),
(3, 'logo', 'logo.jpg', '2020-04-23 10:19:52', '2020-04-23 10:19:52', NULL),
(4, 'favicon', 'favicon.jpg', '2020-04-23 10:19:52', '2020-04-23 10:19:52', NULL),
(5, 'enable_browse', 'on', '2020-04-23 10:19:53', '2020-04-23 10:19:53', NULL),
(6, 'meta_description', 'nofikkr', '2020-04-23 10:19:53', '2020-04-23 10:19:53', NULL),
(7, 'meta_keywords', 'nofikkr', '2020-04-23 10:19:53', '2020-04-23 10:19:53', NULL),
(8, 'site_tagline', 'makeup site', '2020-04-23 10:19:55', '2020-04-23 10:19:55', NULL),
(9, 'invite_privacy', 'only_admins', '2020-04-23 10:20:11', '2020-04-23 10:20:11', NULL),
(10, 'event_timeline_post_privacy', 'only_guests', '2020-04-23 10:20:11', '2020-04-23 10:20:11', NULL),
(11, 'title_seperator', '|', '2020-04-23 10:20:11', '2020-04-23 10:20:11', NULL),
(12, 'timezone', 'Asia/Kolkata', '2020-04-23 10:20:11', '2020-04-23 10:20:11', NULL),
(13, 'enable_rtl', 'off', '2020-04-23 10:20:11', '2020-04-23 10:20:11', NULL),
(14, 'group_event_privacy', 'only_admins', '2020-04-23 10:20:28', '2020-04-23 10:20:28', NULL),
(15, 'footer_languages', 'off', '2020-04-23 10:20:28', '2020-04-23 13:46:44', NULL),
(16, 'linkedin_link', 'http://linkedin.com/', '2020-04-23 10:20:28', '2020-04-23 10:20:28', NULL),
(17, 'instagram_link', 'http://instagram.com/', '2020-04-23 10:20:28', '2020-04-23 10:20:28', NULL),
(18, 'dribbble_link', 'http://dribbble.com/', '2020-04-23 10:20:28', '2020-04-23 10:20:28', NULL),
(19, 'comment_privacy', 'everyone', '2020-04-23 10:21:31', '2020-04-23 10:21:31', NULL),
(20, 'confirm_follow', 'no', '2020-04-23 10:21:31', '2020-04-23 10:21:31', NULL),
(21, 'follow_privacy', 'everyone', '2020-04-23 10:21:31', '2020-04-23 10:21:31', NULL),
(22, 'user_timeline_post_privacy', 'everyone', '2020-04-23 10:21:31', '2020-04-23 10:21:31', NULL),
(23, 'post_privacy', 'everyone', '2020-04-23 10:21:31', '2020-04-23 10:21:31', NULL),
(24, 'page_message_privacy', 'everyone', '2020-04-23 10:21:31', '2020-04-23 10:21:31', NULL),
(25, 'page_timeline_post_privacy', 'everyone', '2020-04-23 10:21:31', '2020-04-23 10:21:31', NULL),
(26, 'page_member_privacy', 'only_admins', '2020-04-23 10:21:31', '2020-04-23 10:21:31', NULL),
(27, 'member_privacy', 'only_admins', '2020-04-23 10:21:32', '2020-04-23 10:21:32', NULL),
(28, 'group_timeline_post_privacy', 'members', '2020-04-23 10:21:32', '2020-04-23 10:21:32', NULL),
(29, 'group_member_privacy', 'only_admins', '2020-04-23 10:21:32', '2020-04-23 10:21:32', NULL),
(30, 'site_name', 'MAAS', '2020-04-23 10:21:32', '2020-04-23 10:21:32', NULL),
(31, 'site_title', 'MAAS', '2020-04-23 10:21:32', '2020-04-23 10:21:32', NULL),
(32, 'site_url', 'mk.theblackshort.com', '2020-04-23 10:21:32', '2020-04-23 10:21:32', NULL),
(33, 'twitter_link', 'http://twitter.com/', '2020-04-23 10:21:32', '2020-04-23 10:21:32', NULL),
(34, 'facebook_link', 'http://facebook.com/', '2020-04-23 10:21:32', '2020-04-23 10:21:32', NULL),
(35, 'youtube_link', 'http://youtube.com/', '2020-04-23 10:21:32', '2020-04-23 10:21:32', NULL),
(36, 'support_email', 'admin@nofikkr.com', '2020-04-23 10:21:32', '2020-04-23 10:21:32', NULL),
(37, 'mail_verification', 'off', '2020-04-23 10:21:32', '2020-04-23 10:21:32', NULL),
(38, 'captcha', 'off', '2020-04-23 10:21:32', '2020-04-23 10:21:32', NULL),
(39, 'censored_words', '', '2020-04-23 10:21:32', '2020-04-23 10:21:32', NULL),
(40, 'birthday', 'off', '2020-04-23 10:21:32', '2020-04-23 10:21:32', NULL),
(41, 'city', 'off', '2020-04-23 10:21:33', '2020-04-23 10:21:33', NULL),
(42, 'about', 'http://mk.theblackshort.com/', '2020-04-23 10:21:33', '2020-04-23 10:21:33', NULL),
(43, 'contact_text', 'Contact page description can be here', '2020-04-23 10:21:33', '2020-04-23 10:21:33', NULL),
(44, 'address_on_mail', 'http://mk.theblackshort.com/', '2020-04-23 10:21:33', '2020-04-23 10:21:33', NULL),
(45, 'items_page', '10', '2020-04-23 10:21:33', '2020-04-23 10:21:33', NULL),
(46, 'min_items_page', '5', '2020-04-23 10:21:33', '2020-04-23 10:21:33', NULL),
(47, 'user_message_privacy', 'everyone', '2020-04-23 10:21:33', '2020-04-23 10:21:33', NULL),
(48, 'home_welcome_message', 'http://mk.theblackshort.com/', '2020-04-23 10:21:33', '2020-04-23 10:21:33', NULL),
(49, 'home_widget_one', 'http://mk.theblackshort.com/', '2020-04-23 10:21:33', '2020-04-23 10:21:33', NULL),
(50, 'home_widget_two', 'http://mk.theblackshort.com/', '2020-04-23 10:21:33', '2020-04-23 10:21:33', NULL),
(51, 'home_widget_three', 'Emoticons, hashtags, music, youtube video, photos, hangouts and many others can be posted', '2020-04-23 10:21:33', '2020-04-23 10:21:33', NULL),
(52, 'home_list_heading', 'Enhancing Features of Socialite', '2020-04-23 10:21:33', '2020-04-23 10:21:33', NULL),
(53, 'home_feature_one_icon', 'users', '2020-04-23 10:21:33', '2020-04-23 10:21:33', NULL),
(54, 'home_feature_one', 'Find and connect with real people living through out the world', '2020-04-23 10:21:34', '2020-04-23 10:21:34', NULL),
(55, 'home_feature_two_icon', 'share-alt', '2020-04-23 10:21:34', '2020-04-23 10:21:34', NULL),
(56, 'home_feature_two', 'Share your posts in other social networks', '2020-04-23 10:21:34', '2020-04-23 10:21:34', NULL),
(57, 'home_feature_three_icon', 'link', '2020-04-23 10:21:34', '2020-04-23 10:21:34', NULL),
(58, 'home_feature_three', 'Add links in your posts with new innovative look', '2020-04-23 10:21:34', '2020-04-23 10:21:34', NULL),
(59, 'home_feature_four_icon', 'bullhorn', '2020-04-23 10:21:34', '2020-04-23 10:21:34', NULL),
(60, 'home_feature_four', 'Place your google Adsense through out the application', '2020-04-23 10:21:34', '2020-04-23 10:21:34', NULL),
(61, 'home_feature_five_icon', 'connectdevelop', '2020-04-23 10:21:34', '2020-04-23 10:21:34', NULL),
(62, 'home_feature_five', 'Connect to MAAS through Facebook, Twitter, Google Plus and Instagram', '2020-04-23 10:21:34', '2020-04-23 10:21:34', NULL),
(63, 'home_feature_six_icon', 'save', '2020-04-23 10:21:34', '2020-04-23 10:21:34', NULL),
(64, 'home_feature_six', 'Now you can save your favourite posts, pages, groups and events', '2020-04-23 10:21:34', '2020-04-23 10:21:34', NULL),
(65, 'home_feature_seven_icon', 'file-photo-o', '2020-04-23 10:21:35', '2020-04-23 10:21:35', NULL),
(66, 'home_feature_seven', 'Create your albums and upload the pictures right now', '2020-04-23 10:21:35', '2020-04-23 10:21:35', NULL),
(67, 'home_feature_eight_icon', 'flag-o', '2020-04-23 10:21:35', '2020-04-23 10:21:35', NULL),
(68, 'home_feature_eight', 'Any page or a post or a group or an event can be reported', '2020-04-23 10:21:35', '2020-04-23 10:21:35', NULL),
(69, 'home_feature_nine_icon', 'language', '2020-04-23 10:21:35', '2020-04-23 10:21:35', NULL),
(70, 'home_feature_nine', 'MAAS is multi-lingual and available in 16 languages', '2020-04-23 10:21:35', '2020-04-23 10:21:35', NULL),
(71, 'home_feature_ten_icon', 'user-plus', '2020-04-23 10:21:35', '2020-04-23 10:21:35', NULL),
(72, 'home_feature_ten', 'Affiliate System adds an extra flavour to MAAS', '2020-04-23 10:21:36', '2020-04-23 10:21:36', NULL),
(73, 'google_analytics', '', '2020-04-23 13:34:31', '2020-04-23 13:34:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `static_pages`
--

CREATE TABLE `static_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `threads`
--

CREATE TABLE `threads` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `timelines`
--

CREATE TABLE `timelines` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `about` text COLLATE utf8_unicode_ci NOT NULL,
  `avatar_id` int(10) UNSIGNED DEFAULT NULL,
  `cover_id` int(10) UNSIGNED DEFAULT NULL,
  `cover_position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hide_cover` int(11) NOT NULL DEFAULT '0',
  `background_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `timelines`
--

INSERT INTO `timelines` (`id`, `username`, `name`, `about`, `avatar_id`, `cover_id`, `cover_position`, `type`, `hide_cover`, `background_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ashish-gaur', 'Ashish Gaur', 'Some text about me', 82, 81, NULL, 'user', 0, NULL, '2020-04-23 10:21:36', '2020-04-23 14:11:15', NULL),
(2, 'vijay', 'Vijay Kumar', 'Some text about me', NULL, NULL, NULL, 'user', 0, NULL, '2020-04-23 10:21:36', '2020-04-23 10:21:36', NULL),
(3, 'Awanish', 'ashish', 'Make U Up Makeup Academy is an NSDC- Govt. of India affiliated makeup school. We have carefully crafted a curriculum as per Skill India standards for Hair Styling and Makeup Courses in Delhi. Our professional makeup courses are designed for people looking to groom themselves and even those who wish to pursue makeup artistry, professionally from amateur to advanced level.\r\nJoin Pooja Sharma’s most Comprehensive and Experienced makeup academy, since 1998, and We are here to help you to sync your passion with a brilliant career.', 94, 95, NULL, 'user', 0, NULL, '2020-04-27 14:11:48', '2020-05-06 10:51:09', NULL),
(4, 'amita', 'amita', 'write something about yourself', NULL, NULL, NULL, 'user', 0, NULL, '2020-05-06 07:32:59', '2020-05-06 07:32:59', NULL),
(5, 'contact', 'asdfsfsd', 'write something about yourself', NULL, NULL, NULL, 'user', 0, NULL, '2020-05-06 09:56:05', '2020-05-06 09:56:05', NULL),
(6, 'khare', 'Abhi', 'write something about yourself', NULL, NULL, NULL, 'user', 0, NULL, '2020-05-06 12:06:20', '2020-05-06 12:06:20', NULL),
(7, 'examp12', 'example', 'write something about yourself', NULL, NULL, NULL, 'user', 0, NULL, '2020-05-07 01:57:39', '2020-05-07 01:57:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `timeline_reports`
--

CREATE TABLE `timeline_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `timeline_id` int(10) UNSIGNED NOT NULL,
  `reporter_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `timeline_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `verification_code` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `email_verified` tinyint(1) DEFAULT NULL,
  `remember_token` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `balance` double(8,2) NOT NULL DEFAULT '0.00',
  `birthday` date NOT NULL DEFAULT '1970-01-01',
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hobbies` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interests` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_option1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_option2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_option3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_option4` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `last_logged` timestamp NULL DEFAULT NULL,
  `timezone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `affiliate_id` int(10) UNSIGNED DEFAULT NULL,
  `language` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dribbble_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkedin_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `timeline_id`, `email`, `verification_code`, `verified`, `email_verified`, `remember_token`, `password`, `balance`, `birthday`, `city`, `country`, `designation`, `hobbies`, `interests`, `custom_option1`, `custom_option2`, `custom_option3`, `custom_option4`, `gender`, `active`, `last_logged`, `timezone`, `affiliate_id`, `language`, `facebook_link`, `twitter_link`, `dribbble_link`, `instagram_link`, `youtube_link`, `linkedin_link`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'admin@unisoftmedia.com', 'B1y8EoGeZ0HmfQ8FdB', 1, 1, 'WRTfG0Mn9v', '$2y$10$0ZA8.q4MbCvm6mEn./j5Z.YFrqjblksH.TUUGYvGaipt6CQeVNSm6', 0.00, '1970-01-01', 'New Delhi', 'India', '', '', '', NULL, NULL, NULL, NULL, 'male', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-04-23 10:21:36', '2020-04-23 13:46:18', NULL),
(2, 2, 'contact@vijaykumar.me', 'znVCu0Mr4CEA508wi7', 0, 1, 'YF7VmDXKva', '$2y$10$y.WoPSGp4QoPHVGxl7OVX.3EsffKNRTo5.qvLetXCWWChNa0.xNA2', 0.00, '1970-01-01', 'Hyderabad', 'India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'male', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-04-23 10:21:36', '2020-04-23 10:21:36', NULL),
(3, 3, 'test@makeupindi.com', 'Jm7bp5ZBwedk5GyjJElbz95UywDZlU', 0, 1, 'YulXU6UAAmiWslQshNQnWFf6RJoZVgHjypPV8mD4tGN370z62hqNcZ6UvrXO', '$2y$10$aypPjitFzD4tkI.kaP5ZaOsDGc4T9FuZ6tTSQkw.ptTL8EFxJtx/W', 0.00, '1970-01-01', 'betthia', 'India', 'Manager', '', '', NULL, NULL, NULL, NULL, 'male', 1, NULL, NULL, NULL, NULL, 'https://www.facebook.com/profile.php?id=100002984453189&ref=bookmarks', '', '', '', '', '', '2020-04-27 14:11:48', '2020-05-06 10:55:04', NULL),
(4, 4, 'b@a.com', 'Zu5qZABXdVnZR3JknKuxHVXCYb2LQ6', 0, 1, 'gZnBin8dSbkYgfutvVQ0yhlEhxHjfddA5kOC0Bu2n5WtRb0j0rMxBDY9FW4y', '$2y$10$v/onNgG0KX9MwVEEWZE7GumkwE/zgbs/nS1G25vaGFpp/jInw3FyC', 0.00, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'female', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-06 07:33:00', '2020-05-06 07:33:00', NULL),
(5, 5, 'contact@barncobusiness.com', 'V1Qgmq7d5wWYf0M1XJwcQ1MkmYKzGQ', 0, 1, 'PirWTnAvPQZgsZV4kuBeXpdF5Nzm1fvsMZ5xMjBKSqoagL5N3IImYqG6Rp6h', '$2y$10$YydfUltioTb20wZbveEQlu6FGB7mDEtLxs7yI09yj/5.a/folg78W', 0.00, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'male', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-06 09:56:05', '2020-05-06 09:56:05', NULL),
(6, 6, 'fast.abhishek@live.in', 'ZxtJ47vQ3X78IxxRD4OkErMzgrP53U', 0, 1, '8zlMgq9L6M', '$2y$10$z8ebebJkduQDY1BTbT/87.3ru0y7WNr5wQydxrkSMm.ThiAAB763K', 0.00, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'male', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-06 12:06:20', '2020-05-06 12:06:20', NULL),
(7, 7, 'admin@example.com', 'WrOlCpBenIr19eVxxWN4iTbmxOpV7e', 0, 1, 'CW4kdKvzxC', '$2y$10$jtEy70efrdNeTrXZHze4ue.KyNluHO11EY/3JkGcEwQ9dU6EmZMES', 0.00, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'male', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-07 01:57:39', '2020-05-07 01:57:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_settings`
--

CREATE TABLE `user_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `comment_privacy` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `follow_privacy` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `post_privacy` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `confirm_follow` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `timeline_post_privacy` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `message_privacy` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `email_follow` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `email_like_post` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `email_post_share` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `email_comment_post` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `email_like_comment` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `email_reply_comment` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `email_join_group` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `email_like_page` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_settings`
--

INSERT INTO `user_settings` (`id`, `user_id`, `comment_privacy`, `follow_privacy`, `post_privacy`, `confirm_follow`, `timeline_post_privacy`, `message_privacy`, `email_follow`, `email_like_post`, `email_post_share`, `email_comment_post`, `email_like_comment`, `email_reply_comment`, `email_join_group`, `email_like_page`) VALUES
(1, 1, 'everyone', 'everyone', 'everyone', 'no', 'everyone', 'everyone', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no'),
(2, 2, 'everyone', 'everyone', 'everyone', 'no', 'everyone', 'everyone', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no'),
(3, 3, 'everyone', 'everyone', 'everyone', 'no', 'everyone', 'everyone', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no'),
(4, 4, 'everyone', 'everyone', 'everyone', 'no', 'everyone', 'everyone', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no'),
(5, 5, 'everyone', 'everyone', 'everyone', 'no', 'everyone', 'everyone', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no'),
(6, 6, 'everyone', 'everyone', 'everyone', 'no', 'everyone', 'everyone', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no'),
(7, 7, 'everyone', 'everyone', 'everyone', 'no', 'everyone', 'everyone', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wallpapers`
--

CREATE TABLE `wallpapers` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `media_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`),
  ADD KEY `albums_timeline_id_foreign` (`timeline_id`),
  ADD KEY `albums_preview_id_foreign` (`preview_id`);

--
-- Indexes for table `album_media`
--
ALTER TABLE `album_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `album_media_album_id_foreign` (`album_id`),
  ADD KEY `album_media_media_id_foreign` (`media_id`);

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcement_user`
--
ALTER TABLE `announcement_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `announcement_user_announcement_id_foreign` (`announcement_id`),
  ADD KEY `announcement_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_post_id_foreign` (`post_id`),
  ADD KEY `comments_user_id_foreign` (`user_id`),
  ADD KEY `comments_parent_id_foreign` (`parent_id`),
  ADD KEY `comments_media_id_foreign` (`media_id`);

--
-- Indexes for table `comment_likes`
--
ALTER TABLE `comment_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comment_likes_user_id_foreign` (`user_id`),
  ADD KEY `comment_likes_comment_id_foreign` (`comment_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `events_timeline_id_foreign` (`timeline_id`),
  ADD KEY `events_user_id_foreign` (`user_id`),
  ADD KEY `events_group_id_foreign` (`group_id`);

--
-- Indexes for table `event_user`
--
ALTER TABLE `event_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_user_event_id_foreign` (`event_id`),
  ADD KEY `event_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `followers_follower_id_foreign` (`follower_id`),
  ADD KEY `followers_leader_id_foreign` (`leader_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `groups_timeline_id_foreign` (`timeline_id`);

--
-- Indexes for table `group_user`
--
ALTER TABLE `group_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_user_group_id_foreign` (`group_id`),
  ADD KEY `group_user_user_id_foreign` (`user_id`),
  ADD KEY `group_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `hashtags`
--
ALTER TABLE `hashtags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_post_id_foreign` (`post_id`),
  ADD KEY `notifications_timeline_id_foreign` (`timeline_id`),
  ADD KEY `notifications_user_id_foreign` (`user_id`),
  ADD KEY `notifications_notified_by_foreign` (`notified_by`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pages_timeline_id_foreign` (`timeline_id`),
  ADD KEY `pages_category_id_foreign` (`category_id`);

--
-- Indexes for table `page_likes`
--
ALTER TABLE `page_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_likes_page_id_foreign` (`page_id`),
  ADD KEY `page_likes_user_id_foreign` (`user_id`);

--
-- Indexes for table `page_user`
--
ALTER TABLE `page_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_user_page_id_foreign` (`page_id`),
  ADD KEY `page_user_user_id_foreign` (`user_id`),
  ADD KEY `page_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_timeline_id_foreign` (`timeline_id`),
  ADD KEY `posts_user_id_foreign` (`user_id`),
  ADD KEY `posts_shared_post_id_foreign` (`shared_post_id`);

--
-- Indexes for table `post_follows`
--
ALTER TABLE `post_follows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_follows_post_id_foreign` (`post_id`),
  ADD KEY `post_follows_user_id_foreign` (`user_id`);

--
-- Indexes for table `post_likes`
--
ALTER TABLE `post_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_likes_post_id_foreign` (`post_id`),
  ADD KEY `post_likes_user_id_foreign` (`user_id`);

--
-- Indexes for table `post_media`
--
ALTER TABLE `post_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_media_post_id_foreign` (`post_id`),
  ADD KEY `post_media_media_id_foreign` (`media_id`);

--
-- Indexes for table `post_reports`
--
ALTER TABLE `post_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_reports_post_id_foreign` (`post_id`),
  ADD KEY `post_reports_reporter_id_foreign` (`reporter_id`);

--
-- Indexes for table `post_shares`
--
ALTER TABLE `post_shares`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_shares_post_id_foreign` (`post_id`),
  ADD KEY `post_shares_user_id_foreign` (`user_id`);

--
-- Indexes for table `post_tags`
--
ALTER TABLE `post_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_tags_post_id_foreign` (`post_id`),
  ADD KEY `post_tags_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `saved_posts`
--
ALTER TABLE `saved_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `saved_posts_post_id_foreign` (`post_id`),
  ADD KEY `saved_posts_user_id_foreign` (`user_id`);

--
-- Indexes for table `saved_timelines`
--
ALTER TABLE `saved_timelines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `saved_timelines_timeline_id_foreign` (`timeline_id`),
  ADD KEY `saved_timelines_user_id_foreign` (`user_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `settings_key_index` (`key`);

--
-- Indexes for table `static_pages`
--
ALTER TABLE `static_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `threads`
--
ALTER TABLE `threads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timelines`
--
ALTER TABLE `timelines`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `timelines_username_unique` (`username`),
  ADD KEY `timelines_avatar_id_foreign` (`avatar_id`),
  ADD KEY `timelines_cover_id_foreign` (`cover_id`),
  ADD KEY `timelines_background_id_foreign` (`background_id`);

--
-- Indexes for table `timeline_reports`
--
ALTER TABLE `timeline_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `timeline_reports_timeline_id_foreign` (`timeline_id`),
  ADD KEY `timeline_reports_reporter_id_foreign` (`reporter_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_timeline_id_foreign` (`timeline_id`),
  ADD KEY `users_affiliate_id_foreign` (`affiliate_id`);

--
-- Indexes for table `user_settings`
--
ALTER TABLE `user_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_settings_user_id_foreign` (`user_id`);

--
-- Indexes for table `wallpapers`
--
ALTER TABLE `wallpapers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wallpapers_media_id_foreign` (`media_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `album_media`
--
ALTER TABLE `album_media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `announcement_user`
--
ALTER TABLE `announcement_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `comment_likes`
--
ALTER TABLE `comment_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_user`
--
ALTER TABLE `event_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `followers`
--
ALTER TABLE `followers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `group_user`
--
ALTER TABLE `group_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hashtags`
--
ALTER TABLE `hashtags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page_likes`
--
ALTER TABLE `page_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page_user`
--
ALTER TABLE `page_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `participants`
--
ALTER TABLE `participants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `post_follows`
--
ALTER TABLE `post_follows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `post_likes`
--
ALTER TABLE `post_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `post_media`
--
ALTER TABLE `post_media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `post_reports`
--
ALTER TABLE `post_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `post_shares`
--
ALTER TABLE `post_shares`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `post_tags`
--
ALTER TABLE `post_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `saved_posts`
--
ALTER TABLE `saved_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `saved_timelines`
--
ALTER TABLE `saved_timelines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `static_pages`
--
ALTER TABLE `static_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `threads`
--
ALTER TABLE `threads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `timelines`
--
ALTER TABLE `timelines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `timeline_reports`
--
ALTER TABLE `timeline_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user_settings`
--
ALTER TABLE `user_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `wallpapers`
--
ALTER TABLE `wallpapers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `albums`
--
ALTER TABLE `albums`
  ADD CONSTRAINT `albums_preview_id_foreign` FOREIGN KEY (`preview_id`) REFERENCES `media` (`id`),
  ADD CONSTRAINT `albums_timeline_id_foreign` FOREIGN KEY (`timeline_id`) REFERENCES `timelines` (`id`);

--
-- Constraints for table `album_media`
--
ALTER TABLE `album_media`
  ADD CONSTRAINT `album_media_album_id_foreign` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`),
  ADD CONSTRAINT `album_media_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`);

--
-- Constraints for table `announcement_user`
--
ALTER TABLE `announcement_user`
  ADD CONSTRAINT `announcement_user_announcement_id_foreign` FOREIGN KEY (`announcement_id`) REFERENCES `announcements` (`id`),
  ADD CONSTRAINT `announcement_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`),
  ADD CONSTRAINT `comments_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `comments` (`id`),
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `comment_likes`
--
ALTER TABLE `comment_likes`
  ADD CONSTRAINT `comment_likes_comment_id_foreign` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`id`),
  ADD CONSTRAINT `comment_likes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  ADD CONSTRAINT `events_timeline_id_foreign` FOREIGN KEY (`timeline_id`) REFERENCES `timelines` (`id`),
  ADD CONSTRAINT `events_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `event_user`
--
ALTER TABLE `event_user`
  ADD CONSTRAINT `event_user_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`),
  ADD CONSTRAINT `event_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `followers`
--
ALTER TABLE `followers`
  ADD CONSTRAINT `followers_follower_id_foreign` FOREIGN KEY (`follower_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `followers_leader_id_foreign` FOREIGN KEY (`leader_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `groups_timeline_id_foreign` FOREIGN KEY (`timeline_id`) REFERENCES `timelines` (`id`);

--
-- Constraints for table `group_user`
--
ALTER TABLE `group_user`
  ADD CONSTRAINT `group_user_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  ADD CONSTRAINT `group_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `group_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_notified_by_foreign` FOREIGN KEY (`notified_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `notifications_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `notifications_timeline_id_foreign` FOREIGN KEY (`timeline_id`) REFERENCES `timelines` (`id`),
  ADD CONSTRAINT `notifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `pages_timeline_id_foreign` FOREIGN KEY (`timeline_id`) REFERENCES `timelines` (`id`);

--
-- Constraints for table `page_likes`
--
ALTER TABLE `page_likes`
  ADD CONSTRAINT `page_likes_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`),
  ADD CONSTRAINT `page_likes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `page_user`
--
ALTER TABLE `page_user`
  ADD CONSTRAINT `page_user_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`),
  ADD CONSTRAINT `page_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `page_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_shared_post_id_foreign` FOREIGN KEY (`shared_post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `posts_timeline_id_foreign` FOREIGN KEY (`timeline_id`) REFERENCES `timelines` (`id`),
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `post_follows`
--
ALTER TABLE `post_follows`
  ADD CONSTRAINT `post_follows_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `post_follows_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `post_likes`
--
ALTER TABLE `post_likes`
  ADD CONSTRAINT `post_likes_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `post_likes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `post_media`
--
ALTER TABLE `post_media`
  ADD CONSTRAINT `post_media_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`),
  ADD CONSTRAINT `post_media_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`);

--
-- Constraints for table `post_reports`
--
ALTER TABLE `post_reports`
  ADD CONSTRAINT `post_reports_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `post_reports_reporter_id_foreign` FOREIGN KEY (`reporter_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `post_shares`
--
ALTER TABLE `post_shares`
  ADD CONSTRAINT `post_shares_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `post_shares_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `post_tags`
--
ALTER TABLE `post_tags`
  ADD CONSTRAINT `post_tags_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `post_tags_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `saved_posts`
--
ALTER TABLE `saved_posts`
  ADD CONSTRAINT `saved_posts_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `saved_posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `saved_timelines`
--
ALTER TABLE `saved_timelines`
  ADD CONSTRAINT `saved_timelines_timeline_id_foreign` FOREIGN KEY (`timeline_id`) REFERENCES `timelines` (`id`),
  ADD CONSTRAINT `saved_timelines_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `timelines`
--
ALTER TABLE `timelines`
  ADD CONSTRAINT `timelines_avatar_id_foreign` FOREIGN KEY (`avatar_id`) REFERENCES `media` (`id`),
  ADD CONSTRAINT `timelines_background_id_foreign` FOREIGN KEY (`background_id`) REFERENCES `media` (`id`),
  ADD CONSTRAINT `timelines_cover_id_foreign` FOREIGN KEY (`cover_id`) REFERENCES `media` (`id`);

--
-- Constraints for table `timeline_reports`
--
ALTER TABLE `timeline_reports`
  ADD CONSTRAINT `timeline_reports_reporter_id_foreign` FOREIGN KEY (`reporter_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `timeline_reports_timeline_id_foreign` FOREIGN KEY (`timeline_id`) REFERENCES `timelines` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_affiliate_id_foreign` FOREIGN KEY (`affiliate_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `users_timeline_id_foreign` FOREIGN KEY (`timeline_id`) REFERENCES `timelines` (`id`);

--
-- Constraints for table `user_settings`
--
ALTER TABLE `user_settings`
  ADD CONSTRAINT `user_settings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `wallpapers`
--
ALTER TABLE `wallpapers`
  ADD CONSTRAINT `wallpapers_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
